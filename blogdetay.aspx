﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="blogdetay.aspx.cs" Inherits="blogdetay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


     <section id="Content" role="main">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div><!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h2 class=" pull-left">
                            Blog
                        </h2>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="index.html">Home</a></li><!--
                            --><li><a href="index.html">Pages</a></li><!--
                            --><li>Blog</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div><!-- !full-width -->
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

        <div class="container">
            <div class="row">
                <div class="col-md-9 space-right-30">

                    <div class="row">
                        <div class="col-md-12 article-wrapper">
                            <!-- POST TYPE STANDARD -->
                            <article class="post post-image">
                                <a href="#">
                                    <img src="images/demo-content/blog-image.jpg"  alt="Blog post image">
                                </a>
                                <header class="post-info-name-date-comments">
                                    <span class="date">Aug 7, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">Leave a comment</a>
                                    <h1>Standard Post Format with Image</h1>
                                </header>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do <a href="#">textlink</a> est tempor incididunt
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute et irure a dolor in reprehenderit in
                                    voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                                <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
                                    beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                                    odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
                                    sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                                    voluptatem.
                                </p>
                                <p class="paragraph-highlight">
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi aliquip commodo
                                    consequat. Duis aute et irure a dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                                <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque
                                    laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto
                                    beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                                    odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                                </p>
                                <h3>
                                    A Large Subheading
                                </h3>
                                <p>
                                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
                                    sed quia numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                                    voluptatem.
                                </p>
                                <h4>
                                    A Smaller Subheading
                                </h4>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod est tempor incididunt
                                    ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                                    laboris nisi ut aliquip ex ea commodo consequat. Duis aute et irure a dolor in reprehenderit in
                                    voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                    non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                                <ul>
                                    <li>
                                        Sed ut perspiciatis unde omnis iste natus error
                                    </li>
                                    <li>
                                        Sit voluptatem accusantium doloremque laudantium
                                    </li>
                                    <li>
                                        Eaque ipsa quae ab illo inventore veritatis et quasi
                                    </li>
                                    <li>
                                        Architecto beatae vitae dicta sunt explicabo
                                    </li>
                                </ul>
                                <p>
                                    Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit,
                                    sed quia numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                                    voluptatem. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                                    ex ea commodo consequat.
                                </p>
                            </article>
                            <!-- !POST TYPE STANDARD -->
                        </div>
                    </div>

                    <nav class="row">
                        <div class="col-md-12">
                            <div class="post-navigation">
                                <div class="next">
                                    <span>Next article</span><br>
                                    <a href="#">Standard Post Format</a>
                                </div>
                                <div class="prev">
                                    <span>Previous article</span><br>
                                    <a href="#">Gallery Post Format</a>
                                </div>
                            </div>
                        </div>
                    </nav>

                    <section id="Comments" class="row comments">
                        <div class="section-header col-xs-12">
                            <hr>
                            <h2 class="strong-header">
                                3 comments
                            </h2>
                        </div>
                        <div class="col-xs-12">
                            <ul class="comment-list list-unstyled">
                                <li>
                                    <article class="comment">
                                        <header>
                                            <img alt="" src="images/avatar.png" class="avatar">
                                            <h4 class="author">Richard Doe</h4>
                                            <span class="date">Aug 7, 2013 at 8:42</span>
                                            <a class="comment-reply" href="#">Reply</a>
                                        </header>

                                        <div class="comment-content">
                                            <p>
                                                Choupette Mulberry dark red lipstick crop button up chunky sole chambray shirt
                                                maxi skirt vintage Levi shorts. Loafers 90s collar indigo denim silver collar
                                                round sunglasses. Cashmere skirt peach Miu Miu Bag 'N' Noun leather shorts
                                                oversized printed clashing patterns. Tulle printed jacket sheer.
                                            </p>
                                        </div>
                                    </article>
                                    <ol class="comment-children list-unstyled">
                                        <li>
                                            <article class="comment">
                                                <header>
                                                    <img alt="" src="images/avatar.png" class="avatar">
                                                    <h4 class="author">Kristopher Kleiner</h4>
                                                    <span class="date">Aug 9, 2013 at 10:01 pm</span>
                                                    <a class="comment-reply" href="#">Reply</a>
                                                </header>

                                                <div class="comment-content">
                                                    <p>
                                                        Leggings washed out Raf Simons green flats Mulberry parka collar.
                                                        Printed jacket preppy chunky sole print neutral cotton plaited Topshop.
                                                    </p>
                                                </div>
                                            </article>
                                        </li>
                                    </ol>
                                </li>
                                <li>
                                    <article class="comment">
                                        <header>
                                            <img alt="" src="images/avatar.png" class="avatar">
                                            <h4 class="author">Melissa Greenfield</h4>
                                            <span class="date">Aug 10, 2013 at 11:59 am</span>
                                            <a class="comment-reply" href="#">Reply</a>
                                        </header>

                                        <div class="comment-content">
                                            <p>
                                                Choupette clashing patterns skinny jeans chunky sole. Crop green cashmere
                                                plaited Chanel I. street style flats cotton metal collar necklace. Razor pleat
                                                printed sheer Colette preppy trouser oversized tucked t-shirt white backpack.
                                            </p>
                                        </div>
                                    </article>
                                </li>
                            </ul>
                        </div>
                    </section>

                    <section class="row">
                        <div class="section-header col-xs-12">
                            <hr>
                            <h2 class="strong-header">
                                Add a new comment
                            </h2>
                        </div>
                        <div class="col-xs-12">
                            <form>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputName1">Name</label>
                                            <input type="text" class="form-control" id="inputName1">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputEmail1">Email</label>
                                            <input type="email" class="form-control" id="inputEmail1">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="inputComment">Comment</label>
                                    <textarea class="form-control" id="inputComment" rows="10"></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Submit comment</button>
                            </form>
                        </div>
                    </section>

                </div>
                <div class="col-md-3">

                    <!-- SEARCH WIDGET -->
                    <div class="search-widget">
                        <form role="search">
                            <div class="form-group">
                                <label class="sr-only" for="page-search">Type your search here</label>
                                <input type="search" id="page-search" class="form-control">
                            </div><!-- no whitespace
                            --><button class="btn btn-default page-search">
                                <span class="fa fa-search">
                                    <span class="sr-only">Search</span>
                                </span>
                            </button>
                        </form>
                    </div>
                    <!-- !SEARCH WIDGET -->

                    <!-- CATEGORIES WIDGET -->
                    <h3 class="strong-header">
                        Categories
                    </h3>
                    <div class="categories-widget">
                        <ul class="list-unstyled">
                            <li><a href="#">Aside</a><span>1</span></li>
                            <li><a href="#">Audios</a><span>4</span></li>
                            <li><a href="#">Gallery</a><span>12</span></li>
                            <li><a href="#">Inspiration</a><span>46</span></li>
                            <li><a href="#">Links</a><span>3</span></li>
                            <li><a href="#">Photography</a><span>18</span></li>
                            <li><a href="#">Quotes</a><span>7</span></li>
                            <li><a href="#">SoundCloud</a><span>22</span></li>
                            <li><a href="#">Vimeo</a><span>9</span></li>
                        </ul>
                    </div>
                    <!-- !CATEGORIES WIDGET -->

                    <!-- RECENT POSTS WIDGET -->
                    <h3 class="strong-header">
                        Recent posts
                    </h3>
                    <div class="recent-posts-widget">
                        <ul class="list-unstyled">
                            <li>
                                <h5><a href="#">Rope necklace strong eyebrows center part shoe razor pleat white shirt</a></h5>
                                <span class="date">Aug, 9, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">3 comments</a>
                            </li>
                            <li>
                                <h5 class="title"><a href="#">Grey leather shorts collar vintage</a></h5>
                                <span class="date">Aug 7, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">1 comments</a>
                            </li>
                            <li>
                                <h5 class="title"><a href="#">Tucked t-shirt motif Missoni maxi skirt white Lanvin headscarf</a></h5>
                                <span class="date">Aug 5, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">12 comments</a>
                            </li>
                        </ul>
                    </div>
                    <!-- !RECENT POSTS WIDGET -->


                </div>
            </div>
        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div> <!-- fixes floating problems when mobile menu is visible -->



</asp:Content>

