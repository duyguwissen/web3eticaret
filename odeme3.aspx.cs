﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class odeme3 : System.Web.UI.Page
{
    List<Siparis> sepettekiler;
    Il il = new Il();
    Ilce ilce = new Ilce();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null)
        {
            /*Fatura Adresi*/
            FaturaAdresGetir();

            /*Kargo Adresi*/
            KargoAdresGetir();

            Sepet sepetim = (Sepet)Session["sepettekiler"];
            sepettekiler = sepetim.siparisListesi;

            rptUrunler.DataSource = sepettekiler;
            rptUrunler.DataBind();
        }
        else
        {
            Response.Write("<script>alert('Buraya girmeye yetkiniz bulunmamaktadır.');window.location.href='" + GetRouteUrl("anasayfa", null) + "'</script>");
        }
    }

    public void FaturaAdresGetir()
    {

        Adres faturaAdres = (Adres)Session["FaturaAdres"];
        lblAd.Text = faturaAdres.adSoyad;
        lblAdres.Text = faturaAdres.adresTanimi;
        lblCepTel.Text = faturaAdres.cepTel;
        lblEmail.Text = faturaAdres.email;
        lblIl.Text = il.ilAdGetir(faturaAdres.sehirID);

        lblIlce.Text = ilce.ilceAdGetir(faturaAdres.ilceID);
        lblPosta.Text = faturaAdres.postaKodu;


    }

    public void KargoAdresGetir()
    {

        Adres kargoAdres = (Adres)Session["KargoAdres"];
        lblKargoAd.Text = kargoAdres.adSoyad;
        lblKargoAdres.Text = kargoAdres.adresTanimi;

        //lblCepTel.Text = faturaAdres.cepTel;
        //lblEmail.Text = faturaAdres.email;
        lblKargoIl.Text = il.ilAdGetir(kargoAdres.sehirID);
        lblKargoIlce.Text = ilce.ilceAdGetir(kargoAdres.ilceID);
        lblKargoPosta.Text = kargoAdres.postaKodu;

    }
    public decimal ToplamTutar()
    {
        Sepet sepettekiler = (Sepet)Session["sepettekiler"];
        return sepettekiler.toplamTutar;
    }
}