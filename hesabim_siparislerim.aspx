﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="hesabim_siparislerim.aspx.cs" Inherits="hesabim_siparislerim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <section id="Content" role="main">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Hesabım
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="index.html">Anasayfa</a></li>
                            <!--
                          -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                          -->
                            <li>Checkout</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <section class="row">
                <div class="col-sm-3">
                    <nav class="shop-section-navigation element-emphasis-weak">
                        <ul class="list-unstyled">
                            <li>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="<%$RouteUrl:RouteName=hesap%>">Hesap Kontrol Paneli</asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="<%$RouteUrl:RouteName=hesapbilgi%>">Hesap Bilgileri</asp:HyperLink>
                            </li>
                            <li class="active"><span>Ürünlerim</span></li>
                            <li>
                                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="<%$RouteUrl:RouteName=adres%>">Adres Defteri</asp:HyperLink>
                            </li>
                            <li>
                                <asp:LinkButton ID="lbtnCikis" runat="server" OnClick="lbtnCikis_Click">Çıkış</asp:LinkButton>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <div class="col-sm-9 space-left-30">
                    <h2 class="strong-header large-header">Ürünlerim</h2>
                    <div class="table table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td class="width20">Sipariş No. </td>
                                    <td>Sipariş Tarihi </td>
                                    <td>Toplam Tutar </td>
                                    <td>Durum </td>
                                    <td class="text-right width13">Detaylar </td>
                                </tr>
                            </thead>
                            <asp:Repeater ID="rptSiparisler" runat="server">
                                <ItemTemplate>
                                    <tr>
                                        <td><a href="#"><strong><%#Eval("siparisId") %></strong></a></td>
                                        <td><%#TarihDuzenle(Eval("siparisTarih")) %></td>
                                        <td><%#Eval("toplamTutar") %></td>
                                        <td><%#Eval("siparisDurum") %></td>
                                        <td class="text-right"><a href="#">Görüntüle</a></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                            <%-- <tr>
                                  <td><a href="#"><strong>8077</strong></a></td>
                                  <td>Jul 25, 2013</td>
                                  <td>$140.00</td>
                                  <td>Shipped</td>
                                  <td class="text-right"> <a href="#">View</a></td>
                              </tr>
                              <tr>
                                  <td><a href="#"><strong>8007</strong></a></td>
                                  <td>Jul 18, 2013</td>
                                  <td>$47.00</td>
                                  <td>Cancelled</td>
                                  <td class="text-right"> <a href="#">View</a></td>
                              </tr>
                              <tr>
                                  <td><a href="#"><strong>7980</strong></a></td>
                                  <td>Jul 3, 2013</td>
                                  <td>$18.00</td>
                                  <td>Shipped</td>
                                  <td class="text-right"> <a href="#">View</a></td>
                              </tr>
                              <tr>
                                  <td><a href="#"><strong>7872</strong></a></td>
                                  <td>Jun 18, 2013</td>
                                  <td>$35.00</td>
                                  <td>Shipped</td>
                                  <td class="text-right"> <a href="#">View</a></td>
                              </tr>
                              <tr>
                                  <td><a href="#"><strong>7723</strong></a></td>
                                  <td>May 28, 2013</td>
                                  <td>$78.00</td>
                                  <td>Shipped</td>
                                  <td class="text-right"> <a href="#">View</a></td>
                              </tr>--%>
                          </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->


</asp:Content>

