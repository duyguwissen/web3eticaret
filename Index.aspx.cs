﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Index : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        SlideModel sl = new SlideModel();
        Urun urun = new Urun();
        Repeater1.DataSource = sl.SlideGetir();
        Repeater1.DataBind();


        Repeater2.DataSource = urun.ensonEklenenUrunlerGetir();
        Repeater2.DataBind();
        OneCikanUrunler.DataSource = urun.oneCikanUrunlerGetir();
        OneCikanUrunler.DataBind();

        rptTopProducts.DataSource = urun.yuksekPuanliUrunGetir();
        rptTopProducts.DataBind();

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
       
        if (string.IsNullOrEmpty(TextBox1.Text) || string.IsNullOrEmpty(TextBox2.Text))
        {
            
        }

        else
        {
            Bulten bltn = new Bulten();
            bltn.EklenmeTarihi = DateTime.Now;
            bltn.Adsoyad = TextBox1.Text;
            bltn.Email = TextBox2.Text;
            bltn.BultenKayit();
        }
    }
    public string Puanla(object puan)
    {

        //string yenipuan = puan.ToString();
        //int gg = yenipuan.IndexOf(",");
        //int ondalik = yenipuan[gg+1].ToString();
        double yeniPuan = Convert.ToDouble(puan);
        string yPuan = yeniPuan.ToString();
        yPuan.IndexOf(",");
        int tamPuan = Convert.ToInt32(yPuan[0].ToString());

        if (yPuan.Length > 1)
        {
            int kusurat = Convert.ToInt32(yPuan[2].ToString());

            if (kusurat != 0)
            {
                kusurat = 5;
                string a = tamPuan + "." + kusurat;
                return tamPuan + "." + kusurat;
            }
        }


        return yeniPuan.ToString();

    }
    protected void btnEnSonUrunIncele_Click(object sender, EventArgs e)
    {
        
    }
}