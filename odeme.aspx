﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="odeme.aspx.cs" Inherits="odeme" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        $(document).ready(function () {

            $("#btnDevam").click(function () {
                var index = document.getElementById("ddlKendi").selectedIndex;
                if (index != 0) {
                    $("input").attr("required", "true");
                }
                else {
                    $("input").attr("required", "false");
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <section id="Content" role="main" runat="server">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Checkout
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="index.html">Home</a></li>
                            <!--
                          -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                          -->
                            <li>Checkout</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <div class="row">
                <div class="col-md-7">
                    <!-- ALERT BOX INFO -->
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <a href="08-a-shop-account-login.html">Login</a> or <a href="08-b-shop-account-register.html">create an account</a> for faster checkout
                    </div>
                    <!-- !ALERT BOX INFO -->
                </div>
                <div class="clearfix space-30"></div>
                <div class="col-sm-5 col-sm-push-7 space-left-30">
                    <section class="order-summary element-emphasis-weak">
                        <h3 class="strong-header element-header pull-left">Sipariş Özeti
                        </h3>
                        <a href="sepet-full.aspx" class="pull-right">Edit cart
                        </a>
                        <div class="clearfix"></div>
                        <!-- SHOP SUMMARY ITEM -->
                        <asp:Repeater ID="rptUrunler" runat="server">
                            <ItemTemplate>
                                <div class="shop-summary-item">
                                    <img src="/Resim/kucuk/<%#Eval("urun.ResimUrl") %>" alt="Shop item in cart">
                                    <header class="item-info-name-features-price">
                                        <h4><a href="#"><%#Eval("urun.urunAd") %></a></h4>
                                        <span class="features"><%#Eval("urun.Renk") %>, <%#Eval("urun.urunDetayi.ozellikDetayi") %></span><br>
                                        <span class="quantity"><%#Eval("urunAdet") %></span><b>&times;</b><span class="price"><%#Eval("urun.UrunYeniFiyat") %></span>
                                    </header>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <!-- !SHOP SUMMARY ITEM -->
                        <!-- SHOP SUMMARY ITEM -->
                        <%--<div class="shop-summary-item">
                          <img src="/images/demo-content/cart-purchased-small-2.jpg"  alt="Shop item in cart">
                          <header class="item-info-name-features-price">
                              <h4><a href="04-shop-product-single.html">Denim Jacket in Oversized Boyfriend Fit in Vintage Wash</a></h4>
                              <span class="features">Light Blue, M</span><br>
                              <span class="quantity">1</span><b>&times;</b><span class="price">$65.00</span>
                          </header>
                      </div>--%>
                        <!-- !SHOP SUMMARY ITEM -->
                        <dl class="order-summary-price">
                            <dt>Alt Toplam</dt>
                            <dd><strong><%=ToplamTutar() %> TL</strong></dd>
                            <dt>Kargo Tutarı</dt>
                            <dd><%=0 %> TL</dd>
                            <dt class="total-price">Sipariş Tutarı</dt>
                            <dd class="total-price"><strong><%=ToplamTutar() %> TL</strong></dd>
                        </dl>
                    </section>
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <div class="col-sm-7 col-sm-pull-5">
                    <section class="checkout checkout-step-1 checkout-step-current element-emphasis-strong clearfix">
                        <h2 class="strong-header element-header">1. Fatura Adresi
                        </h2>
                        <div role="form" class="form">
                            <div class="form-group">
                                <label for="Kendi">Kendi adresiniz mi olsun</label>
                                <asp:DropDownList ID="ddlKendi" AutoPostBack="true" runat="server" class="form-control" Width="139px" OnSelectedIndexChanged="ddlKendi_SelectedIndexChanged">
                                    <asp:ListItem>Evet</asp:ListItem>
                                    <asp:ListItem>Hayır</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <%-- <div class="form-group">
                                <label for="country">Country</label>--%>

                            <%--</div>--%>
                            <div class="row" runat="server" id="FaturaAdres" visible="false">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="first-name">Ad</label>
                                        <input type="text" class="form-control" id="Ad" required>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="last-name">Soyad</label>
                                        <input type="text" class="form-control" id="Soyad" required>
                                    </div>
                                </div>

                                <%--<div class="form-group">
                                <label for="street-address">Adres</label>
                                <input type="text" class="form-control" id="street-address" placeholder="Street address" required>
                                <label for="apartment-number" class="sr-only">Apartment</label>
                                <input type="text" class="form-control" id="apartment-number" placeholder="Apartment, suite, unit etc. (optional)">
                            </div>--%>
                                <%--<div class="form-group">
                                <label for="city">City</label>
                                <input type="text" class="form-control" id="city" required>
                            </div>
                            <div class="form-group">
                                <label for="region">State/Province/Region <small class="explanation">(if applicable)</small></label>
                                <input type="text" class="form-control" id="region" required>
                            </div>--%>
                                <div class="form-group">
                                    <label for="sehir">İl Seçiniz</label>
                                    <asp:DropDownList ID="ddlSehir" runat="server" class="form-control" Width="139px" DataTextField="il" DataValueField="id" OnSelectedIndexChanged="ddlSehir_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                </div>
                                <div class="form-group">
                                    <label for="ilce">İlçe Seçiniz</label>
                                    <asp:DropDownList ID="ddlIlce" runat="server" class="form-control" Width="139px" DataTextField="ilce" DataValueField="id">
                                    </asp:DropDownList>
                                </div>

                                <div class="form-group">
                                    <label for="zip-code">Email</label>
                                    <input type="email" class="form-control" id="Email" required>
                                </div>

                                <div class="form-group">
                                    <label for="zip-code">Posta Kodu</label>
                                    <input type="number" class="form-control" id="PostaKod" required>
                                </div>
                                <div class="form-group">
                                    <label for="adres">Adres</label>
                                    <input type="text" class="form-control" id="Adres" required>
                                </div>
                                <%--<div class="form-group">
                                <label for="email">Email address</label>
                                <input type="email" class="form-control" id="email" required>
                            </div>--%>
                                <div class="form-group">
                                    <label for="phone">Cep Telefonu</label>
                                    <input type="number" class="form-control" id="CepTel" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Ev Telefonu</label>
                                    <input type="number" class="form-control" id="EvTel" required>
                                </div>
                                <%-- <asp:Button ID="btnDevam" runat="server" class="btn btn-primary " Text="Devam" OnClick="btnDevam_Click" />
                                <div class="clearfix"></div>--%>
                            </div>
                        </div>
                        <asp:Button ID="btnDevam" runat="server" class="btn btn-primary btn-pull-right " Text="Devam" OnClick="btnDevam_Click" />
                        <div class="clearfix"></div>
                        <%-- <button type="submit" class="btn btn-primary pull-right">Devam</button>--%>
                    </section>
                    <section class="checkout checkout-step-2 checkout-step-next element-emphasis-weak">
                        <h2 class="strong-header element-header">2. Kargo Adresi
                        </h2>
                    </section>
                    <section class="checkout checkout-step-3 checkout-step-next element-emphasis-weak">
                        <h2 class="strong-header element-header">3. Ödeme Seçenekleri
                        </h2>
                    </section>
                </div>
            </div>

        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->
</asp:Content>

