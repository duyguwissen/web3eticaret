﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="hakkimizda.aspx.cs" Inherits="hakkimizda" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    

    <section id="Content" role="main">
        <div class="container">


            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div><!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">
                            Hakkımızda
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="/">Anasayfa</a></li><!--
                            --><li>Sayfalar</li><!--
                            --><li>Hakkımızda</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div><!-- !full-width -->
         <asp:Repeater ID="rptHakkimizda" runat="server">
             <ItemTemplate>
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->


            <section class="row">
                <div class="section-header col-xs-12">
                   
                    <hr>
                    <h2 class="strong-header">
                        <%#Eval("hakkimizdaBaslik") %>
                    </h2>
                </div>
                <div class="col-md-6">
                    <p>
                        <%#birinciParagraf(Eval("hakkimizdaYazi"))%>
                       
                           
                    </p>
                </div>
                <div class="col-md-6">
                    <p>
                       <%=ikinciParagraf() %>
                    </p>
                </div>
            </section>


        <!-- !SECTION EMPHASIS 1 -->
        <!-- FULL WIDTH -->
        </div><!-- !container -->
        <div class="full-width section-emphasis-2">
            <div class="container">
                <section class="row">
                    <div class="section-header col-xs-12">
                        <hr>
                        <h2 class="strong-header">
                            <%#Eval("servislerBaslik") %>
                        </h2>
                    </div>
                    <div class="col-sm-3 text-center">
                        <!-- ICON BOX LONG -->
                        <div class="icon-box icon-box-long">
                            <i class="fa fa-lightbulb-o"></i>
                            <h4 class="strong-header"><%#Eval("servis1Baslik") %></h4>
                            <ul class="list-unstyled">
                                <%--<li>Ideation & concepts</li>
                                <li>Creative Strategy</li>
                                <li>Art direction</li>
                                <li>Branding & identity</li>--%>
                                <li><%#Eval("servis1Yazi") %></li>
                            </ul>
                        </div>
                        <!-- !ICON BOX LONG -->
                    </div>
                    <div class="clearfix visible-xs space-30"></div><!-- VIRTUAL ROW !IMPORTANT -->
                    <div class="col-sm-3 text-center">
                        <!-- ICON BOX LONG -->
                        <div class="icon-box icon-box-long">
                            <i class="fa fa-desktop"></i>
                            <h4 class="strong-header"><%#Eval("servis2Baslik") %></h4>
                            <ul class="list-unstyled">
                                <li><%#Eval("servis2Yazi") %></li>
                            </ul>
                        </div>
                        <!-- !ICON BOX LONG -->
                    </div>
                    <div class="clearfix visible-xs space-30"></div><!-- VIRTUAL ROW !IMPORTANT -->
                    <div class="col-sm-3 text-center">
                        <!-- ICON BOX LONG -->
                        <div class="icon-box icon-box-long">
                            <i class="fa fa-flask"></i>
                            <h4 class="strong-header"><%#Eval("servis3Baslik") %></h4>
                            <ul class="list-unstyled">
                                <li><%#Eval("servis3Yazi") %></li>
                            </ul>
                        </div>
                        <!-- !ICON BOX LONG -->
                    </div>
                    <div class="clearfix visible-xs space-30"></div><!-- VIRTUAL ROW !IMPORTANT -->
                    <div class="col-sm-3 text-center">
                        <!-- ICON BOX LONG -->
                        <div class="icon-box icon-box-long">
                          <i class="fa fa-bullhorn"></i>
                            <h4 class="strong-header"><%#Eval("servis4Baslik") %></h4>
                            <ul class="list-unstyled">
                                <li><%#Eval("servis4Yazi") %></li>
                            </ul>
                        </div>
                        <!-- !ICON BOX LONG -->
                    </div>
                </section>
            </div>
        </div><!-- !full-width --> 
                 </ItemTemplate>
        </asp:Repeater>
        <div class="container">
        <!-- !FULL WIDTH -->
        <!-- !SECTION EMPHASIS 1 -->


            <section class="row">
                <div class="section-header col-xs-12">
                    <hr>
                    <h2 class="strong-header">
                        Featured brands
                    </h2>
                </div>
                <div class="col-md-12 text-center logotypes">
                    <img src="/images/demo-content/brand-07.png" alt=" ">
                    <img src="/images/demo-content/brand-03.png" alt=" ">
                    <img src="/images/demo-content/brand-02.png" alt=" ">
                    <img src="/images/demo-content/brand-06.png" alt=" ">
                    <img src="/images/demo-content/brand-04.png" alt=" ">
                </div>
            </section>


        <!-- FULL WIDTH -->
        </div><!-- !container -->
        <div class="full-width section-emphasis-1">
            <div class="container"><!-- FLEXSLIDER -->
                <!-- STANDARD -->
                <section class="row">
                    <div class="section-header col-xs-12">
                        <hr>
                        <h2 class="strong-header">
                            What people say
                        </h2>
                    </div>
                    <div class="col-md-12">
                        <div class="flexslider flexslider-nopager row">
                            <ul class="slides">
                                <li>
                                    <div class="col-md-10 col-md-offset-1">
                                        <!-- TESTIMONIAL -->
                                        <blockquote class="testimonial">
                                            <p>
                                                “My relationship with Decima team has benefited me and my company beyond
                                                expectation.  The process that Decima team presided over helped transform
                                                our marketing from a series of haphazard tactics to a unified, recurring
                                                plan.”
                                            </p>
                                            <small>
                                                Denise Johnson, <cite>Montana's Cookhouse</cite>
                                            </small>
                                        </blockquote>
                                        <!-- !TESTIMONIAL -->
                                    </div>
                                </li>
                                <li>
                                    <div class="col-md-10 col-md-offset-1">
                                        <!-- TESTIMONIAL -->
                                        <blockquote class="testimonial">
                                            <p>
                                              “Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis auctor suscipit posuere. Morbi nulla nisi, tincidunt vel nulla et, ullamcorper aliquet elit. Donec ac velit vulputate, tincidunt erat ac.”
                                            </p>
                                            <small>
                                                Andrew Smith, <cite>ABC Company</cite>
                                            </small>
                                        </blockquote>
                                        <!-- !TESTIMONIAL -->
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
                <!-- !FLEXSLIDER -->
            </div>
        </div><!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->

            <section class="row">
                <div class="section-header col-xs-12">
                    <hr>
                    <h2 class="strong-header">
                        Meet the team
                    </h2>
                </div>
                <div class="col-md-4">
                    <!-- PERSON BOX -->
                    <div class="person-box">
                        <div class="overlay-wrapper">
                            <a href="#">
                                <img src="/images/demo-content/team-1.jpg"  alt="Shop item">
                            </a>
                            <div class="overlay-contents">
                                <div class="shop-item-actions">
                                    <a href="#" class="btn btn-primary">
                                        <span class="fb" aria-hidden="true"></span>
                                        <span class="sr-only">Facebook</span>
                                    </a><!--
                                    --><a href="#" class="btn btn-primary">
                                        <span class="tw" aria-hidden="true"></span>
                                        <span class="sr-only">Twitter</span>
                                    </a><!--
                                    --><a href="#" class="btn btn-primary">
                                        <span class="gp" aria-hidden="true"></span>
                                        <span class="sr-only">Google+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item-info-name-position">
                            <h4><a href="#">William Fisher</a></h4>
                            <span class="position">Chief executive officer</span><br>
                        </div>
                    </div>
                    <!-- !PERSON BOX -->
                </div>
                <div class="col-md-4">
                    <!-- PERSON BOX -->
                    <div class="person-box">
                        <div class="overlay-wrapper">
                            <a href="#">
                                <img src="/images/demo-content/team-2.jpg"  alt="Shop item">
                            </a>
                            <div class="overlay-contents">
                                <div class="shop-item-actions">
                                    <a href="#" class="btn btn-primary">
                                        <span class="fb" aria-hidden="true"></span>
                                        <span class="sr-only">Facebook</span>
                                    </a><!--
                                    --><a href="#" class="btn btn-primary">
                                        <span class="tw" aria-hidden="true"></span>
                                        <span class="sr-only">Twitter</span>
                                    </a><!--
                                    --><a href="#" class="btn btn-primary">
                                        <span class="in" aria-hidden="true"></span>
                                        <span class="sr-only">LinkedIn</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item-info-name-position">
                            <h4><a href="#">Christopher Stone</a></h4>
                            <span class="position">Creative director</span><br>
                        </div>
                    </div>
                    <!-- !PERSON BOX -->
                </div>
                <div class="col-md-4">
                    <!-- PERSON BOX -->
                    <div class="person-box">
                        <div class="overlay-wrapper">
                            <a href="#">
                                <img src="/images/demo-content/team-3.jpg"  alt="Shop item">
                            </a>
                            <div class="overlay-contents">
                                <div class="shop-item-actions">
                                    <a href="#" class="btn btn-primary">
                                        <span class="fb" aria-hidden="true"></span>
                                        <span class="sr-only">Facebook</span>
                                    </a><!--
                                    --><a href="#" class="btn btn-primary">
                                        <span class="pt" aria-hidden="true"></span>
                                        <span class="sr-only">Pinterest</span>
                                    </a><!--
                                    --><a href="#" class="btn btn-primary">
                                        <span class="gp" aria-hidden="true"></span>
                                        <span class="sr-only">Google+</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="item-info-name-position">
                            <h4><a href="#">Kimberly Lewis</a></h4>
                            <span class="position">Head of project management</span><br>
                        </div>
                    </div>
                    <!-- !PERSON BOX -->
                </div>
            </section>

        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div> <!-- fixes floating problems when mobile menu is visible -->



</asp:Content>

