﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hesabim_bilgilerim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null & !IsPostBack)
        {
            Uye uye = (Uye)Session["kullanici"];
            txtAd.Text = uye.ad;
            txtSoyad.Text = uye.soyad;
            txtEmail.Text = uye.email;
            txtYeniSifre.Text = uye.sifre;
            txtSifreTekrar.Text = uye.sifre;
        }
        else
        {
            Response.Write("<script>alert('Önce giriş yapmalısınız.');window.location.href='" + GetRouteUrl("anasayfa", null) + "'</script>");
        }
    }
    protected void btnGonder_Click(object sender, EventArgs e)
    {
        /*hesap bilgileri güncelleme*/
        BilgileriDegistir();

    }

    public void BilgileriDegistir()
    {

        Uye yeniuyeBilgi = (Uye)Session["kullanici"];
        yeniuyeBilgi.ad = txtAd.Text;
        yeniuyeBilgi.soyad = txtSoyad.Text;
        yeniuyeBilgi.email = txtEmail.Text;
        yeniuyeBilgi.sifre = txtYeniSifre.Text;

        if (!yeniuyeBilgi.KayitliMi())
        {
            int degistiMi = yeniuyeBilgi.BilgileriDuzenle(yeniuyeBilgi);
            if (degistiMi > 0)
            {
                Session["kullanici"] = yeniuyeBilgi;
                Response.Write("<script>alert('Başarıyla güncellenmiştir.');window.location.href='" + GetRouteUrl("hesapbilgi",null) + "'</script>");

            }
            else
            {
                Response.Write("<script>alert('hata oluştu.Daha sonra tekrar deneyin.')</script>");
            }

        }
        else
        {
            Response.Write("<script>alert('Email daha önceden kayıtlı')</script>");
        }



    }
    protected void lbtnCikis_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.RedirectToRoute("anasayfa", null);
    }
}