﻿<%@ Application Language="C#" %>

<script runat="server">

    private void SetRouteMaps()
    {
        //System.Web.Routing.RouteTable.Routes.MapPageRoute("anasayfa", "", "~/Default.aspx");        
        System.Web.Routing.RouteTable.Routes.MapPageRoute("anasayfa", "", "~/Index.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("giris", "Giris", "~/uyegiris.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("hakkimizda", "Hakkimizda", "~/Hakkimizda.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("iletisim", "Iletisim", "~/iletisim.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("sifremiunuttum", "SifremiUnuttum", "~/SifremiUnuttum.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("kaydol", "Kaydol", "~/uyekayit.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("hesap", "Hesabim", "~/hesabim.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("sss", "SıkSorulanlar", "~/sss.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("bossepet", "Sepetiniz", "~/sepet-empty.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("dolusepet", "Sepetinizdekiler", "~/sepet-full.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("faturabilgileri", "FaturaBilgileri", "~/odeme.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("teslimatbilgileri", "TeslimatBilgileri", "~/odeme2.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("odemebilgileri", "OdemeBilgileri", "~/odeme3.aspx");
        //System.Web.Routing.RouteTable.Routes.MapPageRoute("Hesap", "Hesabım", "~/hesabim.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("hesapbilgi", "HesapBilgileri", "~/hesabim_bilgilerim.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("adres", "AdresBilgileri", "~/hesabim_adreslerim.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("siparisler", "Siparislerim", "~/hesabim_siparislerim.aspx");
        
        //System.Web.Routing.RouteTable.Routes.MapPageRoute("OdemeBilgileri", "OdemeBilgileri", "~/odeme3.aspx");        
               
        
        //System.Web.Routing.RouteTable.Routes.MapPageRoute("hesap", "", "~/hesabim.aspx");


        //System.Web.Routing.RouteTable.Routes.MapPageRoute("Default", "{controller}/{action}/{id}", new { controller = "Home", action = "Index", id = UrlParameter.Optional });
        //System.Web.Routing.RouteTable.Routes.MapPageRoute("Kategoriler", "kategoriler", "~/Anasayfa.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("urunIdyeGetir", "urun/{urunId}", "~/urundetay.aspx");
        System.Web.Routing.RouteTable.Routes.MapPageRoute("AramaGetir", "urunArama/{arama}", "~/urunler.aspx");        
        System.Web.Routing.RouteTable.Routes.MapPageRoute("cinsiyeteGoreGetir", "{cinsiyet}Urunleri", "~/urunler.aspx");        
        System.Web.Routing.RouteTable.Routes.MapPageRoute("kategoriIdIleUrunler", "urunler/{kategori}", "~/urunler.aspx");        
        //System.Web.Routing.RouteTable.Routes.MapPageRoute("SehirBazliSiraliMusteriler", "musteriler/{City}$orderby={FieldName}", "~/musteri.aspx");
        //System.Web.Routing.RouteTable.Routes.MapPageRoute("SehirBazliSiparisler", "siparisler/{ShipCity}", "~/siparis.aspx");
    }


    void Application_Start(object sender, EventArgs e) 
    {
        SetRouteMaps();
        
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        // Code that runs when a new session is started

    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
