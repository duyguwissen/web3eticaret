﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="urundetay.aspx.cs" Inherits="urundetay" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="/js/chosen.jquery.min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section id="Content" role="main">

        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header page-header-short">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="/">Anasayfa</a></li>
                            <!--
                                        -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                                        -->
                            <li><a href="03-shop-products.html#.dress">Dresses</a></li>
                            <!--
                                        -->
                            <li>
                                <asp:Label ID="lblUrun" runat="server" Text=""></asp:Label>

                            </li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <article class="row shop-product-single">

                <div class="col-md-6 space-right-20">

                    <!-- thumbnailSlider -->
                    <div class="thumbnailSlider">
                        <div class="flexslider flexslider-thumbnails">
                            <ul class="slides">
                                <asp:Repeater ID="rptResimler" runat="server">
                                    <ItemTemplate>
                                        <li>
                                            <a href="/Resim/buyuk/<%#Eval("resimUrl") %>" data-rel="prettyPhotoGallery[product]">
                                                <img src="/Resim/buyuk/<%#Eval("resimUrl") %>" alt=" ">
                                            </a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <%-- <li>
                                    <a href="/images/demo-content/product-single-large-2.jpg" data-rel="prettyPhotoGallery[product]">
                                        <img src="/images/demo-content/product-single-2.jpg" alt=" ">
                                    </a>
                                </li>
                                <li>
                                    <a href="/images/demo-content/product-single-large-3.jpg" data-rel="prettyPhotoGallery[product]">
                                        <img src="/images/demo-content/product-single-3.jpg" alt=" ">
                                    </a>
                                </li>
                                <li>
                                    <a href="/images/demo-content/product-single-large-4.jpg" data-rel="prettyPhotoGallery[product]">
                                        <img src="/images/demo-content/product-single-4.jpg" alt=" ">
                                    </a>
                                </li>--%>
                            </ul>
                        </div>

                        <ul class="smallThumbnails clearfix">
                            <asp:Repeater ID="rptKucukResimler" runat="server">
                                <ItemTemplate>
                                    <li data-target="0" class="active">
                                        <img src="/Resim/kucuk/<%#Eval("resimUrl") %>" alt=" ">
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                            <%-- <li data-target="1">
                                <img src="/images/demo-content/product-single-thumbnail-2.jpg" alt=" ">
                            </li>
                            <li data-target="2">
                                <img src="/images/demo-content/product-single-thumbnail-3.jpg" alt=" ">
                            </li>
                            <li data-target="3">
                                <img src="/images/demo-content/product-single-thumbnail-4.jpg" alt=" ">
                            </li>--%>
                        </ul>
                    </div>
                    <!-- / thumbnailSlider -->

                </div>




                <div class="clearfix visible-sm visible-xs space-30"></div>
                <div class="col-md-6 space-left-20">
                    <header>

                        <span class="rating" data-score="<%=Puanla() %>" runat="server" id="puanlama"></span>
                        <a href="#reviews">
                            <asp:Label ID="lblYorumSayisi" runat="server" Text=""></asp:Label>
                        </a>
                        <a href="#reviews">Yorum Yap</a>

                        <h1>
                            <asp:Label ID="lblUrunAd" runat="server" Text=""></asp:Label>
                        </h1>

                        <span class="product-code">
                            <asp:Label ID="lblUrunKod" runat="server" Text="Ürün Kodu: "></asp:Label></span>

                        <br>

                        <br>
                        <span class="price-old">
                            <asp:Label ID="lblPriceOld" runat="server" Text=""></asp:Label>&nbsp;&nbsp;TL</span>&nbsp;&nbsp;
                        <span class="price">
                            <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>&nbsp;&nbsp;TL</span>&nbsp;&nbsp;
                      
                    </header>
                    <div role="form" class="shop-form form-horizontal form">
                        <%--<div class="form-group">
    <label class="col-xs-2" for="color">Color</label>

    <div class="col-xs-5">
      <select class="chosen chosen-select-searchless" id="color" data-placeholder=" ">
        <option value="Vintage blue">Vintage blue</option>
        <option value="Red">Red</option>
        <option value="Green">Green</option>
      </select>
    </div>
  </div>--%>
                        <%--<div class="form-group">
    <label class="col-xs-2" for="size">Size</label>

    <div class="col-xs-5">
      <select class="chosen chosen-select-searchless" id="size" data-placeholder=" ">
        <option value="XS">XS</option>
        <option value="S">S</option>
        <option value="M">M</option>
        <option value="L">L</option>
        <option value="XL">XL</option>
      </select>
    </div>
    <div class="col-xs-3">
      <a href="#SizeGuide" class="size-guide-toggle">Size guide</a>
    </div>
    <div class="clearfix visible-xs visible-sm"></div>
  </div>--%>

                        <%--<asp:Repeater ID="rptOzellikler" runat="server" OnItemDataBound="rptOzellikler_ItemDataBound">
                            <ItemTemplate>
                                <div class="form-group">
                                    <label class="col-xs-2" for="color"><%#Eval("ozellikAdi") %></label>

                                    <div class="col-xs-5">
                                        <select class="chosen chosen-select-searchless" id="color" data-placeholder=" ">
                                            <asp:Repeater ID="rptOzellikDetayi" runat="server">
                                                <ItemTemplate>
                                                    <option value="<%#Eval("ozellikDetayi") %>"><%#Eval("ozellikDetayi") %></option>
                                                    
                                                </ItemTemplate>
                                            </asp:Repeater>
                                        </select>
                                    </div>

                                </div>
                            </ItemTemplate>
                        </asp:Repeater>--%>

                        <div class="form-group">
                            <label class="col-xs-2" for="color">Beden</label>

                            <div class="col-xs-5">
                                <%--<select class="chosen chosen-select-searchless" id="color" data-placeholder=" ">--%>
                                  <%--  <asp:Repeater ID="rptBeden" runat="server">
                                        <ItemTemplate>--%>
                                            <%--<option value="<%#Eval("ozellikDetayi") %>"><%#Eval("ozellikDetayi") %></option>--%>
                                      <%--  </ItemTemplate>
                                    </asp:Repeater>--%>
                                    <asp:DropDownList ID="ddlBeden" runat="server" Height="41px" Width="193px"></asp:DropDownList>

                                <%--</select>--%>
                            </div>

                        </div>

                        <div class="size-guide-wrapper form-group visible-xs visible-sm">
                            <div class="col-xs-12">
                                <div id="SizeGuide">
                                    <div class="table">
                                        <table>
                                            <thead>
                                                <tr>
                                                    <td></td>
                                                    <td>XXS</td>
                                                    <td>XS</td>
                                                    <td>S</td>
                                                    <td>M</td>
                                                    <td>L</td>
                                                    <td>XL</td>
                                                    <td>XXL</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>Bust</th>
                                                    <td>78</td>
                                                    <td>82</td>
                                                    <td>86</td>
                                                    <td>90</td>
                                                    <td>96</td>
                                                    <td>103</td>
                                                    <td>110</td>
                                                </tr>
                                                <tr>
                                                    <th>Waist</th>
                                                    <td>60</td>
                                                    <td>64</td>
                                                    <td>68</td>
                                                    <td>72</td>
                                                    <td>78</td>
                                                    <td>85</td>
                                                    <td>92</td>
                                                </tr>
                                                <tr>
                                                    <th>Hips</th>
                                                    <td>86</td>
                                                    <td>90</td>
                                                    <td>94</td>
                                                    <td>98</td>
                                                    <td>104</td>
                                                    <td>111</td>
                                                    <td>118</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-2" for="quantity">Adet</label>

                            <div class="col-xs-2">
                                <input class="form-control spinner-quantity" id="quantity" required runat="server">
                            </div>
                        </div>
                        <%--<button type="submit" class="btn btn-primary">Sepete Ekle</button>--%>
                        <asp:Button ID="btnSepeteEkle" runat="server" Text="Sepete Ekle" class="btn btn-primary" OnClick="btnSepeteEkle_Click" />
                        <!--
                            -->
                        <button type="button" class="btn btn-default">İstek Listesine Ekle</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="shop-product-single-social">
                        <span class="social-label pull-left">Ürünü Paylaş</span>

                        <div class="social-widget social-widget-mini social-widget-dark">
                            <ul class="list-inline">
                                <li>
                                    <a href="https://www.facebook.com/sharer/sharer.php?u=http://www.createit.pl"
                                        onclick="window.open(this.href, 'facebook-share','width=580,height=296'); return false;"
                                        rel="nofollow"
                                        title="Facebook"
                                        class="fb">
                                        <span class="sr-only">Facebook</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://twitter.com/share?text=CreateIT&amp;url=http://www.createit.pl"
                                        onclick="window.open(this.href, 'twitter-share', 'width=550,height=235'); return false;"
                                        rel="nofollow"
                                        title=" Share on Twitter"
                                        class="tw">

                                        <span class="sr-only">Twitter</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://plus.google.com/share?url=http://www.createit.pl"
                                        onclick="window.open(this.href, 'google-plus-share', 'width=490,height=530'); return false;"
                                        rel="nofollow"
                                        title="Google+"
                                        class="gp">
                                        <span class="sr-only">Google+</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.pinterest.com/pin/create/button/?url=http://www.createit.pl/&amp;media=http://www.createit.pl/images/frontend/logo.png&amp;description=CreateIT"
                                        onclick="window.open(this.href, 'pinterest-share', 'width=770,height=320'); return false;"
                                        rel="nofollow"
                                        title="Pinterest"
                                        class="pt">
                                        <span class="sr-only">Pinterest</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://developer.linkedin.com&amp;title=LinkedIn%20Developer%20Network&amp;summary=My%20favorite%20developer%20program&amp;source=LinkedIn"
                                        onclick="window.open(this.href, 'linkedin-share', 'width=600,height=439'); return false;"
                                        rel="nofollow"
                                        title="LinkedIn" class="in">
                                        <span class="sr-only">LinkedIn</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#description" data-toggle="tab">Açıklama</a></li>
                            <li><a href="#info" data-toggle="tab">Additional info</a></li>
                            <li><a href="#reviews" data-toggle="tab">
                                <asp:Label ID="lblYorum" runat="server" Text=""></asp:Label></a></li>

                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="description">
                                <p>
                                    Directional, exciting and diverse, the Decima makes and breaks the fashion rules.
        Scouring the globe for inspiration, our London based Design Team is inspired by
        fashion's most covetable trends; providing you with a cutting edge wardrobe season
        upon season
                                </p>
                                <ul>
                                    <li>Made from pure cotton denim</li>
                                    <li>Classic pinafore styling</li>
                                    <li>Pull-through button fastening to the front</li>
                                    <li>Patch pocket detailing to front</li>
                                    <li>Waist pockets</li>
                                    <li>Pleated skirt</li>
                                    <li>Vintage wash finish</li>
                                    <li>RegulaRegular fit
                                    </li>
                                </ul>
                            </div>
                            <div class="tab-pane fade" id="info">
                                <div class="table table-condensed">
                                    <table>
                                        <tbody>
                                            <tr>
                                                <th class="weak width25">
                                                Weight
                                               
                                                <td>1 kg</td>
                                            </tr>
                                            <tr>
                                                <th class="weak">Dimensions</th>
                                                <td>40x20x5 cm</td>
                                            </tr>
                                            <tr>
                                                <th class="weak">Composition</th>
                                                <td>100% cotton</td>
                                            </tr>
                                            <tr>
                                                <th class="weak">Size &amp; fit</th>
                                                <td>Regular fit</td>
                                            </tr>
                                            <tr>
                                                <th class="weak">Care</th>
                                                <td>Machine wash according to instructions on care label</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <section class="tab-pane fade" id="reviews">
                                <asp:Repeater ID="rptYorumlar" runat="server">
                                    <ItemTemplate>
                                        <article class="review">
                                            <header>
                                                <span class="rating" data-score="<%#Eval("puan") %>"></span>

                                                <br>
                                                <h4 class="author"><%#Eval("ad")%>&nbsp;<%#Eval("soyad") %></h4>
                                                <span class="date"><%--Aug 7, 2013--%><%#Eval("yorumTarih") %></span>
                                            </header>
                                            <p>
                                                <%#Eval("yorum") %>
                                            </p>
                                        </article>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <%--<article class="review">
                                    <header>
                                        <span class="rating" data-score="3"></span>
                                        <br>
                                        <h4 class="author">Richard Doe</h4>
                                        <span class="date">Aug 3, 2013</span>
                                    </header>
                                    <p>
                                        Leather jacket pastels backpack neutral green white. Strong eyebrows washed out
          Chanel. leggings skinny jeans Missoni capsule clutch cotton.
                                    </p>
                                </article>--%>
                                <div class="review-form">
                                    <label class="raty-label">
                                        Ürüne Puan Ver<br>
                                        <span class="rate"></span>
                                    </label>

                                    <div class="form-group">
                                        <label for="review">Senin Yorumun</label>
                                        <textarea class="form-control" id="review" name="review" rows="6"></textarea>
                                    </div>
                                    <%--<button type="submit" class="btn btn-primary">Yorumu Gönder</button>--%>
                                    <asp:Button ID="btnYorumGonder" runat="server" Text="Yorumu Gönder" class="btn btn-primary" OnClick="btnYorumGonder_Click" />
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </article>


            <script type="text/javascript">
                $(document).ready(function () {
                    $("span.rate img").click(function () {
                        var rate = $(this).attr("alt");
                        $.ajax({
                            url: 'urundetay.aspx?rate=' + rate + '&urunID=<%=urunId%>'
                        });
                    });

                });

            </script>

        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->



</asp:Content>

