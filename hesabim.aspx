﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="hesabim.aspx.cs" Inherits="hesabim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #ContentPlaceHolder1_FaturaAdres {
            display: none;
            background-color: white;
            border: 2px solid black;
            z-index: 10;
            left: 275px;
            top: 275px;
            position: absolute;
            width: 300px;
            height: 200px;
            padding: 10px !important;
            overflow-y: scroll;
        }

        #ContentPlaceHolder1_TeslimatAdres {
            display: none;
            background-color: white;
            border: 2px solid black;
            z-index: 10;
            top: 275px;
            left: 675px;
            position: absolute;
            width: 300px;
            height: 200px;
            padding: 10px !important;
            overflow-y: scroll;
        }

        #SifreDegistirme {
            display: none;
            background-color: white;
            border: 2px solid black;
            z-index: 10;
            left: 275px;
            top: 150px;
            position: absolute;
            width: 250px;
            height: 270px;
            padding: 10px !important;
            overflow-y: scroll;
        }
    </style>
    <script type="text/javascript">
        function EkraniKarart() {
            $("body").append('<div id="popup_overlay"></div>');
            $("#popup_overlay").css({
                position: 'absolute',
                zIndex: 0,
                top: '0px',
                left: '0px',
                width: '100%',
                height: $(document).height(),
                //background: '#000',
                opacity: .2
            });
        }

        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }

        function SifreKapat(obj) {
            $("#popup_overlay").remove();
            $('#SifreDegistirme').css("display", "none");
            e.preventDefault();

        }

        function FaturaKapat(obj) {
            //var id = obj.getAttribute("id");

            $("#popup_overlay").remove();
            $('#ContentPlaceHolder1_FaturaAdres').css("display", "none");
            e.preventDefault();

        }
        function SifreDegisim() {
            $('#SifreDegistirme').css("display", "inline");
            EkraniKarart();
        }
        function TeslimatKapat(obj) {
            //var id = obj.getAttribute("id");

            $("#popup_overlay").remove();
            $('#ContentPlaceHolder1_TeslimatAdres').css("display", "none");
            e.preventDefault();

        }
        function DuzenleSayfasiGetir(obj) {
            var id = obj.getAttribute("id");

            //var top = obj.offsetTop;
            //var divtop = $('.row').offset().top;

            /*ekran karartılıyor.*/
            $("body").append('<div id="popup_overlay"></div>');
            $("#popup_overlay").css({
                position: 'absolute',
                zIndex: 0,
                top: '0px',
                left: '0px',
                width: '100%',
                height: $(document).height(),
                //background: '#000',
                opacity: .2
            });
            if (id == "btnFaturaDuzenle" | id == "ContentPlaceHolder1_ddlFaturaSehir") {
                //var left = obj.offsetLeft;
                //$('#ContentPlaceHolder1_FaturaAdres').css("top", top + divtop+40 + "px");
                //$('#ContentPlaceHolder1_FaturaAdres').css("left", left+265 + "px");
                $('#ContentPlaceHolder1_FaturaAdres').css("display", "inline");

                $("#ContentPlaceHolder1_FaturaAdres").slideDown("normal");


                //document.getElementById('ContentPlaceHolder1_hfAdres').value = "Fatura";

                $('#ContentPlaceHolder1_FaturaAdres').css("background-color", "white");
                $('#ContentPlaceHolder1_FaturaAdres').css("opacity", 1);
                document.getElementById('btnFaturaDuzenle').focus();

            }
            else if (id == "btnTeslimatDuzenle" | id == "ContentPlaceHolder1_ddlTeslimatSehir") {
                //var left = obj.offsetLeft;
                //$('#ContentPlaceHolder1_TeslimatAdres').css("top", top + divtop + 40 + "px");
                //$('#ContentPlaceHolder1_TeslimatAdres').css("left", left + 665 + "px");
                $('#ContentPlaceHolder1_TeslimatAdres').css("display", "inline");

                $("#ContentPlaceHolder1_TeslimatAdres").slideDown("normal");

                $('#ContentPlaceHolder1_TeslimatAdres').css("background-color", "white");
                $('#ContentPlaceHolder1_TeslimatAdres').css("opacity", 1);
                document.getElementById('btnTeslimatDuzenle').focus();
            }

        }
        $(document).ready(function () {
            //var prm = Sys.WebForms.PageRequestManager.getInstance();
            //prm.add_endRequest(function (s, e) {
            //    alert('Postback!');
            //});
            var buttonTop = $('#ContentPlaceHolder1_btnTumSiparisler').offset().top;
            var divTop = $('.divv').offset().top;
            //var buttonTop = document.getElementById('#ContentPlaceHolder1_btnTumSiparisler').offsetTop();
            $('.divv').css("margin-bottom", buttonTop - divTop - 25 + "px");
            $("#ContentPlaceHolder1_btnFaturaDuzene").click(function () {

                var top = this.offsetTop;
                var divtop = $('.row').offset().top;
                $('#ContentPlaceHolder1_Adres').css("top", top + divtop + "px");
                $('#ContentPlaceHolder1_Adres').css("display", "inline");

                $("#ContentPlaceHolder1_FaturaAdres").slideDown("normal");

                /*ekran karartılıyor.*/
                $("body").append('<div id="popup_overlay"></div>');
                $("#popup_overlay").css({
                    position: 'absolute',
                    //zIndex: 99998,
                    top: '0px',
                    left: '0px',
                    width: '100%',
                    height: $(document).height()
                    //,
                    //background: '#000',
                    //opacity: .2
                });
                $('#ContentPlaceHolder1_FaturaAdres').css("background-color", "white");
                $('#ContentPlaceHolder1_FaturaAdres').css("opacity", 1);

            });

            $("#FaturaKapatt").click(function (e) {
                $("#popup_overlay").remove();
                $('#ContentPlaceHolder1_FaturaAdres').css("display", "none");
                e.preventDefault();


            });

            $("body").keydown(function (e) {
                console.log();
                if (e.keyCode == 27) {
                    $("#popup_overlay").remove();
                    $('#ContentPlaceHolder1_FaturaAdres').css("display", "none");
                    $('#ContentPlaceHolder1_TeslimatAdres').css("display", "none");
                    $('#SifreDegistirme').css("display", "none");
                }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section id="Content" role="main" class="divv">
        <div class="container">

            <asp:ScriptManager ID="ScriptManager1" runat="server" />
            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">

            <div class="container">

                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Hesabım
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li>
                                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="<%$RouteUrl:RouteName=anasayfa%>">Anasayfa</asp:HyperLink>

                            </li>
                            <!--
                          -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                          -->
                            <li>Hesabım</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <section id="div" class="row" style="position: absolute;">

                <div class="col-sm-3">
                    <nav class="shop-section-navigation element-emphasis-weak">
                        <ul class="list-unstyled">
                            <li class="active"><span>Hesap Kontrol Paneli</span></li>
                            <li>
                                <asp:HyperLink ID="hlHesap" runat="server" NavigateUrl="<%$RouteUrl:RouteName=hesapbilgi%>">Hesap Bilgileri</asp:HyperLink>

                            </li>
                            <li>
                                <asp:HyperLink ID="hlUrunler" runat="server" NavigateUrl="<%$RouteUrl:RouteName=siparisler%>">Ürünlerim</asp:HyperLink></li>
                            <li>
                                <asp:HyperLink ID="hlAdres" runat="server" NavigateUrl="<%$RouteUrl:RouteName=adres%>">Adres Defteri</asp:HyperLink></li>

                            <li>
                                <asp:LinkButton ID="lbtnCikis" runat="server" OnClick="lbtnCikis_Click">Çıkış</asp:LinkButton></li>
                        </ul>
                    </nav>
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <div class="col-sm-9 space-left-30">
                    <h2 class="strong-header large-header">Kontrol Panelim</h2>
                    <p>
                        From your account dashboard you have the ability to view a snapshot of your recent account activity
                      and update your account information.
                    </p>
                    <hr>
                    <h3 class="strong-header">Hesap Bilgileri</h3>
                    <p>
                        <br>
                        <asp:Label ID="lblUyeAd" runat="server" Text=""></asp:Label><br>
                        <asp:Label ID="lblUyeEmail" runat="server" Text=""></asp:Label><br>
                        <a href="javascript:SifreDegisim();" id="Sifre">Şifreni Değiştir</a><br>
                        <br>
                    </p>
                    <%--<a href="09-b-shop-account-information.html" class="btn btn-default btn-small">Edit</a>--%>
                    <hr>
                    <div class="row">
                        <div class="col-sm-6">

                            <h3 class="strong-header">Fatura Adresi</h3>
                            <p>
                                <br>
                                <asp:Label ID="lblIsim" runat="server" Text="Adres belirtilmedi."></asp:Label><br>
                                <asp:Label ID="lblFaturaAdres" runat="server" Text=""></asp:Label><br>
                                <asp:Label ID="lblFaturaIlcePostaKod" runat="server" Text=""></asp:Label><br>
                                <asp:Label ID="lblFaturaIl" runat="server" Text=""></asp:Label><br>
                                <br>
                            </p>

                            <%--<asp:Button ID="btnFaturaDuzene" runat="server" Text="Düzenle" onClick="btnFaturaDuzene_Click" class="btn btn-default btn-small  duzenle" />--%>

                            <input type="button" id="btnFaturaDuzenle" value="Düzenle" onclick="DuzenleSayfasiGetir(this);" class="btn btn-default btn-small  duzenle" />
                            <hr class="visible-xs">
                        </div>
                        <div class="col-sm-6">
                            <h3 class="strong-header">Teslimat Adresi</h3>
                            <p>
                                <br>
                                <asp:Label ID="lblIsim2" runat="server" Text="Adres belirtilmedi."></asp:Label><br>
                                <asp:Label ID="lblTeslimatAdres" runat="server" Text=""></asp:Label><br>
                                <asp:Label ID="lblTeslimatIlce" runat="server" Text=""></asp:Label><br>
                                <asp:Label ID="lblTeslimatIl" runat="server" Text=""></asp:Label><br>
                                <br>
                            </p>
                            <%--<a href="#" class="btn btn-default btn-small duzenle">Düzenle</a>--%>
                            <input type="button" id="btnTeslimatDuzenle" value="Düzenle" onclick="DuzenleSayfasiGetir(this);" class="btn btn-default btn-small  duzenle" />
                        </div>
                    </div>
                    <hr>
                    <h3 class="strong-header">Son Siparişler</h3>
                    <div class="table table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <td class="width20">Sipariş No. </td>
                                    <td>Sipariş Tarihi</td>
                                    <td>Toplam Tutar </td>
                                    <td>Durum </td>
                                    <td class="text-right width13">Detaylar </td>
                                </tr>
                            </thead>
                            <tbody>
                                <asp:Repeater ID="rptSiparisler" runat="server" OnItemDataBound="rptSiparisler_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td><a href="#"><strong><%#Eval("siparisId") %></strong></a></td>
                                            <td><%#TarihDuzenle(Eval("siparisTarih")) %></td>
                                            <td><%#Eval("toplamTutar") %></td>
                                            <td><%#Eval("siparisDurum") %></td>
                                            <td class="text-right"><a href="#">Görüntüle</a></td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <%-- <tr>
                                    <td><a href="#"><strong>8077</strong></a></td>
                                    <td>Jul 25, 2013</td>
                                    <td>$140.00</td>
                                    <td>Shipped</td>
                                    <td class="text-right"><a href="#">View</a></td>
                                </tr>
                                <tr>
                                    <td><a href="#"><strong>8007</strong></a></td>
                                    <td>Jul 28, 2013</td>
                                    <td>$47.00</td>
                                    <td>Cancelled</td>
                                    <td class="text-right"><a href="#">View</a></td>
                                </tr>
                                <tr>
                                    <td><a href="#"><strong>8007</strong></a></td>
                                    <td>Jul 28, 2013</td>
                                    <td>$47.00</td>
                                    <td>Cancelled</td>
                                    <td class="text-right"><a href="#">View</a></td>
                                </tr>
                                <tr>
                                    <td><a href="#"><strong>8007</strong></a></td>
                                    <td>Jul 28, 2013</td>
                                    <td>$47.00</td>
                                    <td>Cancelled</td>
                                    <td class="text-right"><a href="#">View</a></td>
                                </tr>--%>
                            </tbody>
                        </table>
                    </div>
                    <%--<a href="09-c-shop-account-my-orders.html" class="btn btn-default btn-small">View all orders</a>--%>
                    <asp:Button ID="btnTumSiparisler" runat="server" Text="Tüm Siparişleri Görüntüle" class="btn btn-default btn-small" OnClick="btnTumSiparisler_Click" />
                </div>
                <div id="SifreDegistirme">
                    <div class="form-group">
                    </div>
                    <div>
                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Şifreler Uyuşmuyor" ControlToCompare="txtYeniTekrar" ControlToValidate="txtYeniSifre" ForeColor="Red"></asp:CompareValidator>
                        <a href="javascript:SifreKapat(this);window.print();" id="SifreKapat" style="float: right;">Kapat</a>

                    </div>
                    <div>
                        <label for="first-name">Mevcut Şifre</label>

                        <asp:TextBox ID="txtEskiSifre" runat="server" TextMode="Password" class="form-control" EnableViewState="False"></asp:TextBox>
                    </div>
                    <div>
                        <label for="first-name">Yeni Şifre</label>

                        <asp:TextBox ID="txtYeniSifre" runat="server" TextMode="Password" class="form-control" EnableViewState="False"></asp:TextBox>
                    </div>
                    <div>
                        <label for="first-name">Yeni Şifre Tekrar</label>

                        <asp:TextBox ID="txtYeniTekrar" runat="server" TextMode="Password" class="form-control" EnableViewState="False"></asp:TextBox>
                    </div>
                    <div>

                        <asp:Button ID="btnSifreDegitir" class="btn btn-default btn-small" runat="server" Text="Gönder" OnClick="btnSifreDegitir_Click" />
                    </div>

                </div>
                <div id="FaturaAdres" runat="server">

                    <div class="form-group">
                        <%--<button id="FaturaKapat"  onclick="" value="Kapat" style="float:right;" ></button>--%>
                        <%--<input id="FaturaKapat"  onclick='Kapat(this)'  value="Kapat" type="button" style="float:right;border:none" />--%>
                        <a href="javascript:FaturaKapat(this);window.print();" id="FaturaKapat" style="float: right;">Kapat</a>
                    </div>
                    <div class="form-group">
                        <h3 class="strong-header">Fatura Adresi Düzenle</h3>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Ad Soyad</label>

                        <asp:TextBox ID="txtFaturaAdSoyad" runat="server" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="first-name">İl</label>
                        <asp:DropDownList ID="ddlFaturaSehir" AutoPostBack="true" OnSelectedIndexChanged="ddlFaturaSehir_SelectedIndexChanged" DataTextField="il" DataValueField="id" runat="server" class="form-control" Width="141px"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="first-name">İlçe</label>
                        <asp:DropDownList ID="ddlFaturaIlce" runat="server" DataTextField="ilce" DataValueField="id" class="form-control" Width="146px"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Posta Kodu</label>

                        <asp:TextBox ID="txtFaturaPosta" runat="server" type="number" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Cep Telefonu</label>

                        <asp:TextBox ID="txtFaturaCep" runat="server" onkeypress="return isNumberKey(event)" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Ev Telefonu</label>

                        <asp:TextBox ID="txtFaturaEvTel" runat="server" type="number" class="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Adres Detayı</label>

                        <asp:TextBox ID="txtFaturaAdres" runat="server" class="form-control"></asp:TextBox>
                        <asp:HiddenField ID="hfFaturaAdres" runat="server" />
                    </div>
                    <div class="form-group">
                        <asp:LinkButton ID="lbtnFaturaKaydet" runat="server" OnClick="lbtnFaturaKaydet_Click">Kaydet</asp:LinkButton>
                    </div>
                </div>
                <div id="TeslimatAdres" runat="server">

                    <div class="form-group">
                        <%--<input id="TeslimatKapat" type="button" onclick="" value="Kapat" />--%>
                        <a href="javascript:TeslimatKapat(this);window.print();" id="TeslimatKapat" style="float: right;">Kapat</a>
                    </div>
                    <div class="form-group">
                        <h3 class="strong-header">Teslimat Adresi Düzenle</h3>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Ad Soyad</label>

                        <asp:TextBox ID="txtTeslimatAd" runat="server" class="form-control" required></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Şehir</label>
                        <asp:DropDownList ID="ddlTeslimatSehir" AutoPostBack="true" OnSelectedIndexChanged="ddlTeslimatSehir_SelectedIndexChanged" runat="server" DataTextField="il" DataValueField="id" class="form-control" Width="141px"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="first-name">İlçe</label>
                        <asp:DropDownList ID="ddlTeslimatIlce" runat="server" DataTextField="ilce" DataValueField="id" class="form-control" Width="146px"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Posta Kodu</label>

                        <asp:TextBox ID="txtTeslimatPosta" runat="server" type="number" class="form-control" required></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Cep Telefonu</label>

                        <asp:TextBox ID="txtTeslimatCep" runat="server" class="form-control" type="number" required></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Ev Telefonu</label>

                        <asp:TextBox ID="txtTeslimatEvTel" runat="server" class="form-control" type="number" required></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label for="first-name">Adres Detayı</label>

                        <asp:TextBox ID="txtTeslimatAdres" runat="server" class="form-control" required></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:LinkButton ID="lbtnTeslimatKaydet" OnClick="lbtnTeslimatKaydet_Click" runat="server">Kaydet</asp:LinkButton>

                    </div>
                </div>

            </section>
        </div>

    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->

</asp:Content>

