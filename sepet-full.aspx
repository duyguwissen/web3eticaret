﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="sepet-full.aspx.cs" Inherits="sepet_full" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" type="text/css" href="/css/chosen.css">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">




    <section id="Content" role="main">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Sepet
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="index.html">Home</a></li>
                            <!--
                        -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                        -->
                            <li>Shopping cart</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <section class="row">
                <div>
                    <div class="col-xs-12">
                        <div class="table table-responsive cart-summary">
                            <table>
                                <thead>
                                    <tr>
                                        <td>Ürün</td>
                                        <td class="width16">Seçenekler</td>
                                        <td class="width16">Adet</td>
                                        <td class="text-right width16">Alt Toplam</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <asp:Repeater ID="rptUrunler" runat="server" OnItemCommand="rptUrunler_ItemCommand">
                                        <ItemTemplate>
                                            <tr>
                                                <td>
                                                    <a href="#">
                                                        <img src="/Resim/kucuk/<%#Eval("urun.ResimUrl") %>" alt="Shop item">
                                                    </a>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                <td>
                                                    <h4><a href="#"><%#Eval("urun.urunAd") %></a></h4>
                                                    <span class="price"><%#Eval("urun.urunFiyat") %> TL</span>
                                                    <br>
                                                    <br>
                                                  
                                        <%--<a href="#">Kaldır</a>--%>
                                                    <asp:LinkButton ID="lbtnKaldir" runat="server" CommandArgument="<%# Container.ItemIndex %>" OnClick="lbtnKaldir_Click">Kaldır</asp:LinkButton>
                                                </td>
                                                <td>
                                                    <p class="features">
                                                        Renk: <strong><%#Eval("urun.Renk") %></strong><br>
                                                        Beden: <strong><%#Eval("urun.urunDetayi.ozellikDetayi") %></strong>
                                                    </p>
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-control spinner-quantity" id="quantity1" required>
                                                </td>
                                                <td class="text-right">
                                                    <strong><%#Eval("urun.urunYeniFiyat") %> TL</strong>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                    <%--<tr>
                                    <td>
                                        <a href="#">
                                            <img src="/images/demo-content/cart-purchased-2.jpg" alt="Shop item">
                                        </a>
                                    </td>
                                    <td>
                                        <h4><a href="#">Denim Jacket in Oversized Boyfriend Fit in Vintage Wash</a></h4>
                                        <span class="price">$65.00</span>
                                        <br><br>
                                        <a href="#">Add to wishlist</a>&nbsp;&nbsp;&nbsp;
                                        <a href="#">Remove</a>
                                    </td>
                                    <td>
                                        <p class="features">
                                            Color: <strong>Black</strong><br>
                                            Size: <strong>S</strong>
                                        </p>
                                        <a href="#">Edit</a>
                                    </td>
                                    <td>
                                        <input class="form-control spinner-quantity" id="quantity2" required>
                                    </td>
                                    <td class="text-right">
                                        <strong>$65.00</strong>
                                    </td>
                                </tr>--%>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-sm-6 col-md-4 form-inline">
                       
                        <!--
                        -->
                       
                    </div>
                    <div class="col-sm-6 col-md-4 col-md-offset-4">
                        <div class="table">
                            <table class="price-calc">
                                <tbody>
                                    <tr>
                                        <th>AltTutar</th>
                                        <td class="text-right">
                                            <strong><%=ToplamTutar() %> TL</strong>
                                        </td>
                                    </tr>
                                    <tr>
                                         <th>Kargo Bedeli</th>
                                        <td class="text-right">
                                            <strong><%=0 %> TL</strong>
                                        </td>
                                    </tr>
                                   
                                    <tr class="order-total">
                                        <th>Sipariş toplamı</th>
                                        <td class="text-right"><%=ToplamTutar() %> TL
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <%--<button type="button" class="btn btn-default pull-left">Alışverişe Devam Et</button>--%>
                        <asp:LinkButton ID="LinkButton1" runat="server" class="btn btn-default pull-left" type="button" PostBackUrl="<%$RouteUrl:RouteName=anasayfa%>">Alışverişe Devam Et</asp:LinkButton>
                        <%--<button type="submit" class="btn btn-primary pull-right">Alışverişi Bitir</button>--%>
                        <asp:LinkButton ID="LinkButton2" type="submit"  PostBackUrl="<%$RouteUrl:RouteName=faturabilgileri%>" class="btn btn-primary pull-right" runat="server">Alışverişi Bitir</asp:LinkButton>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->



</asp:Content>

