﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class odeme : System.Web.UI.Page
{
    List<Siparis> sepettekiler;
    Il il;
    Ilce ilce;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null)
        {
            if (ddlKendi.SelectedIndex == 0)
            {
                FaturaAdres.Visible = false;
            }
            Sepet sepetim = (Sepet)Session["sepettekiler"];
            sepettekiler = sepetim.siparisListesi;

            rptUrunler.DataSource = sepettekiler;
            rptUrunler.DataBind();
        }
        else
        {
            Response.Write("<script>alert('Buraya girmeye yetkiniz bulunmamaktadır.');window.location.href='" + GetRouteUrl("anasayfa", null) + "'</script>");
        }
    }
    protected void ddlKendi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlKendi.SelectedIndex == 1)
        {

            il = new Il();
            ddlSehir.DataSource = il.IlleriGetir();
            ddlSehir.DataBind();
            int secilenIl = Convert.ToInt32(ddlSehir.SelectedValue);
            ilce = new Ilce();
            ddlIlce.DataSource = ilce.IlceleriGetir(secilenIl);
            ddlIlce.DataBind();
            FaturaAdres.Visible = true;

        }
    }
    protected void btnDevam_Click(object sender, EventArgs e)
    {
       
        Adres adres = new Adres();
        if (ddlKendi.SelectedIndex == 0)
        {
            /*kendi adresini gönder*/

            adres.defaultAdres = Convert.ToBoolean(ddlKendi.SelectedIndex + 1);
            adres.uyeID = ((Uye)Session["kullanici"]).ID;
            DataRow adresi = adres.AdresGetir();
            if (adresi == null)
            {
                Response.Write("<script>alert('Bireysel adresiniz sistemde kayıtlı değildir.')</script>");
                return;
            }
            adres.adSoyad = adresi["adSoyad"].ToString();
            adres.cepTel = adresi["cepTel"].ToString();
            adres.evTel = adresi["evTel"].ToString();
            adres.postaKodu = adresi["postaKodu"].ToString();
            adres.email = adresi["email"].ToString();
            adres.sehirID = Convert.ToInt32(adresi["sehirId"].ToString());
            adres.ilceID = Convert.ToInt32(adresi["ilceId"].ToString());
            Session["FaturaAdres"] = adres;
        }
        else
        {
            adres.defaultAdres = Convert.ToBoolean(ddlKendi.SelectedIndex - 1);
            adres.adSoyad = Request.Form["Ad"] + " " + Request.Form["Soyad"];
            adres.cepTel = Request.Form["CepTel"];
            adres.evTel = Request.Form["EvTel"];
            adres.postaKodu = Request.Form["PostaKod"];
            adres.email = Request.Form["Email"];
            adres.sehirID = Convert.ToInt32(ddlSehir.SelectedValue.ToString());
            adres.ilceID = Convert.ToInt32(ddlIlce.SelectedValue.ToString());
            Session["FaturaAdres"] = adres;
        }
        //Response.Redirect("odeme2.aspx");
        Response.RedirectToRoute("teslimatbilgileri",null);
        
    }
    protected void ddlSehir_SelectedIndexChanged(object sender, EventArgs e)
    {
        int secilenIl = Convert.ToInt32(ddlSehir.SelectedValue);
        ilce = new Ilce();
        ddlIlce.DataSource = ilce.IlceleriGetir(secilenIl);
        ddlIlce.DataBind();

    }
    public decimal ToplamTutar()
    {
        Sepet sepettekiler = (Sepet)Session["sepettekiler"];
        return sepettekiler.toplamTutar;
    }
}