﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ForgotYourPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnForgot_Click(object sender, EventArgs e)
    {
        string email = txtEmail.Text;
        Uye uye = new Uye();
        uye.UyeBilgileriGetir(email);

        if (uye.sifre != null)
        {
            MailMessage mesaj = new MailMessage();
            mesaj.To.Add(new MailAddress(uye.email));
            mesaj.From = new MailAddress("web3eticaret.deneme@gmail.com");
            mesaj.Subject = "Şifre Hatırlatma";
            mesaj.Body = "E-mail =" + uye.email + "\n" + "Kullanıcı Adı:" + uye.kullaniciAdi + "\n" + "Şifre:" + uye.sifre + "\n";
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential("web3eticaret.deneme@gmail.com", "web3deneme");
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            try
            {

                client.Send(mesaj);
                //Response.Write(alarmVer.msjBox("Şifreniz E-Mail adresinize gönderilmiştir. Teşekkür ederiz !", "Default.aspx"));
                Response.Write("<script>alert('Şifreniz E-Mail adresinize gönderilmiştir. Teşekkür ederiz !')</script>");
            }
            catch (Exception ex)
            {
                //Response.Write(ex.Message);
                Response.Write("<script>alert('" + ex.Message + "')</script>");

                //Response.Write(alarmVer.msjBox("Mesaj Gönderilirken bir hata oluştu.", "Sifremi-Unuttum.aspx"));
            }

            lblMesaj.Visible = false;
        }
        else
        {

            lblMesaj.Text = "E-mail adresi kayıtlı değil!";
        }

        txtEmail.Text = "";
        System.Threading.Thread.Sleep(2000);
    }
}