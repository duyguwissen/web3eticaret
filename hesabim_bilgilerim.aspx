﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="hesabim_bilgilerim.aspx.cs" Inherits="hesabim_bilgilerim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <section id="Content" role="main">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Hesabım
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="index.html">Anasayfa</a></li>
                            <!--
                          -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                          -->
                            <li>Checkout</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <section class="row">
                <div class="col-sm-3">
                    <nav class="shop-section-navigation element-emphasis-weak">
                        <ul class="list-unstyled">
                            <li>
                                <%--<a href='<%#GetRouteUrl("hesap",null) %>'>Hesap Kontrol Paneli</a>--%>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="<%$RouteUrl:RouteName=hesap%>">Hesap Kontrol Paneli</asp:HyperLink>
                            </li>
                            <li class="active"><span>Hesap Bilgileri</span></li>
                            <li>
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="<%$RouteUrl:RouteName=siparisler%>">Ürünlerim</asp:HyperLink>
                            </li>
                            <li>
                                <asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="<%$RouteUrl:RouteName=adres%>">Adres Defteri</asp:HyperLink>
                            </li>
                            <li>
                                <asp:LinkButton ID="lbtnCikis" runat="server" OnClick="lbtnCikis_Click">Çıkış</asp:LinkButton>
                            </li>

                        </ul>
                    </nav>
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <div class="col-md-6 col-sm-9 space-left-30">
                    <h2 class="strong-header large-header">Hesap Bilgilerini Düzenle</h2>
                    <div role="form">
                        <div class="form-group">
                            <label for="first-name">Ad</label>
                            <%--<input type="text" class="form-control" id="first-name" required value="John">--%>
                            <asp:TextBox ID="txtAd" runat="server" class="form-control" required></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label for="last-name">Soyad</label>
                            <%--<input type="text" class="form-control" id="last-name" required value="Doe">--%>
                            <asp:TextBox ID="txtSoyad" class="form-control" runat="server" required></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label for="email">Email Adresi</label>
                            <%--<input type="email" class="form-control" id="email" required value="john.doe@gmail.com">--%>
                            <asp:TextBox ID="txtEmail" runat="server" class="form-control" type="email" required></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <table>
                                <tr>
                                    <td>
                                        <label for="password">Yeni Şifre</label></td>
                                    <td>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="Şifreler farklı" ControlToValidate="txtYeniSifre" ControlToCompare="txtSifreTekrar"></asp:CompareValidator>
                                    </td>

                                </tr>
                                <tr>
                                    <td>
                                        <%--<input type="password" class="form-control" id="password" required>--%>
                                        <asp:TextBox ID="txtYeniSifre" class="form-control" runat="server" required></asp:TextBox></td>
                                </tr>
                            </table>
                        </div>
                        <div class="form-group">
                            <label for="password-repeat">Şifre Tekrar</label>
                            <asp:TextBox ID="txtSifreTekrar" class="form-control" runat="server" required></asp:TextBox>
                            <%--<input type="password" class="form-control" id="password-repeat" required>--%>
                        </div>
                        <%--<button type="submit" class="btn btn-primary">Gönder</button>--%>
                        <asp:Button ID="btnGonder" runat="server" Text="Gönder" type="submit" class="btn btn-primary" OnClick="btnGonder_Click" />
                    </div>
                </div>
            </section>
        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->



</asp:Content>

