﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class iletisim : System.Web.UI.Page
{
    Uye u;
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void Button1_Click(object sender, EventArgs e)
    {
        GonderilenMesaj gmsj = new GonderilenMesaj();
        gmsj.eklenmeTarihi = DateTime.Now;
        gmsj.adSoyad = txtAdSoyad.Text;
        gmsj.email = txtEmail.Text;
        gmsj.mesajiniz = txtMesaj.Text;
        bool kayitliMi = gmsj.MesajKayit();

        /*Adminlere Mesaj atılması*/
        u = new Uye();
        List<string> emailListesi = u.AdminMailAdresleriGetir();
        MailMessage mesaj = new MailMessage();
        foreach (string item in emailListesi)
        {
            mesaj.To.Add(new MailAddress(item));
        }

        mesaj.From = new MailAddress("web3eticaret.deneme@gmail.com");
        mesaj.Subject = "İletişim";
        mesaj.Body = "Ad Soyad : " + txtAdSoyad.Text + "\n" + "Email: " + txtEmail.Text + "\n" + "Mesaj: " + txtMesaj.Text + "\n";
        SmtpClient client = new SmtpClient();
        client.Credentials = new System.Net.NetworkCredential("web3eticaret.deneme@gmail.com", "web3deneme");
        client.EnableSsl = true;
        client.Host = "smtp.gmail.com";
        client.Port = 587;
        try
        {
            client.Send(mesaj);
            
            //Response.Write("<script>alert('Şifreniz E-Mail adresinize gönderilmiştir. Teşekkür ederiz !')</script>");
        }
        catch (Exception)
        {
            //Response.Write("<script>alert('" + ex.Message + "')</script>");

        }

        if (kayitliMi)
            Response.RedirectToRoute("anasayfa", null);


    }
}