﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="uyekayit.aspx.cs" Inherits="uyekayit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        #rbtnBayan, #rbtnBay {
            padding-left: 5px;
            margin-left: 10px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="Content" role="main">
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Hesabım
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="/">Anasayfa</a></li>
                            <!--
                          -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                          -->
                            <li>Kayıt</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <div class="row">
                <div class="col-md-6 space-right-20">
                    <section class="element-emphasis-weak">
                        <h2 class="strong-header">Hesabınız mı var?
                        </h2>
                        <%--<a href="08-a-shop-account-login.html" >
                          Login
                      </a>--%>
                        <asp:HyperLink ID="hlLogin" runat="server" class="btn btn-default" NavigateUrl="<%$RouteUrl:RouteName=giris%>">Giriş Yapın</asp:HyperLink>
                    </section>
                </div>
                <div class="col-md-6 space-left-20">
                    <section class="register element-emphasis-strong">
                        <h2 class="strong-header large-header">Hesap Oluştur
                        </h2>
                        <form role="form" action="09-a-shop-account-dashboard.html" method="post" novalidate="">

                            <div class="form-group">
                                <table>
                                    <tr>
                                        <td>
                                            <label for="last-name">Tc Kimlik No</label>
                                            <%--<input type="text" class="form-control" id="last-name" required="">--%>
                                            <asp:TextBox ID="txtTc" runat="server" class="form-control" required="" type="number"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:CustomValidator ID="cvTc" runat="server" ErrorMessage="Tc Kimlik Numaranızı Doğru Giriniz" ControlToValidate="txtTc" ForeColor="Red" OnServerValidate="cvTc_ServerValidate"></asp:CustomValidator>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="form-group">
                                <label for="last-name">Kullanıcı Adı</label>
                                <%--<input type="text" class="form-control" id="last-name" required="">--%>
                                <asp:TextBox ID="txtKullaniciAd" runat="server" class="form-control" required=""></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="password">Şifre</label>
                                <%--<input type="password" class="form-control" id="password" required="">--%>
                                <asp:TextBox ID="txtPassword" runat="server" class="form-control" required="" TextMode="Password"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="password-repeat">Şifre Tekrar</label>
                                <%--<input type="password" class="form-control" id="password-repeat" required="">--%>
                                <asp:TextBox ID="txtRepeatPassword" runat="server" class="form-control" required="" TextMode="Password"></asp:TextBox>
                                <asp:CompareValidator ID="cvSifreUyumu" runat="server" ErrorMessage="Şifreler Uyuşmuyor" ControlToCompare="txtRepeatPassword" ControlToValidate="txtPassword" ForeColor="Red"></asp:CompareValidator>
                            </div>
                            <div class="form-group">
                                <label for="first-name">Ad</label>
                                <%--<input type="text"  id="first-name" >--%>
                                <asp:TextBox ID="txtAd" runat="server" class="form-control" required=""></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label for="last-name">Soyad</label>
                                <%--<input type="text" class="form-control" id="last-name" required="">--%>
                                <asp:TextBox ID="txtSoyad" runat="server" class="form-control" required=""></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label for="email">Email Adresi</label>
                                <%--<input type="email" class="form-control" id="email" required="">--%>
                                <asp:TextBox ID="txtEmail" runat="server" class="form-control" required="" type="email"></asp:TextBox>

                            </div>

                            <div class="form-group">
                                <label for="last-name">Cinsiyet</label><br />
                                <asp:RequiredFieldValidator ID="rvCinsiyet" runat="server" ErrorMessage="Cinsiyetinizi Seçiniz" ControlToValidate="rbtnCinsiyet" ForeColor="Red"></asp:RequiredFieldValidator>
                                <asp:RadioButtonList ID="rbtnCinsiyet" runat="server" required="">
                                    <asp:ListItem>Bayan</asp:ListItem>
                                    <asp:ListItem>Bay</asp:ListItem>

                                </asp:RadioButtonList>
                            </div>

                            <div class="form-group">
                                <label for="last-name">DoğumTarihi</label><br />

                                <asp:TextBox ID="txtDogumTarihi" runat="server" type="date" class="form-control" required=""></asp:TextBox>


                            </div>

                            <div class="form-group">
                                <label for="last-name">Ev Telefonu</label>
                                <%--<input type="email" class="form-control" id="email" required="">--%>
                                <asp:TextBox ID="txtEvTel" runat="server" class="form-control" required="" type="number"></asp:TextBox>

                            </div>

                            <div class="form-group">
                                <label for="last-name">Cep Telefonu</label>
                                <%--<input type="email" class="form-control" id="email" required="">--%>
                                <asp:TextBox ID="txtCepTel" runat="server" class="form-control" required="" type="number"></asp:TextBox>

                            </div>




                            <%--<button type="submit" >Register</button>--%>
                            <asp:Button ID="btnKaydol" runat="server" Text="Kaydol" class="btn btn-primary" OnClick="btnKaydol_Click" />
                        </form>
                    </section>
                </div>
            </div>

        </div>
    </section>
</asp:Content>

