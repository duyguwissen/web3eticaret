﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Index.aspx.cs" Inherits="Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <section id="Content" role="main">
        <div class="container">

            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->


        <div class="full-width no-space flexslider-full-width">
            <!-- FLEXSLIDER -->
            <!-- SPECIAL VERSION 1 -->



            <div class="flexslider">
                <ul class="slides">
                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <!-- FLEXSLIDER SLIDE -->
                            <li style="background-image: url(/images/demo-content/<%#Eval("Resim") %>); min-height: 560px">
                                <div class="container">
                                    <div class="row">

                                        <div class="col-sm-4 col-sm-offset-6">
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <!-- CALL TO ACTION BOX -->
                                            <div class="calltoaction-box text-center">
                                                <h4 class="strong-header"><%#Eval ("Yazi") %></h4>

                                                <p>
                                                    <%#Eval ("Altyazi") %>
                                                    <br>
                                                    <br>
                                                </p>
                                                <a href="#" class="btn btn-primary">Satın Al!</a>
                                            </div>
                                            <!-- !CALL TO ACTION BOX -->
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <!-- !FLEXSLIDER SLIDE -->


                        </ItemTemplate>

                    </asp:Repeater>




                </ul>
                <div class="flexslider-full-width-controls"></div>
            </div>
            <!-- !FLEXSLIDER -->

        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->


            <!-- !SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1">
            <div class="container">
                <section class="row">
                    <div class="col-md-12 text-center">
                        <!-- ICON BOX SHORT -->
                        <div class="icon-box icon-box-short">
                            <i class="fa fa-refresh"></i>
                            <h4 class="strong-header">Yeni 30 Days return</h4>
                        </div>
                        <!-- !ICON BOX SHORT -->

                        <!-- ICON BOX SHORT -->
                        <div class="icon-box icon-box-short">
                            <i class="fa fa-truck"></i>
                            <h4 class="strong-header">Free shipping</h4>
                        </div>
                        <!-- !ICON BOX SHORT -->

                        <!-- ICON BOX SHORT -->
                        <div class="icon-box icon-box-short">
                            <i class="fa fa-lock"></i>
                            <h4 class="strong-header">Secure payments</h4>
                        </div>
                        <!-- !ICON BOX SHORT -->

                        <!-- ICON BOX SHORT -->
                        <div class="icon-box icon-box-short">
                            <i class="fa fa-gift"></i>
                            <h4 class="strong-header">New styles every day</h4>
                        </div>
                        <!-- !ICON BOX SHORT -->
                    </div>
                </section>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->


            <!-- FLEXSLIDER -->
            <!-- STANDARD -->
            <section class="row">
                <div class="section-header col-xs-12">
                    <hr>
                    <h2 class="strong-header">ÖNE ÇIKAN ÜRÜNLER
                    </h2>
                </div>
                <div class="col-md-12">
                    <div class="flexslider flexslider-nopager row">
                        <ul class="slides">
                            <li class="row">
                                <asp:Repeater ID="OneCikanUrunler" runat="server">
                                    <ItemTemplate>
                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <!-- SHOP FEATURED ITEM -->
                                            <div class="shop-item shop-item-featured overlay-element">
                                                <div class="overlay-wrapper">
                                                    <a href="04-shop-product-single.html">
                                                        <img src="/Resim/buyuk/<%#Eval("resimUrl") %>" alt="Shop item">
                                                    </a>

                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="overlay-contents">
                                                        <div class="shop-item-actions">
                                                            <%-- <button class="btn btn-primary btn-block">
                                                                Sepete Ekle
                                                            </button>--%>
                                                            <asp:HyperLink ID="hlSepet" class="btn btn-primary btn-block" runat="server">SepeteEkle</asp:HyperLink>
                                                            
                                                            <asp:HyperLink ID="hlUrun"  class="btn btn-default btn-block" runat="server" NavigateUrl='<%#"~/urundetay.aspx?urunId="+Eval("id") %>'>Ürünü İncele</asp:HyperLink>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-info-name-price">
                                                    <h4><a href="#"><%#Eval("urunAd") %></a></h4>
                                                    <span class="price"><%#Eval("urunFiyat") %></span>
                                                </div>
                                            </div>
                                            <!-- !SHOP FEATURED ITEM -->
                                        </div>
                                        <div class="clearfix visible-xs space-30"></div>
                                        <%#Container.ItemIndex%3==0&Container.ItemIndex<8&Container.ItemIndex>0? "</li><li>":"" %>
                                    </ItemTemplate>
                                </asp:Repeater>
                                <!-- VIRTUAL ROW !IMPORTANT -->



                            </li>

                        </ul>
                    </div>
                </div>
            </section>
            <!-- !FLEXSLIDER -->


            <section class="row">
                <div class="section-header col-xs-12">
                    <hr>
                    <h2 class="strong-header">Highlights
                    </h2>
                </div>
                <div class="col-sm-4">
                    <!-- SHOP HIGHLIGHT -->
                    <!-- VERSION 1 -->
                    <div class="shop-item-highlight shop-item-highlight-version-1">
                        <a href="04-shop-product-single.html">
                            <img src="/images/demo-content/home-highlights-1.jpg" alt="Highlighted shop item">
                        </a>

                        <a href="04-shop-product-single.html" class="item-info-name-data">
                            <div class="item-info-name-data-wrapper">
                                <h4>Knit dress with seaming detail</h4>
                                From <span class="price">$45.00</span>
                            </div>
                        </a>
                    </div>
                    <!-- !SHOP HIGHLIGHT -->
                    <!-- !VERSION 1 -->
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <!-- VIRTUAL ROW !IMPORTANT -->
                <div class="col-sm-4">
                    <!-- SHOP HIGHLIGHT -->
                    <!-- VERSION 1 -->
                    <div class="shop-item-highlight shop-item-highlight-version-1">
                        <a href="04-shop-product-single.html">
                            <img src="/images/demo-content/home-highlights-2.jpg" alt="Highlighted shop item">
                        </a>

                        <a href="04-shop-product-single.html" class="item-info-name-data">
                            <div class="item-info-name-data-wrapper">
                                <h4>Midi dress in tapestry print</h4>
                                From <span class="price">$55.00</span>
                            </div>
                        </a>
                    </div>
                    <!-- !SHOP HIGHLIGHT -->
                    <!-- !VERSION 1 -->
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <!-- VIRTUAL ROW !IMPORTANT -->
                <div class="col-sm-4">
                    <!-- SHOP HIGHLIGHT -->
                    <!-- VERSION 1 -->
                    <div class="shop-item-highlight shop-item-highlight-version-1">
                        <a href="04-shop-product-single.html">
                            <img src="/images/demo-content/home-highlights-3.jpg" alt="Highlighted shop item">
                        </a>

                        <a href="04-shop-product-single.html" class="item-info-name-data">
                            <div class="item-info-name-data-wrapper">
                                <h4>Denim overall in dark wash</h4>
                                From <span class="price">$65.00</span>
                            </div>
                        </a>
                    </div>
                    <!-- !SHOP HIGHLIGHT -->
                    <!-- !VERSION 1 -->
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <!-- VIRTUAL ROW !IMPORTANT -->
            </section>


            <!-- FLEXSLIDER WITH FLIPPED IMAGES -->
            <!-- STANDARD -->

            <section class="row ">
                <div class="section-header col-xs-12">
                    <hr>
                    <h2 class="strong-header">En Son Eklenen Ürünler
                    </h2>
                </div>
                <div class="col-md-12">
                    <div class="flexslider flexslider-nopager row">
                        <ul class="slides">
                            <li class="row">
                                <asp:Repeater ID="Repeater2" runat="server">
                                    <ItemTemplate>

                                        <div class="col-md-3 col-sm-6 col-xs-12">
                                            <!-- SHOP FEATURED ITEM -->
                                            <div class="shop-item shop-item-featured overlay-element">
                                                <div class="overlay-wrapper">
                                                    <a href="04-shop-product-single.html">
                                                        <img src="/Resim/buyuk/<%#Eval("resimUrl") %>" alt="Shop item">
                                                    </a>

                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class="overlay-contents">
                                                        <div class="shop-item-actions">
                                                            <asp:HyperLink ID="HyperLink2" class="btn btn-primary btn-block" runat="server">Sepete Ekle</asp:HyperLink>

                                                            <asp:HyperLink ID="HyperLink1" class="btn btn-default btn-block" runat="server" NavigateUrl='<%#"~/urundetay.aspx?urunId="+Eval("id") %>'>Ürünü İncele</asp:HyperLink>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="item-info-name-price">
                                                    <h3><%#Eval("markaAd") %></h3>
                                                    <h4><a href="#"><%#Eval("urunAd") %>  <%#Eval("urunAciklama") %></a></h4>
                                                    <span class="price"><%#Eval("urunFiyat") %> TL</span>
                                                </div>
                                            </div>
                                            <!-- !SHOP FEATURED ITEM -->
                                        </div>
                                        <div class="clearfix visible-xs space-30"></div>
                                        <!-- VIRTUAL ROW !IMPORTANT -->
                                        <%#Container.ItemIndex%3==0&Container.ItemIndex<8&Container.ItemIndex>0? "</li><li>":"" %>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </li>

                        </ul>
                    </div>
                </div>
            </section>



            <!-- !FLEXSLIDER -->


            <section class="row">
                <div class="section-header col-xs-12">
                    <hr>
                    <h2 class="strong-header">Top Rated Products
                    </h2>
                </div>


                <asp:Repeater ID="rptTopProducts" runat="server">
                    <ItemTemplate>
                        <div class="col-sm-6 col-md-4">
                            <!-- TOP RATED PRODUCT -->
                            <div class="shop-top-rated">
                                <%-- <a href="04-shop-product-single.html">
                                    <img src="/images/demo-content/shop-item-top-01.jpg" alt=" ">
                                </a>--%>
                                <%--<asp:ImageButton ID="imgUrun" runat="server" ImageUrl='/Resim/buyuk/<%#Eval("resimUrl") %>' AlternateText=""/>--%>
                                <asp:LinkButton ID="LinkButton1" runat="server">
                                    <img src="/Resim/buyuk/<%#Eval("resimUrl") %>" alt="">
                                </asp:LinkButton>


                                <span class="rating" data-score="<%#Puanla(Eval("puan")) %>"></span>

                                <div class="item-info-name-price">
                                    <h4><%--<a href="04-shop-product-single.html">Shirt With Contrast Collar In Chambray</a>--%>
                                        <asp:LinkButton ID="lbtnUrunAd" runat="server"><%#Eval("urunAd") %></asp:LinkButton>
                                    </h4>
                                    <span class="price"><%#Eval("urunFiyat") %></span>
                                </div>
                            </div>
                            <!-- !TOP RATED PRODUCT -->
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </section>
            <!-- !SHOP TOP RATED -->


            <!-- FLEXSLIDER -->
            <!-- STANDARD -->
            <section class="row">
                <div class="section-header col-xs-12">
                    <hr>
                    <h2 class="strong-header">Recently viewed
                    </h2>
                </div>
                <div class="col-md-12">
                    <div class="flexslider flexslider-pager row">
                        <ul class="slides">
                            <li>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-1.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-2.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-xs space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-3.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-sm space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-4.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-xs space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-5.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-6.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                            </li>
                            <li>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-7.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-8.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-xs space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-9.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-sm space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-10.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-xs space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-11.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-12.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                            </li>
                            <li>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-13.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-14.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-xs space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-15.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-sm space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-16.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="clearfix visible-xs space-30"></div>
                                <!-- VIRTUAL ROW !IMPORTANT -->
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-17.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                                <div class="col-md-2 col-sm-4 col-xs-6">
                                    <!-- RECENTLY VIEWED ITEM -->
                                    <div class="shop-recently-viewed overlay-element">
                                        <div class="overlay-wrapper">
                                            <a href="04-shop-product-single.html">
                                                <img src="/images/demo-content/home-recently-viewed-18.jpg" alt="Recently viewed item">
                                            </a>

                                            <div class="overlay-contents">
                                                <div class="shop-item-actions">
                                                    <button class="btn btn-primary btn-small">
                                                        Delete
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- !RECENTLY VIEWED ITEM -->
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <!-- !FLEXSLIDER -->


            <section class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="section-header col-xs-12">
                            <hr>
                            <h2 class="strong-header">From our blog
                            </h2>
                        </div>
                        <!-- BLOG POSTS MINI -->
                        <div class="col-sm-6">
                            <article class="post-preview-mini">
                                <a href="15-pages-blog-single.html">
                                    <img src="/images/demo-content/home-blog-1.jpg" alt="Blog post image">
                                </a>

                                <div class="post-info-name-date-comments">
                                    <span class="date">Aug 9, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">3 comments</a>
                                    <h4><a href="15-pages-blog-single.html">Green leather shorts trousers headscarf neutral plaited</a></h4>
                                </div>
                            </article>
                        </div>
                        <!-- !BLOG POSTS MINI -->
                        <div class="clearfix visible-xs space-30"></div>
                        <!-- VIRTUAL ROW !IMPORTANT -->
                        <!-- BLOG POSTS MINI -->
                        <div class="col-sm-6">
                            <article class="post-preview-mini">
                                <a href="15-pages-blog-single.html">
                                    <img src="/images/demo-content/home-blog-2.jpg" alt="Blog post image">
                                </a>

                                <div class="post-info-name-date-comments">
                                    <span class="date">Aug 7, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">Leave a comment</a>
                                    <h4><a href="15-pages-blog-single.html">Clashing patterns cuff surf pop</a></h4>
                                </div>
                            </article>
                        </div>
                        <!-- !BLOG POSTS MINI -->
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="row">
                        <div class="section-header col-xs-12">
                            <hr>
                            <h2 class="strong-header">En Son Tweeetler
                            </h2>
                        </div>
                        <div class="col-xs-12">
                            <!-- display tweets here -->
                            <!-- configuration is placed in twitter/config.php -->
                            <div class="tweets_display" data-limit="1"></div>
                        </div>
                    </div>
                </div>
                <!-- !TWITTER -->
            </section>

            <!-- SECTION EMPHASIS 2 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-2">
            <div class="container">
                <section class="row">
                    <div class="section-header col-xs-12">
                        <hr>
                        <h2 class="strong-header">Bültenimize Katılın
                        </h2>
                    </div>
                    <div class="col-md-8 col-md-offset-2 text-center">
                        <!-- NEWSLETTER BOX -->
                        <div class="newsletter-box">
                            <div class="successMessage alert alert-success alert-dismissable" style="display: none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Email gönderdiğiniz için teşekkür ederiz.
                            </div>
                            <div class="errorMessage alert alert-danger alert-dismissable" style="display: none">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                Ups! Bir hata oluştu. Lütfen tekrar deneyiniz.
                            </div>

                            <div role="form" class="form-inline validateIt" data-email-subject="Newsletter Form" data-show-errors="true" data-hide-form="true">
                                <div class="form-group">
                                    <label class="sr-only" for="newsletter-widget-name-1">Adınız</label>
                                    <asp:TextBox ID="TextBox1" runat="server" class="form-control-" CssClass="form-group ad" placeholder="Adınızı Giriniz"></asp:TextBox>
                                    <!--!  <input type="text" required name="field[]" class="form-control" id="newsletter-widget-name-1" placeholder="Adınızı Giriniz" > -->
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="newsletter-widget-email-1">Email</label>
                                    <asp:TextBox ID="TextBox2" runat="server" class="form-control" CssClass="form-group mail" placeholder="Email Giriniz"></asp:TextBox>
                                    <!--!<input type="email" required name="field[]" class="form-control" id="newsletter-widget-email-1" placeholder="Email Giriniz" > -->
                                </div>
                                <asp:Button ID="Button1" runat="server" Text="Kaydol" class="form-control" CssClass="btn btn-primary btn-small" OnClick="Button1_Click" />

                            </div>
                            <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
                        </div>
                        <script>
                            $(function () {
                                $(".btn-small").on('click', function (e) {
                                    if ($('.ad').val() == "" || $('.mail').val() == "") {
                                        alert("Lütfen gerekli alanları doldurunuz");
                                        e.preventDefault();
                                    }
                                    else {

                                        alert("Mesajınız Kaydedildi.Teşekkür Ederiz.");

                                    }

                                })
                            })
                        </script>
                        <!-- !NEWSLETTER BOX -->

                    </div>
                </section>

            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 2 -->


        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->




</asp:Content>

