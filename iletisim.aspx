﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="iletisim.aspx.cs" Inherits="iletisim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   <script src="http://maps.googleapis.com/maps/api/js"></script>
<script>
    function initialize() {
        var mapProp = {
            center: new google.maps.LatLng(41.042022, 29.009045),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var marker = new google.maps.Marker({
            position: { lat: 41.042022, lng: 29.009045 },
            map: map,
            title: 'Hello World!'
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

<section id="Content" role="main">
  <div class="container">
    <!-- SECTION EMPHASIS 1 -->
    <!-- FULL WIDTH -->
  </div>
  <!-- !container -->
  <div class="full-width section-emphasis-1 page-header">
    <div class="container">
      <header class="row">
        <div class="col-md-12">
          <h1 class="strong-header pull-left">
            İLETİŞİM
          </h1>
          <!-- BREADCRUMBS -->
          <ul class="breadcrumbs list-inline pull-right">
            <li><a href="/">Anasayfa</a></li>
            <!--
                                        -->
            <li>Sayfalar</li>
            <!--
                                        -->
            <li>İletişim Kurun</li>
          </ul>
          <!-- !BREADCRUMBS -->
        </div>
      </header>
    </div>
  </div>
  <!-- !full-width -->
  <div class="container">
    <!-- !FULL WIDTH -->
    <!-- !SECTION EMPHASIS 1 -->


    <!-- FULL WIDTH -->
  </div>
  <!-- !container -->
  <div class="full-width google-map">
 <div id="googleMap" style="width:100%;height:420px;"></div>
  </div>
  <!-- !full-width -->
  <div class="container">
    <!-- !FULL WIDTH -->

    <section class="row">
      <div class="col-md-8">
        <div class="section-header col-xs-12">
          <hr>
          <h2 class="strong-header">
            İletişim Kurun
          </h2>
        </div>
        <div class="col-xs-12">
          <p>
              Sitemiz hakkında her türlü soru ve yorumlarınız için ya da iletişim kurmak istediğinizde aşağıdaki formu doldurunuz. Sizinle irtibata geçeceğiz.
          </p>

          <div class="simpleForm">
            <div class="successMessage alert alert-success alert-dismissable" style="display: none">
                <button type="submit" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Thank You! We will contact you shortly.
            </div>
            <div class="errorMessage alert alert-danger alert-dismissable" style="display: none">
                <button type="submit" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                Ups! An error occured. Please try again later.
            </div>
            <div role="form">
              <fieldset>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="field1">Ad Soyad</label>
                      <div id="field1">
                          <asp:TextBox ID="txtAdSoyad" runat="server"  name="field[]" cssClass="form-control adsoyad"></asp:TextBox>
                      </div>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="field2">Email</label>
                      <div id="field2">
                      <asp:TextBox ID="txtEmail" runat="server" type="email" cssClass="form-control email"  name="field[]"  ></asp:TextBox>
                      </div>
                   
                  </div>
                </div>
              </div>
              <div class="form-group">
                <label for="field3">Mesajınız</label>
                <div id="field3">
                  <asp:TextBox ID="txtMesaj" runat="server"  TextMode="MultiLine" name="field[]" cssClass="form-control mesaj" Rows="10"></asp:TextBox>
                </div>
              </div>
                  <asp:Button type="submit" ID="Button1" runat="server" Text="Mesaj Gönder" cssclass="btn btn-primary iletisimbtn" OnClick="Button1_Click"/>
                 
                  
              </fieldset>
            </div>
          </div>
          <!-- / simpleForm -->

        </div>
      </div>
      

       <script>
           $(function () {
               $(".iletisimbtn").on('click', function () {
                   if ($('.adsoyad').val() == "" || $('.email').val() == "" || $('.mesaj').val() == "") {
                       alert("Lütfen gerekli alanları doldurunuz");
                       event.preventDefault()
                   }
                   else {
                       alert("Mesajınız Kaydedildi.Teşekkür Ederiz.");   
                   }
               })
           })
            </script>

      <div class="col-md-4">
        <div class="space-30"></div>
        <div class="section-emphasis-3 page-info">
          <h3 class="strong-header">
            Kontak
          </h3>

          <div class="text-widget">
            <address>
              <a href="mailto:bahcesehir.edu.tr">bahcesehir.edu.tr</a><br>
              +90 444 2 864
            </address>
          </div>
          <br>
          <h3 class="strong-header">
            Lokasyon
          </h3>

          <div class="text-widget">
            <address>
                Çırağan Caddesi<br>
Osmanpaşa Mektebi Sokak No: 4 - 6<br>
34349 Beşiktaş/İstanbul<br>
Türkiye
     
             
            </address>
          </div>
        </div>
      </div>
    </section>
  </div>
</section>

<div class="clearfix visible-xs visible-sm"></div>
<!-- fixes floating problems when mobile menu is visible -->


</asp:Content>

