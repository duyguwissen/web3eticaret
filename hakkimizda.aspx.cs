﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hakkimizda : System.Web.UI.Page
{
   int bitisIndex;
   string yeniYazi;
    protected void Page_Load(object sender, EventArgs e)
    {
        Hakkimizda hakkim= new Hakkimizda();
        rptHakkimizda.DataSource = hakkim.HakkimizdaGetir();
        rptHakkimizda.DataBind();
    }

    public string birinciParagraf(object yazi) {

        string Yazi = yazi.ToString();
        yeniYazi = Yazi;
        int uzunluk = Yazi.Length / 2;
        for (int i = uzunluk; i < Yazi.Length; i++)
        {
            if (Yazi[i].ToString()==" ")
            {
                bitisIndex = i;
                break;
            }
        }
        return Yazi.Substring(0, bitisIndex);

    
    }

    public string ikinciParagraf() {
        int uzunluk = yeniYazi.Length;
      return  yeniYazi.Substring(bitisIndex, (yeniYazi.Length - 1-bitisIndex));

    }
}