﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="urunler.aspx.cs" Inherits="urunler" EnableEventValidation="True" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">



    <section id="Content" role="main">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Shop</h1>

                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="/">Anasayfa</a></li>
                            <!--
                         -->
                            <li>Shop</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->


            <div class="row">
                <div>
                    <div class="shop-list-filters col-sm-4 col-md-3">

                        <div class="filters-active element-emphasis-strong" style="display: none;">
                            <h3 class="strong-header element-header" style="display: none;">You've selected
                            </h3>
                            <!-- dynamic added selected filters -->
                            <ul class="filters-list list-unstyled">
                                <li></li>
                            </ul>
                            <button type="button" class="filters-clear btn btn-primary btn-small btn-block">
                                Clear all
                            </button>
                        </div>

                        <button type="button" class="btn btn-default btn-small visible-xs" data-texthidden="Hide Filters" data-textvisible="Show Filters" id="toggleListFilters"></button>

                        <div id="listFilters">

                            <div class="filters-details element-emphasis-weak">
                                <!-- ACCORDION -->
                                <div class="accordion">
                                    <div class="panel-group">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="strong-header panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" href="#collapse-001">Fiyat Aralığı
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse-001" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="filters-range" data-min="10" data-max="320" data-step="5">
                                                        <div class="filter-widget"></div>
                                                        <div class="filter-value">
                                                            <input type="text" class="min">
                                                            <input type="text" class="max">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="strong-header panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" href="#collapse-002">Markalar
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse-002" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="filters-checkboxes myFilters" data-option-group="category" data-option-type="filter">
                                                        <div class="form-group">
                                                            <input type="checkbox" class="sr-only" id="filters-categories-all">
                                                            <label for="filters-categories-all" data-option-value="" class="selected isotopeFilter">Tüm Markalarda Arama</label>
                                                        </div>


                                                        <asp:Repeater ID="Repeater1" runat="server">
                                                            <ItemTemplate>
                                                                <div class="form-group">
                                                                    <input type="checkbox" class="sr-only" id="filters-categories-hoodies_and_sweatshirts">
                                                                    <label for="filters-categories-hoodies_and_sweatshirts" data-option-value=".cat<%#Eval("markaID") %>" class="isotopeFilter"><%#Eval("markaAd") %></label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>


                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="strong-header panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" href="#collapse-003">Size
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse-003" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="filters-size myFilters" data-option-group="size" data-option-type="filter">
                                                        <div class="form-group">
                                                            <input type="checkbox" class="sr-only" id="filters-size-all">
                                                            <label for="filters-size-all" data-option-value="" class="selected isotopeFilter">
                                                                <abbr title="All">All</abbr></label>
                                                        </div>
                                                        <div class="form-group">
                                                            <asp:Repeater ID="rptbedenkatile" runat="server">
                                                                <ItemTemplate>
                                                                    <input type="checkbox" class="sr-only" id='filters-size-<%#Eval("ozellikDetayi") %>'>
                                                                    <label for="filters-size-<%#Eval("ozellikDetayi") %>" data-option-value=".size<%#Eval("ozellikDetayId") %>" class="isotopeFilter"><%#Eval("ozellikDetayi") %></label>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                            <asp:Repeater ID="rptbeden" runat="server">
                                                                <ItemTemplate>
                                                                    <input type="checkbox" class="sr-only" id="filters-size-<%#Eval("ozellikDetayId") %>">


                                                                    <label for="filters-size-<%#Eval("ozellikDetayId") %>" data-option-value=".size<%#Eval("ozellikDetayId") %>" class="isotopeFilter"><%#Eval("ozellikDetayi") %></label>
                                                                </ItemTemplate>
                                                            </asp:Repeater>
                                                        </div>




                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="strong-header panel-title">
                                                    <a class="accordion-toggle" data-toggle="collapse" href="#collapse-004">Color
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapse-004" class="panel-collapse collapse in">
                                                <div class="panel-body">
                                                    <div class="filters-color myFilters" data-option-group="color" data-option-type="filter">
                                                        <div class="form-group">
                                                            <input type="checkbox" class="sr-only" id="filters-color-all">
                                                            <label for="filters-color-all" data-option-value="" class="selected isotopeFilter"><span class="filters-color-swatch" style="background: transparent">All</span></label>
                                                        </div>
                                                        <asp:Repeater ID="rptrenkkatile" runat="server">
                                                            <ItemTemplate>
                                                                <div class="form-group" data-toggle="tooltip" data-placement="bottom" title="<%#RenkCevir(Eval("renk")) %>">
                                                                    <input type="checkbox" class="sr-only" id='filters-color-<%#Eval("renkId") %>'>
                                                                    <label for="filters-color-<%#Eval("renkId") %>" data-option-value=".color<%#Eval("renkId") %>" class="isotopeFilter">Renk<span class="filters-color-swatch" style="background: <%#Eval("renk") %>"></span></label>
                                                                </div>
                                                            </ItemTemplate>

                                                        </asp:Repeater>

                                                        <asp:Repeater ID="rptrenk" runat="server">
                                                            <ItemTemplate>
                                                                <div class="form-group" data-toggle="tooltip" data-placement="bottom" title="<%#RenkCevir(Eval("renk")) %>">
                                                                    <input type="checkbox" class="sr-only" id="filters-color-<%#Eval("renkId") %>">
                                                                    <label for="filters-color-<%#Eval("renkId") %>" data-option-value=".color<%#Eval("renkId") %>" class="isotopeFilter">Renk<span class="filters-color-swatch" style="background: <%#Eval("renk") %>"></span></label>
                                                                </div>
                                                            </ItemTemplate>
                                                        </asp:Repeater>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- !ACCORDION -->
                            </div>
                        </div>
                        <!-- / #listFilters -->
                    </div>

                    <div class="clearfix visible-xs"></div>
                    <div class="col-sm-8 col-md-9">
                        <div class="row">
                            <div class="shop-list-filters col-sm-6 col-md-8">
                                <span class="filters-result-count"><span>24</span> results</span>
                            </div>
                            <div class="shop-list-filters col-sm-6 col-md-4">
                                <div class="filters-sort">
                                    <div class="btn-group myFilters" data-option-group="sortby" data-option-type="sortBy">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            Orjinal Sıraya Göre <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#" class="selected isotopeFilter" data-option-value="original-order" data-option-asc="true">Orjinal Sıraya Göre</a></li>
                                            <li><a href="#" class="isotopeFilter" data-option-value="date" data-option-asc="false">En Son Eklenene Göre</a></li>
                                            <li><a href="#" class="isotopeFilter" data-option-value="popular" data-option-asc="false">Görüntülenme Sayısına Göre</a></li>
                                            <li><a href="#" class="isotopeFilter" data-option-value="rating" data-option-asc="false">Verilen Oylara Göre</a></li>
                                            <li><a href="#" class="isotopeFilter" data-option-value="random" data-option-asc="false">Rastgele Ürün Getir</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-xs-12">


                                <!-- ISOTOPE GALLERY -->
                                <div id="isotopeContainer" class="shop-product-list isotope">
                                    <asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
                                        <ItemTemplate>

                                            <div class='isotope-item <asp:Literal ID="RenkClass" runat="server"></asp:Literal>  <asp:Literal ID="BedenClass" runat="server"></asp:Literal>  cat<%#Eval("markaID") %>' data-date="<%#tarihduzenle(Eval("urunEklenmeTarih")) %>" data-popular="<%#Eval("hit") %>" data-rating="4.0">
                                                <!-- SHOP FEATURED ITEM -->
                                                <div class="shop-item shop-item-featured overlay-element">
                                                    <div class="overlay-wrapper">
                                                        <a href="#">
                                                            <img src="/Resim/buyuk/<%#Eval("resimUrl") %>" alt=" "></a>
                                                        <div class="overlay-contents">
                                                            <div class="shop-item-actions">
                                                                <%--<button class="btn btn-primary btn-block">Sepete Ekle</button><button class="btn btn-default btn-block">--%>
                                                                <asp:Button ID="btnSepeteEkle" runat="server" Text="Sepete Ekle" class="btn btn-primary btn-block" OnClick="btnSepeteEkle_Click" />
                                                                <%--<asp:Button ID="Button1" runat="server" Text="Button" class="btn btn-primary btn-block" />--%>
                                                                <button class="btn btn-primary btn-block">
                                                                    <%--<a href="/urundetay.aspx?urunId=<%#Eval("id") %>" style="color: #fff;">Detayları Görüntüle</a></button></div></div>--%>
                                                                    <a href='<%# GetRouteUrl("urunIdyeGetir", new {urunId = Eval("id")}) %>' style="color: #fff;">Detayları Görüntüle</a></button>
                                                            </div>
                                                        </div>
                                                        </button>
                                                    </div>
                                                    <div class="item-info-name-price">
                                                        <h3><%#Eval("markaAd") %></h3>
                                                        <h4><a href="#"><%#Eval("urunAd") %>  <%#Eval("urunAciklama") %></a></h4>
                                                        <span class="price"><%#Eval("urunFiyat") %> TL</span>
                                                    </div>
                                                </div>
                                                <!-- !SHOP FEATURED ITEM -->
                                            </div>
                                        </ItemTemplate>
                                    </asp:Repeater>


                                </div>
                                <!-- !ISOTOPE GALLERY -->

                            </div>


                        </div>
                    </div>


                </div>
            </div>
            <!-- / row -->

        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->



</asp:Content>

