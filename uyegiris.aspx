﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="uyegiris.aspx.cs" Inherits="uyegiris" EnableEventValidation="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <section id="Content" role="main" style="min-height: 129px;">
      <!-- !container -->
      <div class="full-width section-emphasis-1 page-header">
          <div class="container">
              <header class="row">
                  <div class="col-md-12">
                      <h1 class="strong-header pull-left">
                          Hesabım
                      </h1>
                      <!-- BREADCRUMBS -->
                      <ul class="breadcrumbs list-inline pull-right">
                          <li>
                              <asp:HyperLink ID="hlHome" runat="server" NavigateUrl="<%$RouteUrl:RouteName=anasayfa%>">Anasayfa</asp:HyperLink></li><!--
                          --><li>
                              <asp:HyperLink ID="hlShop" runat="server" NavigateUrl="<%$RouteUrl:RouteName=anasayfa%>">Shop</asp:HyperLink></li><!--
                          --><li>Hesabım</li>
                      </ul>
                      <!-- !BREADCRUMBS -->
                  </div>
              </header>
          </div>
      </div><!-- !full-width -->
      <div class="container">
          <!-- !FULL WIDTH -->
          <!-- !SECTION EMPHASIS 1 -->

          <div class="row">
              <div class="col-md-6 space-right-20">
                  <section class="login element-emphasis-strong">
                      <h2 class="strong-header large-header">
                          Kullanıcı Girişi
                      </h2>

                      <asp:Label ID="lblDurum" runat="server" Text="" Visible="false"></asp:Label>

                      <form role="form" action="06-b-shop-checkout.html" method="post" novalidate="">
                          <div class="form-group">
                              <label for="email">Kullanıcı Adı veya Email</label>
                             <%-- <input type="email" class="form-control" id="email" required="">--%>
                              <asp:TextBox ID="txtEmail" runat="server" class="form-control" required="" OnTextChanged="txtEmail_TextChanged"></asp:TextBox>
                          </div>
                          <div class="form-group">
                              <label for="password">Şifre</label>
                             <%-- <input type="password" class="form-control" id="password" required="">--%>
                              <asp:TextBox ID="txtPassword" runat="server" type="password" class="form-control" required="deneme" OnTextChanged="txtPassword_TextChanged"  ></asp:TextBox>
                          </div>
                         <%-- <button type="submit" class="btn btn-primary pull-left">Login</button>--%>
                          <asp:Button ID="btnLogin" class="btn btn-primary pull-left" runat="server" Text="Giriş Yap" OnClick="btnLogin_Click" />
                          <%--<a href="#" class="btn btn-link pull-right">Forgot your password?</a>--%>
                          <asp:HyperLink ID="hlForgot" class="btn btn-link pull-right" runat="server" NavigateUrl="<%$RouteUrl:RouteName=sifremiunuttum%>">Şifremi Unuttum</asp:HyperLink>
                          <div class="clearfix"></div>
                      </form>
                  </section>
              </div>
              <div class="col-md-6 space-left-20">
                  <section class="element-emphasis-weak">
                      <h2 class="strong-header">
                          Yeni Müşteriler
                      </h2>
                      <p>
                          By creating an account with our store, you will be able to move through the checkout process
                          faster, store multiple shipping addresses, view and track your orders in your account and more
                      </p>
                      <%--<a href="08-b-shop-account-register.html" class="btn btn-default">
                          Kaydol
                      </a>--%>
                      <asp:HyperLink ID="hlKaydol" class="btn btn-default" runat="server" NavigateUrl="<%$RouteUrl:RouteName=kaydol%>">Kaydol</asp:HyperLink>
                  </section>
              </div>
          </div>

      </div>
  </section>
</asp:Content>

