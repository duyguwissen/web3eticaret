﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="odeme3.aspx.cs" Inherits="odeme3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">





    <section id="Content" role="main">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Checkout
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="index.html">Home</a></li>
                            <!--
                          -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                          -->
                            <li>Checkout</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <div class="row">
                <div class="col-md-7">
                    <!-- ALERT BOX INFO -->
                    <div class="alert alert-info alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <a href="08-a-shop-account-login.html">Login</a> or <a href="08-b-shop-account-register.html">create an account</a> for faster checkout
                    </div>
                    <!-- !ALERT BOX INFO -->
                </div>
                <div class="clearfix space-30"></div>
                <div class="col-sm-5 col-sm-push-7 space-left-30">
                    <section class="order-summary element-emphasis-weak">
                        <h3 class="strong-header element-header pull-left">Sipariş Özeti
                        </h3>
                        <a href="sepet-full.aspx" class="pull-right">Edit cart
                        </a>
                        <div class="clearfix"></div>
                        <!-- SHOP SUMMARY ITEM -->
                        <asp:Repeater ID="rptUrunler" runat="server">
                            <ItemTemplate>
                                <div class="shop-summary-item">
                                    <img src="/Resim/kucuk/<%#Eval("urun.ResimUrl") %>" alt="Shop item in cart">
                                    <header class="item-info-name-features-price">
                                        <h4><a href="#"><%#Eval("urun.urunAd") %></a></h4>
                                        <span class="features"><%#Eval("urun.Renk") %>, <%#Eval("urun.urunDetayi.ozellikDetayi") %></span><br>
                                        <span class="quantity"><%#Eval("urunAdet") %></span><b>&times;</b><span class="price"><%#Eval("urun.UrunYeniFiyat") %></span>
                                    </header>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <!-- !SHOP SUMMARY ITEM -->
                        <!-- SHOP SUMMARY ITEM -->
                        <%--<div class="shop-summary-item">
                          <img src="/images/demo-content/cart-purchased-small-2.jpg"  alt="Shop item in cart">
                          <header class="item-info-name-features-price">
                              <h4><a href="04-shop-product-single.html">Denim Jacket in Oversized Boyfriend Fit in Vintage Wash</a></h4>
                              <span class="features">Light Blue, M</span><br>
                              <span class="quantity">1</span><b>&times;</b><span class="price">$65.00</span>
                          </header>
                      </div>--%>
                        <!-- !SHOP SUMMARY ITEM -->
                        <dl class="order-summary-price">
                            <dt>Alt Toplam</dt>
                            <dd><strong><%=ToplamTutar() %> TL</strong></dd>
                            <dt>Kargo Tutarı</dt>
                            <dd><%=0 %> TL</dd>
                            <dt class="total-price">Sipariş Tutarı</dt>
                            <dd class="total-price"><strong><%=ToplamTutar() %> TL</strong></dd>
                        </dl>
                    </section>
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <div class="col-sm-7 col-sm-pull-5">
                    <section class="checkout checkout-step-1 checkout-step-previous element-emphasis-weak">
                        <h2 class="strong-header element-header pull-left">1. Fatura Adresi
                        </h2>
                        <a href="06-a-shop-checkout.html" class="btn btn-default btn-small pull-right">Edit
                        </a>

                        <div class="clearfix"></div>
                        <p>
                            <asp:Label ID="lblAd" runat="server" Text=""></asp:Label><br>
                            <asp:Label ID="lblAdres" runat="server" Text=""></asp:Label><br>
                            <asp:Label ID="lblIlce" runat="server" Text=""></asp:Label>,
                            <asp:Label ID="lblPosta" runat="server" Text=""></asp:Label><br>
                            <asp:Label ID="lblIl" runat="server" Text=""></asp:Label>
                        </p>
                        <h4 class="strong-header">Contact information
                        </h4>

                        <p>
                            <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label><br>
                            <asp:Label ID="lblCepTel" runat="server" Text=""></asp:Label>
                        </p>
                    </section>
                    <section class="checkout checkout-step-2 checkout-step-previous element-emphasis-weak">
                        <h2 class="strong-header element-header pull-left">2. Kargo Adresi
                        </h2>
                        <a href="06-b-shop-checkout.html" class="btn btn-default btn-small pull-right">Edit
                        </a>
                        <div class="clearfix"></div>
                        <p>
                            <asp:Label ID="lblKargoAd" runat="server" Text=""></asp:Label><br>
                            <asp:Label ID="lblKargoAdres" runat="server" Text=""></asp:Label><br>
                            <asp:Label ID="lblKargoIlce" runat="server" Text=""></asp:Label>,
                            <asp:Label ID="lblKargoPosta" runat="server" Text=""></asp:Label><br>
                            <asp:Label ID="lblKargoIl" runat="server" Text=""></asp:Label>
                        </p>
                    </section>
                    <section class="checkout checkout-step-3 checkout-step-current element-emphasis-strong clearfix">
                        <h2 class="strong-header element-header">3. Ödeme Seçenekleri
                        </h2>
                        <div role="form" class="form">
                            <div class="form-group">
                                <input type="radio" name="payment-methods" class="large sr-only" id="direct-bank-transfer" value="bank-transfer" checked>
                                <label for="direct-bank-transfer">Direct bank transfer</label>
                            </div>
                            <span class="help-block">Make your payment directly into our bank account. Please use your Order ID as the payment
                                  reference. Your order will not be shipped until the funds have cleared in our account.
                            </span>
                            <div class="form-group">
                                <input type="radio" name="payment-methods" class="large sr-only" id="cheque-payment" value="cheque">
                                <label for="cheque-payment">Checkque payment</label>
                            </div>
                            <div class="form-group">
                                <input type="radio" name="payment-methods" class="large sr-only" id="paypal" value="paypal">
                                <label for="paypal">Paypal <a href="#">
                                    <img src="/images/paypal-logo.png" alt="Paypal"></a></label>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Place order</button>
                            <div class="clearfix"></div>
                        </div>
                    </section>
                </div>
            </div>

        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->


</asp:Content>


