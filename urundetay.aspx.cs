﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class urundetay : System.Web.UI.Page
{

    UrunDetay ud;
    Siparis siparsim;
    Sepet sepetim;
    Urun u;
    Yorum y;
    UrunResim resim;
    Uye uye;
    protected int urunId;
    double puan;
    List<int> stokAdetleri;
    //int rate;

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Request.QueryString["urunId"] != null)
        //{
        if (RouteData.Values["urunId"] != null)
        {

            urunId = Convert.ToInt32(RouteData.Values["urunId"].ToString());
            //urunId = Convert.ToInt32(Request.QueryString["urunId"]);
            ud = new UrunDetay();
            ud.urunId = urunId;

            ddlBeden.DataTextField = "ozellikDetayi";
            ddlBeden.DataValueField = "ozellikDetayId";

            DataTable bedenler = ud.urunIdileBedenGetir();
            stokAdetleri = new List<int>();

            foreach (DataRow item in bedenler.Rows)
            {
                stokAdetleri.Add(Convert.ToInt32(item["stokAdet"].ToString()));
            }
            if (!IsPostBack)
            {
                ddlBeden.DataSource = bedenler;
                ddlBeden.DataBind();
            }

            resim = new UrunResim();
            resim.urunId = urunId;


            DataTable resimler = resim.urunResimleriGetir();
            int sayi = resimler.Rows.Count;
            //u.resimUrl = resimler.Rows[0]["resimUrl"].ToString();
            u = new Urun();

            DataRow resimSatiri = resimler.Rows[0];
            u.resimUrl = resimSatiri["resimUrl"].ToString();

            rptResimler.DataSource = resimler;
            rptResimler.DataBind();

            rptKucukResimler.DataSource = resimler;
            rptKucukResimler.DataBind();


            #region son kodlar

            u.id = urunId;

            //DataRow urunOzellikleri = u.urunIdileUrunGetir(urunId);
            DataRow urunOzellikleri = u.urunuGetir();
            u.urunKod = urunOzellikleri["urunKod"].ToString();
            u.urunAd = urunOzellikleri["urunAd"].ToString();
            u.Renk = urunOzellikleri["renk"].ToString();

            lblUrunAd.Text = urunOzellikleri["urunAd"].ToString();
            lblUrunKod.Text = "Ürün Kodu: " + urunOzellikleri["urunKod"].ToString();
            lblUrun.Text = lblUrunAd.Text;

            //Urun a = new Urun();

            double eskiFiyat = Convert.ToDouble(urunOzellikleri["urunFiyat"].ToString());
            lblPriceOld.Text = eskiFiyat.ToString();
            double indirimOrani = Convert.ToDouble(urunOzellikleri["indirimOrani"].ToString());
            double urunYeniFiyat = eskiFiyat * (100 - indirimOrani) / 100;
            u.urunFiyat = Convert.ToDecimal(eskiFiyat);
            u.urunYeniFiyat = Convert.ToDecimal(urunYeniFiyat);
            //u.resimUrl = urunOzellikleri["resimUrl"].ToString();

            lblPrice.Text = urunYeniFiyat.ToString();
            #endregion

            #region eski kodlar

            //if (u.UrunKampanyadaMi(urunId))
            //{
            //    DateTime baslamaTarihi = Convert.ToDateTime(urunOzellikleri["baslangicTarih"].ToString());
            //    DateTime bitisTarihi = Convert.ToDateTime(urunOzellikleri["bitisTarih"].ToString());

            //    lblPriceOld.Text = eskiFiyat.ToString();
            //    double indirimOrani = Convert.ToDouble(urunOzellikleri["indirimOrani"].ToString());
            //    double urunYeniFiyat = eskiFiyat * (100 - indirimOrani) / 100;

            //    lblPrice.Text = urunYeniFiyat.ToString();
            //}
            //else
            //{
            //    lblPriceOld.Text = eskiFiyat.ToString();
            //    lblPrice.Text = eskiFiyat.ToString();
            //}


            #endregion


            y = new Yorum();
            DataTable yorumlar = y.YorumlariGetir(urunId);
            rptYorumlar.DataSource = yorumlar;
            rptYorumlar.DataBind();

            int yorumSayisi = yorumlar.Rows.Count;
            lblYorum.Text = "Yorumlar(" + yorumSayisi + ")";
            lblYorumSayisi.Text = yorumSayisi + " yorum";
            if (yorumSayisi != 0)
            {
                foreach (DataRow item in yorumlar.Rows)
                {
                    puan += Convert.ToDouble(item["puan"].ToString());
                }
                puan = puan / yorumSayisi;
            }
        }

    }

    //protected void rptOzellikler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    //{
    //    Repeater rp = (Repeater)e.Item.FindControl("rptOzellikDetayi");
    //    rp.DataSource = ud.OzellikDetaylariGetir(urunId, DataBinder.Eval(e.Item.DataItem, "ozellikAdi").ToString());
    //    rp.DataBind();
    //}



    public string Puanla()
    {
        if (puan != 0)
        {
            double yeniPuan = Convert.ToDouble(puan);
            string yPuan = yeniPuan.ToString();
            yPuan.IndexOf(",");
            int tamPuan = Convert.ToInt32(yPuan[0].ToString());

            if (yPuan.Length > 1)
            {
                int kusurat = Convert.ToInt32(yPuan[2].ToString());

                if (kusurat != 0)
                {
                    kusurat = 5;
                    string a = tamPuan + "." + kusurat;
                    return tamPuan + "." + kusurat;
                }
            }
            return yeniPuan.ToString();
        }
        else
        {
            return puan.ToString();
        }



    }


    protected void btnYorumGonder_Click(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null)
        {
            Uye u = (Uye)Session["kullanici"];

            y = new Yorum();
            y.urunID = urunId;
            y.uyeID = u.ID;

            Response.Write("<script>alert('Yorumunuz Alınmıştır.Teşekkür ederiz.')</script>");

        }
        else
        {
            Response.Write("<script>alert('Üye girişi yapmadan oy veremezsiniz')</script>");

        }
    }
    protected void btnSepeteEkle_Click(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null)
        {
            int adet = Convert.ToInt32(quantity.Value);
            if (stokAdetleri[ddlBeden.SelectedIndex] < Convert.ToInt32(quantity.Value))
            {
                Response.Write("<script>alert('Stok adetini aştınız')</script>");
            }
            else
            {
                try
                {
                    //u.urunDetayi = new UrunDetay { stokAdet = Convert.ToInt32(ddlBeden.SelectedValue), ozellikDetayi = ddlBeden.SelectedItem.Text };
                    u.urunDetayi = new UrunDetay { ozellikDetayId = Convert.ToInt32(ddlBeden.SelectedValue), ozellikDetayi = ddlBeden.SelectedItem.Text };


                    uye = (Uye)Session["kullanici"];
                    siparsim = new Siparis();
                    siparsim.siparisTarih = DateTime.Now;
                    siparsim.urunAdet = Convert.ToInt32(quantity.Value.ToString());

                    //siparsim.urunId = urunId;

                    siparsim.urun = u;//ürünün kendisi gidiyor.
                    siparsim.uyeId = uye.ID;
                    siparsim.urunFiyat = Convert.ToDecimal(lblPrice.Text.ToString());
                    siparsim.toplamTutar = siparsim.urunFiyat * siparsim.urunAdet;
                    sepetim = (Sepet)Session["sepettekiler"];

                    if (Session["sepettekiler"] == null || sepetim.siparisListesi.Count == 0)
                    {
                        sepetim = new Sepet();
                        List<Siparis> siparislerim = new List<Siparis>();
                        siparislerim.Add(siparsim);
                        sepetim.siparisListesi = siparislerim;
                        sepetim.toplamTutar = siparsim.toplamTutar;
                        sepetim.uyeId = uye.ID;
                        Session["sepettekiler"] = sepetim;
                    }


                    else
                    {
                        bool bulunduMu = false;
                        foreach (Siparis item in sepetim.siparisListesi)
                        {
                            if (item.urun.id == u.id && item.urun.urunDetayi.ozellikDetayi == u.urunDetayi.ozellikDetayi)
                            {
                                item.urunAdet += siparsim.urunAdet;
                                item.toplamTutar += siparsim.toplamTutar;
                                sepetim.toplamTutar += siparsim.toplamTutar;
                                bulunduMu = true;
                                break;
                            }
                        }
                        if (!bulunduMu)
                        {
                            sepetim.siparisListesi.Add(siparsim);
                            sepetim.toplamTutar += siparsim.toplamTutar;
                        }


                    }

                    Response.Write("<script>alert('Sipariş sepetinize eklenmiştir.');window.location.href='" + GetRouteUrl("urunIdyeGetir", new { urunId = u.id }) + "'</script>");
                    //"urunId=" + u.id + ""

                }
                catch (Exception)
                {
                    Response.Write("<script>alert('Sepete eklenme sırasında hata oluştu.')</script>");
                }
            }

        }
        else
        {
            Response.Write("<script>alert('Öncelikle giriş yapmanız gerekmektedir.')</script>");
        }
    }

}