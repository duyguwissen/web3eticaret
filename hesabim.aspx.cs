﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hesabim : System.Web.UI.Page
{
    Adres adres;
    Il il;
    Ilce ilce;
    Uye uye;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null & !IsPostBack)
        {
            adres = new Adres();

            lblUyeAd.Text = ((Uye)(Session["kullanici"])).ad + " " + ((Uye)(Session["kullanici"])).soyad;
            lblUyeEmail.Text = ((Uye)(Session["kullanici"])).email;
            adres.uyeID = ((Uye)(Session["kullanici"])).ID;

            FaturaAdresGetir();
            TeslimatBilgileriGetir();
            SiparisleriGetir(false);

        }
        else
        {
            Response.Write("<script>alert('Önce giriş yapmalısınız.');window.location.href='" + GetRouteUrl("anasayfa", null) + "'</script>");
        }


    }
    public void FaturaAdresGetir()
    {
        /*fatura bilgileri*/
        DataRow faturaAdres = adres.FaturaAdresGetir();
        
        if (faturaAdres != null)
        {
            //rptFaturaBilgileri.DataSource = faturaAdres;
            //rptFaturaBilgileri.DataBind();
            lblIsim.Text = faturaAdres["adSoyad"].ToString();
            lblFaturaAdres.Text = faturaAdres["AdresDetay"].ToString();
            lblFaturaIl.Text = faturaAdres["il"].ToString();
            lblFaturaIlcePostaKod.Text = faturaAdres["ilce"].ToString() + "," + faturaAdres["postaKodu"].ToString();
            //ltrFaturaIlcePostaKod.Text = "deneme";
            PopUpFaturaDoldur(faturaAdres);
            
        }
       
    }
    public void TeslimatBilgileriGetir()
    {

        /*teslimat bilgileri*/
        DataRow teslimatAdres = adres.TeslimatAdresGetir();
        if (teslimatAdres != null)
        {
            lblIsim2.Text = teslimatAdres["adSoyad"].ToString();
            lblTeslimatAdres.Text = teslimatAdres["AdresDetay"].ToString();
            lblTeslimatIl.Text = teslimatAdres["il"].ToString();
            lblTeslimatIlce.Text = teslimatAdres["ilce"].ToString() + "," + teslimatAdres["postaKodu"].ToString();

            PopUpTeslimatDoldur(teslimatAdres);
        }


    }

    public void SiparisleriGetir(bool hepsiMi)
    {
        
        int uyeId = adres.uyeID;
        Siparis s = new Siparis();
        s.uyeId = uyeId;
        if (!hepsiMi)
        {
            rptSiparisler.DataSource = s.BirkacSiparisGetir();
            rptSiparisler.DataBind();
        }
        else
        {
            rptSiparisler.DataSource = s.TumSiparisleriGetir();
            rptSiparisler.DataBind();
        }

    }

    public string TarihDuzenle(object tarih)
    {

        DateTime t = (DateTime)tarih;
        string son = String.Format("{0:D}", t);
        return son;
    }

    protected void lbtnCikis_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.RedirectToRoute("anasayfa", null);

    }
    protected void btnTumSiparisler_Click(object sender, EventArgs e)
    {
        SiparisleriGetir(true);
        btnTumSiparisler.Visible = false;
    }
    protected void rptSiparisler_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
    }
   

    protected void lbtnKaydet_Click(object sender, EventArgs e)
    {
        LinkButton b = (LinkButton)sender;
        var deger = b.CommandArgument;
    }
    public void PopUpFaturaDoldur(DataRow faturaAdres) {

        il = new Il();
        ddlFaturaSehir.DataSource = il.IlleriGetir();
        ddlFaturaSehir.DataBind();

        ddlFaturaSehir.SelectedValue = faturaAdres["sehirID"].ToString();

        int value = Convert.ToInt32(ddlFaturaSehir.SelectedValue);
        ilce = new Ilce();
        ddlFaturaIlce.DataSource = ilce.IlceleriGetir(value);
        ddlFaturaIlce.DataBind();
        ddlFaturaIlce.SelectedValue = faturaAdres["ilceID"].ToString();
        txtFaturaAdSoyad.Text = faturaAdres["adSoyad"].ToString();
        txtFaturaAdres.Text = faturaAdres["AdresDetay"].ToString();
        txtFaturaPosta.Text = faturaAdres["postaKodu"].ToString();
        int deger=Convert.ToInt32(faturaAdres["cepTel"].ToString());
        txtFaturaCep.Text = faturaAdres["cepTel"].ToString().Trim();
        txtFaturaEvTel.Text = faturaAdres["evTel"].ToString().Trim();

    
    }

    public void PopUpTeslimatDoldur(DataRow teslimatAdres)
    {

        il = new Il();
        ddlTeslimatSehir.DataSource = il.IlleriGetir();
        ddlTeslimatSehir.DataBind();

        ddlTeslimatSehir.SelectedValue = teslimatAdres["sehirID"].ToString();

        int value = Convert.ToInt32(ddlFaturaSehir.SelectedValue);
        ilce = new Ilce();
        ddlTeslimatIlce.DataSource = ilce.IlceleriGetir(value);
        ddlTeslimatIlce.DataBind();
        //ddlTeslimatIlce.SelectedValue = teslimatAdres["ilceID"].ToString();
        txtTeslimatAd.Text = teslimatAdres["adSoyad"].ToString();
        txtTeslimatAdres.Text = teslimatAdres["AdresDetay"].ToString();
        txtTeslimatPosta.Text = teslimatAdres["postaKodu"].ToString();
        txtTeslimatCep.Text = teslimatAdres["cepTel"].ToString().Trim();
        txtTeslimatEvTel.Text = teslimatAdres["evTel"].ToString().Trim();


    }

    
   
    protected void ddlTeslimatSehir_SelectedIndexChanged(object sender, EventArgs e)
    {
        TeslimatAdres.Visible = true;
        int value = Convert.ToInt32(ddlTeslimatSehir.SelectedValue);
        ilce = new Ilce();
        ddlTeslimatIlce.DataSource = ilce.IlceleriGetir(value);
        ddlTeslimatIlce.DataBind();

        ClientScript.RegisterStartupScript(this.GetType(), "aKey", "DuzenleSayfasiGetir(" + this.ddlTeslimatSehir.ClientID + ");", true);
    }
    protected void ddlFaturaSehir_SelectedIndexChanged(object sender, EventArgs e)
    {
        FaturaAdres.Visible = true;
        int value = Convert.ToInt32(ddlFaturaSehir.SelectedValue);
        ilce = new Ilce();
        ddlFaturaIlce.DataSource = ilce.IlceleriGetir(value);
        ddlFaturaIlce.DataBind();

        ClientScript.RegisterStartupScript(this.GetType(), "bKey", "DuzenleSayfasiGetir("+this.ddlFaturaSehir.ClientID+");", true);
    }
    protected void lbtnFaturaKaydet_Click(object sender, EventArgs e)
    {
        adres = new Adres();
        adres.adSoyad = txtFaturaAdSoyad.Text;
        adres.cepTel = txtFaturaCep.Text;
        adres.adresDetay = txtFaturaAdres.Text;
        adres.adresTanimi = "Fatura";
        adres.evTel = txtFaturaEvTel.Text;
        adres.postaKodu = txtFaturaPosta.Text;
        adres.uyeID = ((Uye)(Session["kullanici"])).ID;
        adres.sehirID = Convert.ToInt32(ddlFaturaSehir.SelectedValue);
        adres.ilceID = Convert.ToInt32(ddlFaturaIlce.SelectedValue);
        bool kaydedildimi = adres.AdresDegistir(adres);
        
        if (kaydedildimi)
        {
            FaturaAdresGetir();
            //ClientScript.RegisterStartupScript(this.GetType(), "AlertCode", "alert('Kaydedildi');", true);
           
           Response.Write("<script>alert('Bilgileriniz kaydedildi.')</script>");
            //ClientScript.RegisterStartupScript(this.GetType(), "AKey", "DuzenleSayfasiGetir(" + this.ddlFaturaSehir.ClientID + ");", true);

        }
        else
        {
            Response.Write("<script>alert('Bilgileriniz kaydedilirken hata oluştu.Lütfen daha sonra tekrar deneyin.')</script>");
        }
    }
    protected void lbtnTeslimatKaydet_Click(object sender, EventArgs e)
    {
        adres = new Adres();
        adres.adSoyad = txtTeslimatAd.Text;
        adres.cepTel = txtTeslimatCep.Text;
        adres.adresDetay = txtTeslimatAdres.Text;
        adres.adresTanimi = "Teslimat";
        adres.evTel = txtTeslimatEvTel.Text;
        adres.postaKodu = txtTeslimatPosta.Text;
        adres.uyeID = ((Uye)(Session["kullanici"])).ID;
        adres.sehirID = Convert.ToInt32(ddlTeslimatSehir.SelectedValue);
        adres.ilceID = Convert.ToInt32(ddlTeslimatIlce.SelectedValue);

        bool kaydedildimi = adres.AdresDegistir(adres);
        if (kaydedildimi)
        {
            TeslimatBilgileriGetir();
            Response.Write("<script>alert('Bilgileriniz kaydedildi.')</script>");
        }
        else
        {
            Response.Write("<script>alert('Bilgileriniz kaydedilirken hata oluştu.Lütfen daha sonra tekrar deneyin.')</script>");
        }
    }
    protected void btnSifreDegitir_Click(object sender, EventArgs e)
    {
        /*sifrenin dogrulugu kontrol edilecek*/
        uye = new Uye();
        uye.ID = ((Uye)(Session["kullanici"])).ID;
        uye.sifre = txtEskiSifre.Text;
        bool sifreDogruMu=uye.DogruMu();
        if (sifreDogruMu)
        {
            uye.SifreDegistir(uye.sifre, uye.ID);
            Response.Write("<script>alert('Şifreniz değiştirilmiştir.')</script>");
        }
        else
        {
            Response.Write("<script>alert('Mevcut şifrenizi hatalı girdiniz.')</script>");
        }   
    }
}