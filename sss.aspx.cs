﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sss : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        SorularModel sm = new SorularModel();
        
        string Tur =Request.QueryString["Tur"];
        if (Tur == "Iade")
            Repeater1.DataSource = sm.TumSorulariGetir("Iade");
        else if(Tur=="Siparis")
            Repeater1.DataSource = sm.TumSorulariGetir("Siparis");
        else
            Repeater1.DataSource = sm.TumSorulariGetir("Kargo");
        Repeater1.DataBind();
        
    }
}