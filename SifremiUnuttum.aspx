﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SifremiUnuttum.aspx.cs" Inherits="ForgotYourPassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="container">
        <!-- !FULL WIDTH -->
        <!-- !SECTION EMPHASIS 1 -->

        <div class="row" style="margin-top: 50px;">
            <div class="col-md-6 space-right-20">
                <section class="login element-emphasis-strong" style="margin-bottom: 200px; height: 200px;">
                    <h2 class="strong-header large-header">Şifreni Sıfırla
                    </h2>
                    <asp:Label ID="lblMesaj" runat="server" Text="" ForeColor="red"></asp:Label>

                        <table class="nav-justified">
                            <tr>
                                <td>
                                    <label for="email">Email address</label></td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control" required="" type="email" Width="185px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td>
                                    <asp:Button ID="btnForgot" class="btn btn-primary pull-left" runat="server" Text="Gönder" OnClick="btnForgot_Click" /></td>
                            </tr>

                        </table>
                </section>
            
        </div>

    </div>
    </div>
</asp:Content>

