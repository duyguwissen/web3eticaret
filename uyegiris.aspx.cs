﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class uyegiris : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        Uye uye = new Uye();
        uye.GirisYap(txtEmail.Text, txtPassword.Text);
        if (uye.kullaniciAdi == null)
        {
            this.lblDurum.Visible = true;
            this.lblDurum.ForeColor = System.Drawing.Color.Red;
            this.lblDurum.Text = "Kullanıcı Adı veya Şifre Yanlış";
        }
        else
        {
            this.lblDurum.Visible = false;
            //Response.Redirect("")
            Session["kullanici"] = uye;
            Response.Redirect(GetRouteUrl("anasayfa", null));
        }

        txtEmail.Text = "";
        txtPassword.Text = "";
    }
    protected void txtPassword_TextChanged(object sender, EventArgs e)
    {
        lblDurum.Visible = false;
    }
    protected void txtEmail_TextChanged(object sender, EventArgs e)
    {

    }
}