﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="hesabim_adreslerim.aspx.cs" Inherits="hesabim_adreslerim" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">





    <section id="Content" role="main">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div>
        <!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class="strong-header pull-left">Hesabım
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="index.html">Anasayfa</a></li>
                            <!--
                          -->
                            <li><a href="03-shop-products.html">Shop</a></li>
                            <!--
                          -->
                            <li>Checkout</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div>
        <!-- !full-width -->
        <div class="container">
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

            <section class="row">
                <div class="col-sm-3">
                    <nav class="shop-section-navigation element-emphasis-weak">
                        <ul class="list-unstyled">
                            <li>
                                <asp:HyperLink ID="hlKontrol" runat="server" NavigateUrl="<%$RouteUrl:RouteName=hesap%>">Hesap Kontrol Paneli</asp:HyperLink>

                            </li>
                            <li>
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="<%$RouteUrl:RouteName=hesapbilgi%>">Hesap Bilgileri</asp:HyperLink>

                            </li>
                            <li>
                                <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="<%$RouteUrl:RouteName=siparisler%>">Ürünlerim</asp:HyperLink>
                            </li>
                            <li class="active"><span>Adres Defteri</span></li>
                            <li>
                                <asp:LinkButton ID="lbtnCikis" runat="server" OnClick="lbtnCikis_Click">Çıkış</asp:LinkButton>
                            </li>
                        </ul>
                    </nav>
                </div>
                <div class="clearfix visible-xs space-30"></div>
                <div class="col-sm-9 space-left-30">
                    <h2 class="strong-header large-header">Adres Defteri</h2>
                    <div class="row">
                        <div class="col-sm-6">
                            <h3 class="strong-header">Fatura Adresi</h3>
                            <p>
                                <br>
                                <asp:Label ID="lblIsim" runat="server" Text="Adres belirtilmedi."></asp:Label><br>
                                <asp:Label ID="lblFaturaAdres" runat="server" Text=""></asp:Label><br>
                                <asp:Label ID="lblFaturaIlcePostaKod" runat="server" Text=""></asp:Label><br>
                                <asp:Label ID="lblFaturaIl" runat="server" Text=""></asp:Label><br>
                                <br>
                            </p>
                            <a href="javascript:pencereGoster();" class="btn btn-default btn-small">Düzenle</a>
                            <hr class="visible-xs">
                        </div>
                        <div class="col-sm-6">
                            <h3 class="strong-header">Teslimat Adresi</h3>
                            <p>
                                <br>
                                <asp:Label ID="lblIsim2" runat="server" Text="Adres belirtilmedi."></asp:Label><br>
                                <asp:Label ID="lblTeslimatAdres" runat="server" Text=""></asp:Label><br>
                                <asp:Label ID="lblTeslimatIlce" runat="server" Text=""></asp:Label><br>
                                <asp:Label ID="lblTeslimatIl" runat="server" Text=""></asp:Label><br>
                                <br>
                            </p>
                            <a href="09-d-shop-account-address-book.html" class="btn btn-default btn-small">Düzenle</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div>
    <!-- fixes floating problems when mobile menu is visible -->

</asp:Content>

