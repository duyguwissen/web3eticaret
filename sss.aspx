﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="sss.aspx.cs" Inherits="sss" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    

  <section id="Content" role="main">
      <div class="container">

          <!-- SECTION EMPHASIS 1 -->
          <!-- FULL WIDTH -->
      </div><!-- !container -->
      <div class="full-width section-emphasis-1 page-header">
          <div class="container">
              <header class="row">
                  <div class="col-md-12">
                      <h1 class="strong-header pull-left">
                          MÜŞTERİ HİZMETLERİ
                      </h1>
                      <!-- BREADCRUMBS -->
                      <ul class="breadcrumbs list-inline pull-right">
                          <li><a href="index.html">Anasayfa</a></li><!--
                          --><li>Sayfalar</li><!--
                          --><li>Müşteri Hizmetleri</li>
                      </ul>
                      <!-- !BREADCRUMBS -->
                  </div>
              </header>
          </div>
      </div><!-- !full-width -->
      <div class="container">
          <!-- !FULL WIDTH -->
          <!-- !SECTION EMPHASIS 1 -->

          <section class="row">
              <div class="col-sm-3">
                  <nav class="shop-section-navigation element-emphasis-weak">
                      <ul class="list-unstyled">
                          <li class="active"><a href="?Tur=Siparis">SİPARİŞ</a></li>
                          <li><a href="?Tur=Kargo">KARGO</a></li>          
                          <li><a href="?Tur=Iade">İADE</a></li>
                      </ul>
                  </nav>
              </div>
              <div class="clearfix visible-xs space-30"></div>
              <div class="col-sm-9 space-left-30">
                  <h2 class="strong-header large-header">SIK SORULAN SORULAR</h2>
                  <!-- ACCORDION -->
                    
                  <div class="accordion faq">
                               <asp:Repeater ID="Repeater1" runat="server">
                                  <ItemTemplate>
                                      
                      <div class="panel-group" id="accordion-<%#Eval ("ID") %>">
                          <div class="panel panel-default">
                              
                              <div class="panel-heading">
                                  <h4 class="panel-title">
                                      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-<%#Eval ("ID") %>" href="#collapse-<%#Eval ("ID") %>">
                                          <%#Eval ("Soru") %>
                                      </a>
                                  </h4>
                              </div>
                              <div id="collapse-<%#Eval ("ID") %>" class="panel-collapse collapse in">
                                  <div class="panel-body">
                                     <%#Eval("Cevap") %>
                                  </div>
                              </div>
                                   
                          </div>
                          
                           
                          
                          
                      </div>
                         </ItemTemplate>
                        </asp:Repeater>  
                  </div>

                  <!-- !ACCORDION -->
              </div>
          </section>
      </div>
  </section>

  <div class="clearfix visible-xs visible-sm"></div> <!-- fixes floating problems when mobile menu is visible -->



</asp:Content>

