﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="sepet-empty.aspx.cs" Inherits="sepet_empty" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


    
  <section id="Content" role="main">
      <div class="container">

          <!-- SECTION EMPHASIS 1 -->
          <!-- FULL WIDTH -->
      </div><!-- !container -->
      <div class="full-width section-emphasis-1 page-header">
          <div class="container">
              <header class="row">
                  <div class="col-md-12">
                      <h1 class="strong-header pull-left">
                          Shopping cart
                      </h1>
                      <!-- BREADCRUMBS -->
                      <ul class="breadcrumbs list-inline pull-right">
                          <li><a href="index.html">Home</a></li><!--
                      --><li><a href="03-shop-products.html">Shop</a></li><!--
                      --><li>Shopping cart</li>
                      </ul>
                      <!-- !BREADCRUMBS -->
                  </div>
              </header>
          </div>
      </div><!-- !full-width -->
      <div class="container">
          <!-- !FULL WIDTH -->
          <!-- !SECTION EMPHASIS 1 -->

          <div class="row shop-cart-empty">
              <div class="col-xs-12">
                  <h1 class="strong-header">
                      Sepetiniz<br>
                      Boş
                  </h1>
                  <p>
                      You have no items in your shopping cart.
                  </p>
                  <%--<a href='<%#GetRouteUrl("anasayfa",null) %>' class="btn btn-primary">Alışverişe Devam Et</a>--%>
                  <asp:HyperLink ID="HyperLink1" runat="server" class="btn btn-primary"  NavigateUrl="<%$RouteUrl:RouteName=anasayfa%>">Alışverişe Devam Et</asp:HyperLink>
              </div>
          </div>
      </div>
  </section>

  <div class="clearfix visible-xs visible-sm"></div> <!-- fixes floating problems when mobile menu is visible -->


</asp:Content>

