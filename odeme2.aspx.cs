﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class odeme2 : System.Web.UI.Page
{
    List<Siparis> sepettekiler;
    Il il = new Il();
    Ilce ilce = new Ilce();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null)
        {
            /*fatura adresi kısmının doldurulması*/
            FaturaAdresGetir();

            Sepet sepetim = (Sepet)Session["sepettekiler"];
            sepettekiler = sepetim.siparisListesi;

            rptUrunler.DataSource = sepettekiler;
            rptUrunler.DataBind();
        }
        else
        {
            Response.Write("<script>alert('Buraya girmeye yetkiniz bulunmamaktadır.');window.location.href='" + GetRouteUrl("anasayfa", null) + "'</script>");
        }

    }
    protected void ddlKendi_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlKendi.SelectedIndex == 1)
        {
            il = new Il();
            ddlSehir.DataSource = il.IlleriGetir();
            ddlSehir.DataBind();
            int secilenIl = Convert.ToInt32(ddlSehir.SelectedValue);
            ilce = new Ilce();
            ddlIlce.DataSource = ilce.IlceleriGetir(secilenIl);
            ddlIlce.DataBind();
            KargoAdres.Visible = true;

        }
        else
        {
            KargoAdres.Visible = false;
        }
        //int secilenIl = Convert.ToInt32(ddlSehir.SelectedValue);
        //ilce = new Ilce();
        //ddlIlce.DataSource = ilce.IlceleriGetir(secilenIl);
        //ddlIlce.DataBind();
    }
    protected void ddlSehir_SelectedIndexChanged(object sender, EventArgs e)
    {
        int secilenIl = Convert.ToInt32(ddlSehir.SelectedValue);
        ilce = new Ilce();
        ddlIlce.DataSource = ilce.IlceleriGetir(secilenIl);
        ddlIlce.DataBind();
    }
    protected void btnDevam_Click(object sender, EventArgs e)
    {
        Adres adres = new Adres();
        if (ddlKendi.SelectedIndex == 0)
        {
            /*kendi adresini gönder*/

            adres.defaultAdres = Convert.ToBoolean(ddlKendi.SelectedIndex + 1);
            adres.uyeID = ((Uye)Session["kullanici"]).ID;
            DataRow adresi = adres.AdresGetir();
            if (adresi == null)
            {
                Response.Write("<script>alert('Bireysel adresiniz sistemde kayıtlı değildir.')</script>");
                return;
            }
            adres.adSoyad = adresi["adSoyad"].ToString();
            adres.cepTel = adresi["cepTel"].ToString();
            adres.evTel = adresi["evTel"].ToString();
            adres.postaKodu = adresi["postaKodu"].ToString();
            adres.email = adresi["email"].ToString();
            adres.sehirID = Convert.ToInt32(adresi["sehirId"].ToString());
            adres.ilceID = Convert.ToInt32(adresi["ilceId"].ToString());
            Session["KargoAdres"] = adres;
        }
        else
        {
            adres.defaultAdres = Convert.ToBoolean(ddlKendi.SelectedIndex - 1);
            adres.adSoyad = Request.Form["Ad"] + " " + Request.Form["Soyad"];
            adres.cepTel = Request.Form["CepTel"];
            adres.evTel = Request.Form["EvTel"];
            adres.postaKodu = Request.Form["PostaKod"];
            adres.email = Request.Form["Email"];
            adres.sehirID = Convert.ToInt32(ddlSehir.SelectedValue.ToString());
            adres.ilceID = Convert.ToInt32(ddlIlce.SelectedValue.ToString());
            Session["KargoAdres"] = adres;
        }
        //Response.Redirect("odeme3.aspx");
        Response.RedirectToRoute("odemebilgileri", null);
    }
    public void FaturaAdresGetir()
    {

        Adres faturaAdres = (Adres)Session["FaturaAdres"];
        lblAd.Text = faturaAdres.adSoyad;
        lblAdres.Text = faturaAdres.adresTanimi;
        lblCepTel.Text = faturaAdres.cepTel;
        lblEmail.Text = faturaAdres.email;
        lblIl.Text = il.ilAdGetir(faturaAdres.sehirID);

        lblIlce.Text = ilce.ilceAdGetir(faturaAdres.ilceID);
        lblPosta.Text = faturaAdres.postaKodu;


    }
    public decimal ToplamTutar()
    {
        Sepet sepettekiler = (Sepet)Session["sepettekiler"];
        return sepettekiler.toplamTutar;
    }
}