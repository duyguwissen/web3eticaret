﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hesabim_siparislerim : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null & !IsPostBack)
        {
            SiparisleriGetir();
        }
        else
        {
            Response.Write("<script>alert('Önce giriş yapmalısınız.');window.location.href='" + GetRouteUrl("anasayfa", null) + "'</script>");
        }
    }
    public void SiparisleriGetir()
    {

        Uye uye = (Uye)Session["kullanici"];
        Siparis s = new Siparis();
        s.uyeId = uye.ID;
        rptSiparisler.DataSource = s.TumSiparisleriGetir();
        rptSiparisler.DataBind();

    }

    public string TarihDuzenle(object tarih)
    {

        DateTime t = (DateTime)tarih;
        string son = String.Format("{0:D}", t);
        return son;
    }
    protected void lbtnCikis_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.RedirectToRoute("anasayfa", null);
    }
}