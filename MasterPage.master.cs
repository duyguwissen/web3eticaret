﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MasterPage : System.Web.UI.MasterPage
{
    List<Siparis> siparisListesi;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null)
        {
            SepetiYukle();
            hlGiris.Text = "Hesabım";
            hlGiris.NavigateUrl = GetRouteUrl("hesap", null);

            lbtnKaydol.Text = "Çıkış Yap";
        }

        else
        {
            hlGiris.Text = "Giriş Yap";
            hlGiris.NavigateUrl = GetRouteUrl("giris", null);
            myshop.Visible = false;
        }

        //if (sender.GetType().ToString()!="Button")
        //{
        Kategori kat = new Kategori();
        rptkadin.DataSource = kat.KategoriGetir(true);
        rptkadin.DataBind();
        rpterkek.DataSource = kat.KategoriGetir(false);
        rpterkek.DataBind();

        //}

    }
    protected void aramadegistir_Click(object sender, EventArgs e)
    {
        if (txtArama.Text == "")
        {
            Response.Write("<script> alert('Lütfen aramak istediğiniz kelimeyi girin')</script>");
        }
        else
        {
            //Response.Redirect("urunler.aspx?arama=" + txtArama.Text + "");
            Response.Redirect(GetRouteUrl("aramaGetir", new { arama = txtArama.Text }));
            
        }
    }
    protected void lbtnKaydol_Click(object sender, EventArgs e)
    {
        if (lbtnKaydol.Text == "Kaydol")
        {
            hlGiris.Text = "Giriş Yap";
            //lbtnKaydol.GetRouteUrl("kaydol", "");
            Response.Redirect(GetRouteUrl("kaydol", null));
        }
        else
        {
            hlGiris.Text = "Giriş Yap";
            Session.RemoveAll();
            lbtnKaydol.Text = "Kaydol";
            Response.Redirect(GetRouteUrl("anasayfa", null));
        }
    }
    protected void btnSepet_Click(object sender, EventArgs e)
    {
        //if (Session["sepettekiler"] != null)
        //{
        //    Response.Redirect(GetRouteUrl("dolusepet", ""));
        //}
        //else
        //{
        //    Response.Redirect(GetRouteUrl("bossepet", ""));
        //}

        Sepet sepetim = (Sepet)Session["sepettekiler"];
        if (Session["sepettekiler"] == null || sepetim.siparisListesi.Count == 0)
        {
            Response.Redirect(GetRouteUrl("bossepet", null));
            return;
        }
        else
        {
            Response.Redirect(GetRouteUrl("dolusepet", null));
        }
    }
    protected void rptSepet_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        SepetiYukle();
    }

    protected void rptSepet_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

    }
    protected void rptSepet_DataBinding(object sender, EventArgs e)
    {

    }
    protected void lbtnUrunSil_Click(object sender, EventArgs e)
    {
        LinkButton lbtnSil = sender as LinkButton;
        int index = Convert.ToInt32(lbtnSil.CommandArgument.ToString());
        Sepet sepetim = (Sepet)Session["sepettekiler"];
        decimal siparistutar = sepetim.siparisListesi[index].toplamTutar;
        sepetim.toplamTutar -= siparistutar;
        sepetim.siparisListesi.RemoveAt(index);
        //Response.Write("<script>document.forms[0].submit();</script>");
    }
    public void SepetiYukle()
    {
        myshop.Visible = true;
        Sepet sepetim = (Sepet)Session["sepettekiler"];

        if (sepetim != null && sepetim.siparisListesi.Count != 0 )
        {
            siparisListesi = sepetim.siparisListesi;


            //decimal sepetToplamFiyat = 0;
            //foreach (var item in siparisListesi)
            //{

            //    sepetToplamFiyat += item.toplamTutar;
            //}
            lblUrunVeFiyat.Text = siparisListesi.Count() + " ürün - " + sepetim.toplamTutar + " TL";
            lblToplam.Text = sepetim.toplamTutar + " TL";
        }
        else
        {
            sepetim = null;
            Session["sepettekiler"] = sepetim;
            lblUrunVeFiyat.Text = "0 ürün - 0 TL";
            lblToplam.Text = "0 TL";
        }
        rptSepet.DataSource = siparisListesi;
        rptSepet.DataBind();

    }
    protected void btnBitir_Click(object sender, EventArgs e)
    {
        Sepet sepetim = (Sepet)Session["sepettekiler"];
        if (Session["sepettekiler"] == null || sepetim.siparisListesi.Count == 0)
        {
            Response.Write("<script>alert('Sepetinizde ürün bulunmamaktadır.')</script>");
            return;
        }
        Response.RedirectToRoute("faturabilgileri", null);

    }
}

