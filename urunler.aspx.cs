﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class urunler : System.Web.UI.Page
{
    public string katid;
    public string cinsiyet;
    Urun urun;
    //List<Urun> urunListesi;
    protected void Page_Load(object sender, EventArgs e)
    {
        //cinsiyet = Request.QueryString["cinsiyet"];
        if (RouteData.Values["cinsiyet"]!=null)
        {
            cinsiyet = (RouteData.Values["cinsiyet"].ToString());
            
        //}

        //if (!string.IsNullOrEmpty(cinsiyet))
        //{
            if (cinsiyet == "Kadın")
            {
                urun = new Urun();
                DataTable urunler = urun.cinsiyeteGoreUrunGetir("1");
                if (urunler.Rows.Count > 0)
                {
                    Repeater2.DataSource = urunler;
                    Repeater2.DataBind();

                }

            }
            if (cinsiyet == "Erkek")
            {
                urun = new Urun();
                DataTable urunler = urun.cinsiyeteGoreUrunGetir("0");
                if (urunler.Rows.Count > 0)
                {
                    Repeater2.DataSource = urunler;
                    Repeater2.DataBind();

                }

            }

        }


        //katid = Request.QueryString["kategori"];
        if (RouteData.Values["kategori"] != null)
        {
            katid = (RouteData.Values["kategori"].ToString());

            //}
            //if (!string.IsNullOrEmpty(katid))
            //{
            urun = new Urun();
            DataTable urunler = urun.kategoriIdIleUrunGetir(katid);
            if (urunler.Rows.Count > 0)
            {
                Repeater2.DataSource = urunler;
                Repeater2.DataBind();
            }

            Marka marka = new Marka();
            DataTable markaileurunler = marka.markaIdIleGetir(katid);
            if (markaileurunler.Rows.Count > 0)
            {
                Repeater1.DataSource = markaileurunler;
                Repeater1.DataBind();
            }

            UrunDetay ud = new UrunDetay();
            DataTable bedenileurunler = ud.kategoriIdileBedenGetir(katid);
            if (bedenileurunler.Rows.Count > 0)
            {
                rptbedenkatile.DataSource = bedenileurunler;
                rptbedenkatile.DataBind();

            }
            urun = new Urun();
            DataTable renkileurunler = urun.kategoriIdileRenkGetir(katid);
            if (renkileurunler.Rows.Count > 0)
            {
                int count = renkileurunler.Rows.Count;
                rptrenkkatile.DataSource = renkileurunler;
                rptrenkkatile.DataBind();

            }



        }
        else
        {

            OzellikDetay od = new OzellikDetay();
            rptbeden.DataSource = od.TumbedenleriGetir();
            rptbeden.DataBind();
            urun = new Urun();
            rptrenk.DataSource = urun.TumrenkleriGetir();
            rptrenk.DataBind();

            // urunAdına göre ürünlerin gelmesi
            AramayaAitUrunleriGetir();


        }

    }




    private void AramayaAitUrunleriGetir()
    {

        //string arama = Request.QueryString["arama"];

        //if (!string.IsNullOrEmpty(arama))
        //{
        if (RouteData.Values["arama"] != null)
        {

            string arama = RouteData.Values["arama"].ToString();
            urun = new Urun();
            DataTable urunler = urun.aramayaGoreUrunGetir(arama);


            #region başka yöntem
            //List<Urun> urunlerim = new List<Urun>();
            //List<OzellikDetay> detaylarim = new List<OzellikDetay>();

            //foreach (DataRow item in urunler.Rows)
            //{
            //    OzellikDetay d = new OzellikDetay();
            //    d.ozellikDetayi = item["ozellikDetayi"].ToString();
            //    d.ozellikDetayId = Convert.ToInt32(item["ozellikDetayId"].ToString());
            //    d.ozellikId = Convert.ToInt32(item["ozellikId"].ToString());
            //    detaylarim.Add(d);

            //    Urun u = new Urun();
            //    u.id = Convert.ToInt32(item["id"].ToString());
            //    u.urunAciklama=
            //}

            #endregion

            if (urunler.Rows.Count > 0)
            {
                Repeater2.DataSource = urunler;
                Repeater2.DataBind();
                //int sayi = urun.aramayaGoreMarkaGetir(arama).Rows.Count;

                Repeater1.DataSource = urun.aramayaGoreMarkaGetir(arama);
                Repeater1.DataBind();

                //sayi = urun.aramayaGoreBedenGetir(arama).Rows.Count;
                rptbeden.DataSource = urun.aramayaGoreBedenGetir(arama);
                rptbeden.DataBind();

            }
        }
    }

    public string tarihduzenle(object tarih)
    {
        CultureInfo culture = new CultureInfo("en-US");
        //january 1,2012
        DateTime t = Convert.ToDateTime(tarih);
        return string.Format(culture, "{0:MMMM d, yyyy}", t);
    }

    protected void btnSepeteEkle_Click(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null)
        {
            //Response.Write("<script>alert('Sepete eklemek için üye girişi yapmalısınız')</script>");
        }
        else
        {
            //ürün sepete eklenecek..
        }
    }
    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            DataRowView simdikiUrun = (DataRowView)e.Item.DataItem;
            string urunid = simdikiUrun.Row["id"].ToString();
            string urunid2 = simdikiUrun.Row["id"].ToString();
            //Data d = new Data();
            //d.cmd.Parameters.AddWithValue("simdiki", urunid);

            //bedenler --------------------------
            UrunDetay detay = new UrunDetay();
            detay.urunId = Convert.ToInt32(simdikiUrun.Row["id"].ToString());
            //DataTable dt = d.TableGetir("SELECT OzellikDetayId FROM tbl_urunDetay where ozellikId=1 and urunId=@simdiki");
            DataTable dt = detay.urunIdileBedenGetir();
            string bedenler = "";
            foreach (DataRow item in dt.Rows)
            {
                bedenler += " size" + item["OzellikDetayId"] + " ";
            }
            ((Literal)e.Item.FindControl("BedenClass")).Text = bedenler;

            //renkler --------------------------

            //d.cmd.Parameters.AddWithValue("simdiki2", urunid2);
            //DataTable dt2 = d.TableGetir("Select  renkId from tbl_urun where id=@simdiki2");
            dt = new DataTable();
            dt = detay.urunIdileRenkGetir();
            string renkler = "";
            foreach (DataRow item in dt.Rows)
            {
                renkler += "color" + item["renkId"] + " ";
            }
            ((Literal)e.Item.FindControl("RenkClass")).Text = renkler;
        }
    }

    public string RenkCevir(object renk)
    {

        string yeniRenk = renk.ToString();
        switch (yeniRenk)
        {
            case "Black":
                yeniRenk = "Siyah";
                break;
            case "Green":
                yeniRenk = "Yeşil";
                break;
            case "White":
                yeniRenk = "Beyaz";
                break;
            case "Orange":
                yeniRenk = "Turuncu";
                break;
            case "Red":
                yeniRenk = "Kırmızı";
                break;
            case "Pink":
                yeniRenk = "Pembe";
                break;
            case "Purple":
                yeniRenk = "Mor";
                break;
            case "Blue":
                yeniRenk = "Mavi";
                break;
            case "Grey":
                yeniRenk = "Gri";
                break;
            case "Brown":
                yeniRenk = "Kahverengi";
                break;
            case "Yellow":
                yeniRenk = "Sarı";
                break;

            default:
                yeniRenk = "black";
                break;
        }
        return yeniRenk;

    }

}