﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="blog.aspx.cs" Inherits="blog" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">


        <section id="Content" role="main">
        <div class="container">

            <!-- SECTION EMPHASIS 1 -->
            <!-- FULL WIDTH -->
        </div><!-- !container -->
        <div class="full-width section-emphasis-1 page-header">
            <div class="container">
                <header class="row">
                    <div class="col-md-12">
                        <h1 class=" pull-left">
                            Blog
                        </h1>
                        <!-- BREADCRUMBS -->
                        <ul class="breadcrumbs list-inline pull-right">
                            <li><a href="index.html">Home</a></li><!--
                            --><li><a href="index.html">Pages</a></li><!--
                            --><li>Blog</li>
                        </ul>
                        <!-- !BREADCRUMBS -->
                    </div>
                </header>
            </div>
        </div><!-- !full-width -->
            <!-- !FULL WIDTH -->
            <!-- !SECTION EMPHASIS 1 -->

        <div class="container">
            <div class="row">
                <div class="col-md-9 space-right-30">
                    <div class="row">
                        <div class="col-md-12 article-wrapper">
                            <!-- POST TYPE STANDARD -->
                            <article class="post-preview post-preview-image">
                                <a href="15-pages-blog-single.html">
                                    <img src="/images/demo-content/blog-image.jpg"  alt="Blog post image">
                                </a>
                                <header class="post-info-name-date-comments">
                                    <span class="date">Aug 7, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">Leave a comment</a>
                                    <h2><a href="15-pages-blog-single.html">Standard Post Format with Image</a></h2>
                                </header>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod est tempor incididunt ut
                                    labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris
                                    nisi ut aliquip ex ea commodo consequat. Duis aute et irure a dolor in reprehenderit in voluptate
                                    velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                                    sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </p>
                                <a href="15-pages-blog-single.html" class="btn btn-default btn-small read-more">Continue reading</a>
                            </article>
                            <!-- !POST TYPE STANDARD -->
                        </div>

                        <div class="col-md-12 article-wrapper">
                            <!-- POST TYPE GALLERY -->
                            <article class="post-preview post-preview-gallery">
                                <!-- FLEXSLIDER -->
                                <div class="flexslider flexslider-nopager">
                                    <ul class="slides">
                                        <li>
                                            <img src="/images/demo-content/blog-slider-1.jpg"  alt="Blog gallery image">
                                        </li>
                                        <li>
                                            <img src="/images/demo-content/blog-slider-2.jpg"  alt="Blog gallery image">
                                        </li>
                                        <li>
                                            <img src="/images/demo-content/blog-slider-3.jpg"  alt="Blog gallery image">
                                        </li>
                                    </ul>
                                </div>
                                <!-- !FLEXSLIDER -->
                                <header class="post-info-name-date-comments">
                                    <span class="date">Aug 5, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">5 comments</a>
                                    <h2><a href="#">Gallery Post Format</a></h2>
                                </header>
                                <p>
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute et irure a dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                                    deserunt mollit anim id est laborum.
                                </p>
                            </article>
                            <!-- !POST TYPE GALLERY -->
                        </div>

                        <div class="col-md-12 article-wrapper">
                            <!-- POST TYPE LINK -->
                            <article class="post-preview post-preview-link">
                                <header class="post-info-name-date-comments">
                                    <span class="date">Aug 1, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">2 comments</a>
                                    <h2><a href="#">Link Post Format</a></h2>
                                </header>
                            </article>
                            <!-- !POST TYPE LINK -->
                        </div>

                        <div class="col-md-12 article-wrapper">
                            <!-- POST TYPE QUOTE -->
                            <article class="post-preview post-preview-quote">
                                <header class="post-info-name-date-comments">
                                    <span class="date">Jul 30, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">1 comments</a>
                                </header>
                                <blockquote class="main-quote">
                                    <p>
                                        “Whatever you can do, or dream you can, begin it. Boldness has genius, power and magic in
                                        it.”
                                    </p>
                                    <small>
                                        Johann Wolfgang von Goethe <cite></cite>
                                    </small>
                                </blockquote>
                            </article>
                            <!-- !POST TYPE QUOTE -->
                        </div>

                        <div class="col-md-12 article-wrapper">
                            <!-- POST TYPE AUDIO -->
                            <article class="post-preview post-preview-audio">
                                <div class="audio">
                                    <iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=http%3A%2F%2Fapi.soundcloud.com%2Ftracks%2F69975342"></iframe>
                                </div>
                                <header class="post-info-name-date-comments">
                                    <span class="date">Jul 27, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">2 comments</a>
                                    <h2><a href="#">Audio Post Format</a></h2>
                                </header>
                                <p>
                                    Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                                    pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                    mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                                    accusantium doloremque laudantium.
                                </p>
                            </article>
                            <!-- !POST TYPE AUDIO -->
                        </div>

                        <div class="col-md-12 article-wrapper">
                            <!-- POST TYPE VIDEO -->
                            <article class="post-preview post-preview-video">
                                <iframe src="http://player.vimeo.com/video/16430345?color=2d91ff" width="680" height="382" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                                <header class="post-info-name-date-comments">
                                    <span class="date">Jul 27, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">2 comments</a>
                                    <h2><a href="#">Video Post Format</a></h2>
                                </header>
                                <p>
                                    Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute et irure a dolor in reprehenderit in voluptate velit esse cillum dolore eu
                                    fugiat nulla pariatur.
                                </p>
                            </article>
                            <!-- !POST TYPE VIDEO -->
                        </div>

                        <div class="col-md-12 article-wrapper">
                            <!-- PAGER -->
                            <nav>
                                <div class="pager">
                                    <ul class="list-inline">
                                        <li class="prev"><a href="#" class="icon-decima-small-arrow-left"><span class="sr-only">Prev</span></a></li><!--
                                        --><li>1</li><!--
                                        --><li class="active">2</li><!--
                                        --><li>3</li><!--
                                        --><li class="next"><a href="#" class="icon-decima-small-arrow-right"><span class="sr-only">Next</span></a></li>
                                    </ul>
                                </div>
                            </nav>
                            <!-- !PAGER -->
                        </div>
                    </div>
                </div>

                <div class="col-md-3">

                    <!-- SEARCH WIDGET -->
                    <div class="search-widget">
                        <div role="search">
                            <div class="form-group">
                                <label class="sr-only" for="page-search">Type your search here</label>
                                <input type="search" id="page-search" class="form-control">
                            </div><!-- no whitespace
                            --><button class="btn btn-default page-search">
                                <span class="fa fa-search">
                                    <span class="sr-only">Search</span>
                                </span>
                            </button>
                        </div>
                    </div>
                    <!-- !SEARCH WIDGET -->

                    <!-- CATEGORIES WIDGET -->
                    <h3 class="strong-header">
                        Categories
                    </h3>
                    <div class="categories-widget">
                        <ul class="list-unstyled">
                            <li><a href="#">Aside</a><span>1</span></li>
                            <li><a href="#">Audios</a><span>4</span></li>
                            <li><a href="#">Gallery</a><span>12</span></li>
                            <li><a href="#">Inspiration</a><span>46</span></li>
                            <li><a href="#">Links</a><span>3</span></li>
                            <li><a href="#">Photography</a><span>18</span></li>
                            <li><a href="#">Quotes</a><span>7</span></li>
                            <li><a href="#">SoundCloud</a><span>22</span></li>
                            <li><a href="#">Vimeo</a><span>9</span></li>
                        </ul>
                    </div>
                    <!-- !CATEGORIES WIDGET -->

                    <!-- RECENT POSTS WIDGET -->
                    <h3 class="strong-header">
                        Recent posts
                    </h3>
                    <div class="recent-posts-widget">
                        <ul class="list-unstyled">
                            <li>
                                <h5><a href="#">Rope necklace strong eyebrows center part shoe razor pleat white shirt</a></h5>
                                <span class="date">Aug, 9, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">3 comments</a>
                            </li>
                            <li>
                                <h5 class="title"><a href="#">Grey leather shorts collar vintage</a></h5>
                                <span class="date">Aug 7, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">1 comments</a>
                            </li>
                            <li>
                                <h5 class="title"><a href="#">Tucked t-shirt motif Missoni maxi skirt white Lanvin headscarf</a></h5>
                                <span class="date">Aug 5, 2013</span>&nbsp;&nbsp;<a href="#" class="comments">12 comments</a>
                            </li>
                        </ul>
                    </div>
                    <!-- !RECENT POSTS WIDGET -->


                </div>
            </div>
        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div> <!-- fixes floating problems when mobile menu is visible -->

    



</asp:Content>

