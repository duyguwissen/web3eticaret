﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class Marka
{
    public int markaID { get; set; }
    public string markaAd { get; set; }

    Data data;
	public Marka()
	{
        data = new Data();
	}

    public DataTable markaIdIleGetir(string katid)
    {
        data.cmd.Parameters.AddWithValue("kategori", katid);
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("markaIdIleGetir");
        //return data.TableGetir(@"SELECT DISTINCT m.markaAd,m.markaId FROM tbl_marka m INNER JOIN tbl_urun u ON u.urunMarkaId=m.markaId INNER JOIN tbl_kategori k ON u.urunKategoriId=k.kategoriId WHERE u.urunAktifPasif=1 AND u.silinmismi=0 AND k.kategoriId=@kategori");
    }
}