﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;


public class Urun
{
    public int id { get; set; }
    public int urunKategoriId { get; set; }
    public int urunMarkaId { get; set; }
    public string urunAd { get; set; }
    public string urunKod { get; set; }
    public string resimUrl { get; set; }
    public decimal urunFiyat { get; set; }
    public decimal urunYeniFiyat { get; set; }
    public int urunStok { get; set; }
    public bool urunAktifPasif { get; set; }
    public string urunAciklama { get; set; }
    public bool urunCinsiyet { get; set; }
    public DateTime urunEklenmeTarih { get; set; }
    public List<OzellikDetay> detay { get; set; }
    public string Renk { get; set; }
    public OzellikDetay ozelikDetayi { get; set; }
    public UrunDetay urunDetayi { get; set; }

    Data data;
    public Urun()
    {
        data = new Data();
    }

    public DataTable ensonEklenenUrunlerGetir()
    {
        //return data.TableGetir(@"SELECT * FROM tbl_urun u INNER JOIN tbl_urunResim ur ON u.id=ur.urunId INNER JOIN tbl_marka m ON m.markaId=u.urunMarkaId WHERE u.urunAktifPasif=1 AND u.silinmismi=0 ORDER BY u.urunEklenmeTarih DESC");
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("ensonEklenenUrunlerGetir");
    }

    public DataTable oneCikanUrunlerGetir()
    {
        //return data.TableGetir("SELECT * FROM tbl_urun u INNER JOIN tbl_urunResim r on u.id=r.urunId where urunAktifPasif=1 and silinmismi=0 ORDER BY hit DESC");
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("oneCikanUrunlerGetir");

    }

    public DataTable kategoriIdIleUrunGetir(string katid)
    {
        data.cmd.Parameters.AddWithValue("kategori", katid);
        //return data.TableGetir(@"SELECT * FROM tbl_kategori k INNER JOIN tbl_urun u ON u.urunKategoriId=k.kategoriId INNER JOIN tbl_urunResim ur ON ur.urunId=u.id INNER JOIN tbl_marka m ON m.markaId=u.urunMarkaId WHERE u.urunAktifPasif=1 AND u.silinmismi=0 AND k.kategoriId=@kategori");
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("kategoriIdIleUrunGetir");
    }
    public DataTable cinsiyeteGoreUrunGetir(string cinsiyet) {
        data.cmd.Parameters.AddWithValue("cinsiyet", cinsiyet);
        data.cmd.CommandType = CommandType.StoredProcedure;
        //return data.TableGetir("SELECT * FROM tbl_kategori k INNER JOIN tbl_urun u ON u.urunKategoriId=k.kategoriId INNER JOIN tbl_urunResim ur ON ur.urunId=u.id INNER JOIN tbl_marka m ON m.markaId=u.urunMarkaId WHERE u.urunAktifPasif=1 AND u.silinmismi=0 AND k.kategoriCinsiyet=@cinsiyet");
        return data.TableGetir("cinsiyeteGoreUrunGetir");
    }

    public DataTable yuksekPuanliUrunGetir()
    {
        //return data.TableGetir("select top 6 up.urunId,avg(up.puan) as puan,min(ur.resimUrl) as resimUrl,min(u.urunFiyat) as urunFiyat,min(u.urunAd) as urunAd from tbl_urun_puan up,tbl_urun u,tbl_urunResim ur where up.urunId=u.Id and up.urunId=ur.urunId  group by up.urunId order by avg(up.puan) desc");
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("yuksekPuanliUrunGetir");
    }

   

    public DataTable aramayaGoreUrunGetir(string arama)
    {

        SqlParameter param = new SqlParameter("@arama", SqlDbType.NVarChar);
        param.Direction = ParameterDirection.Input;
        param.Value = arama;
        data.cmd.Parameters.Add(param);

        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("aramayaGoreUrunGetir");
       }

    public DataTable aramayaGoreBedenGetir(string arama)
    {
        data.cmd.Parameters.AddWithValue("@bedenArama", arama);
        data.cmd.CommandType = CommandType.StoredProcedure;

        return data.TableGetir("aramayaGoreBedenGetir");
       

    }

    public DataTable aramayaGoreMarkaGetir(string arama)
    {

        data.cmd.Parameters.AddWithValue("@markaArama", arama);
        data.cmd.CommandType = CommandType.StoredProcedure;

        return data.TableGetir("aramayaGoreMarkaGetir");
        
    }

    public DataRow urunIdileUrunGetir(int urunId)
    {
        data.cmd.Parameters.AddWithValue("@urunIdsi", urunId);


        if (UrunKampanyadaMi(urunId))
        {
            data.cmd.Parameters.AddWithValue("@urunId", urunId);
            return data.SatirGetir("select y.urunId,avg(puan) puan,count(*) yorumSayisi,min(ur.resimUrl) as resimUrl,min(u.urunKod) as urunKod,min(u.urunAd) as urunAd,min(u.urunStok) as urunStok,min(u.urunFiyat) as urunFiyat,min(k.baslangicTarih) baslangicTarihi,min(k.bitisTarihi) as bitisTarihi,min(indirimOrani) as indirimOrani from tbl_urun u, tbl_yorum y,tbl_urunResim ur,tbl_kampanya k where  u.id=y.urunId and u.id=ur.urunId and k.urunId=u.id and u.id=@urunIdsi group by y.urunId,ur.urunId");
        }
        else
        {
            data.cmd.Parameters.AddWithValue("@urunIdsi", urunId);
            return data.SatirGetir("select y.urunId,avg(puan) puan,count(*) yorumSayisi,min(ur.resimUrl) as resimUrl,min(u.urunKod) as urunKod,min(u.urunAd) as urunAd,min(u.urunStok) as urunStok,min(u.urunFiyat) as urunFiyat from tbl_urun u, tbl_yorum y,tbl_urunResim ur where  u.id=y.urunId and u.id=ur.urunId and u.id=@urunIdsi group by y.urunId,ur.urunId");
        }


    }

    public bool UrunKampanyadaMi(int urunId)
    {
        /*denenmedi*/ 
        data.cmd.Parameters.AddWithValue("@urunId", urunId);
        data.cmd.CommandType = CommandType.StoredProcedure;
        int varMi = data.ExecuteNonQuery("UrunKampanyadaMi");

        if (varMi > 0)
        {
            return true;

        }
        else
        {
            return false;
        }


    }

    public DataRow urunuGetir()
    {

        data.cmd.Parameters.AddWithValue("@urunId", this.id);
        data.cmd.CommandType = CommandType.StoredProcedure;

        return data.SatirGetir("UrunGetir");

    }
    public DataTable TumrenkleriGetir()
    {
        data.cmd.CommandType = CommandType.StoredProcedure;
        
        return data.TableGetir("TumrenkleriGetir ");

    }
    public DataTable kategoriIdileRenkGetir(string katid)
    {

        data.cmd.Parameters.AddWithValue("katid", katid);
        data.cmd.CommandType = CommandType.StoredProcedure;
        //return data.TableGetir("Select DISTINCT renk from tbl_urun where urunKategoriId=@katid");
        //return data.TableGetir("Select DISTINCT renk,r.renkId from tbl_urun u inner join tbl_renk r on r.renkId=u.renkId where u.urunKategoriId=@katid");
        return data.TableGetir("kategoriIdileRenkGetir");

    }


}