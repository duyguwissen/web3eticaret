﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


public class Uye
{
    public int ID { get; set; }
    public string TC { get; set; }
    public string kullaniciAdi { get; set; }
    public string sifre { get; set; }
    public string ad { get; set; }
    public string soyad { get; set; }
    public bool cinsiyet { get; set; }
    public DateTime dogumTarihi { get; set; }
    public string cepTel { get; set; }
    public string evTel { get; set; }
    public string email { get; set; }
    public string yetki { get; set; }
    public DateTime kayitTarihi { get; set; }


    Data d;
    public Uye()
    {
        d = new Data();
    }

    public void GirisYap(string kullaniciOrEmail, string sifre)
    {

        d.cmd.Parameters.AddWithValue("@kullaniciOrEmail", kullaniciOrEmail);
        d.cmd.Parameters.AddWithValue("@sifre", sifre);
        d.cmd.CommandType = CommandType.StoredProcedure;
        DataRow uyeBilgileri = d.SatirGetir("GirisYap");
        //DataRow uyeBilgileri = d.SatirGetir("select * from tbl_uye where email=@kullaniciOrEmail and sifre=@sifre");
        if (uyeBilgileri != null)
        {

            this.ID = Convert.ToInt32(uyeBilgileri["ID"].ToString());
            this.ad = uyeBilgileri["ad"].ToString();
            this.email = uyeBilgileri["email"].ToString();
            this.kullaniciAdi = uyeBilgileri["kullaniciAdi"].ToString();
            this.sifre = uyeBilgileri["sifre"].ToString();
            this.soyad = uyeBilgileri["soyad"].ToString();

        }
        //else
        //{
        //    d.cmd.Parameters.AddWithValue("@kullaniciOrEmail", kullaniciOrEmail);
        //    d.cmd.Parameters.AddWithValue("@sifre", sifre);
        //    uyeBilgileri = d.SatirGetir("select * from tbl_uye where kullaniciAdi=@kullaniciOrEmail and sifre=@sifre");
        //    if (uyeBilgileri != null)
        //    {
        //        this.ID = Convert.ToInt32(uyeBilgileri["ID"].ToString());
        //        this.email = uyeBilgileri["email"].ToString();
        //        //uye.TC = uyeBilgileri["TC"].ToString();
        //        this.ad = uyeBilgileri["ad"].ToString();
        //        //uye.cepTel = uyeBilgileri["cepTel"].ToString();
        //        //uye.cinsiyet = Convert.ToBoolean(uyeBilgileri["cinsiyet"]);
        //        //uye.dogumTarihi = DateTime.Parse(uyeBilgileri["dogumTarihi"].ToString());
        //        //uye.kayitTarihi = DateTime.Parse(uyeBilgileri["kayitTarihi"].ToString());
        //        this.kullaniciAdi = uyeBilgileri["kullaniciAdi"].ToString();
        //        this.sifre = uyeBilgileri["sifre"].ToString();
        //        this.soyad = uyeBilgileri["soyad"].ToString();
        //    }


        //}


    }

    public bool UyeOl(Uye uye)
    {

        d.cmd.Parameters.AddWithValue("@TC", uye.TC);
        d.cmd.Parameters.AddWithValue("@kullaniciAdi", uye.kullaniciAdi);
        d.cmd.Parameters.AddWithValue("@sifre", uye.sifre);
        d.cmd.Parameters.AddWithValue("@ad", uye.ad);
        d.cmd.Parameters.AddWithValue("@soyad", uye.soyad);
        d.cmd.Parameters.AddWithValue("@cinsiyet", uye.cinsiyet);
        d.cmd.Parameters.AddWithValue("@dogumTarihi", uye.dogumTarihi);
        d.cmd.Parameters.AddWithValue("@cepTel", uye.cepTel);
        d.cmd.Parameters.AddWithValue("@evTel", uye.evTel);
        d.cmd.Parameters.AddWithValue("@email", uye.email);
        d.cmd.Parameters.AddWithValue("@kayitTarihi", uye.kayitTarihi);
        d.cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            //int sonuc = d.ExecuteNonQuery("insert into tbl_uye(TC,kullaniciAdi,sifre,ad,soyad,cinsiyet,dogumTarihi,cepTel,evTel,email,kayitTarihi) values(@TC,@kullaniciAdi,@sifre,@ad,@soyad,@cinsiyet,@dogumTarihi,@cepTel,@evTel,@email,@kayitTarihi)");
            int sonuc = d.ExecuteNonQuery("UyeOl");
            if (sonuc > 0)
            {
                return true;
            }
            return false;
        }
        catch (Exception)
        {
            return false;
        }
    }

    public void UyeBilgileriGetir(string email)
    {
        d.cmd.Parameters.AddWithValue("@email", email);
        d.cmd.CommandType = CommandType.StoredProcedure;
        //DataRow dr = d.SatirGetir("select * from tbl_uye where email=@email");
        DataRow dr = d.SatirGetir("UyeBilgileriGetir");
        if (dr != null)
        {
            this.kullaniciAdi = dr["kullaniciAdi"].ToString();
            this.sifre = dr["sifre"].ToString();
            this.email = dr["email"].ToString();
        }

    }

    public string Kaydol(Uye uye)
    {
        try
        {
            if (!KayitliMi())
            {
                d.cmd.Parameters.AddWithValue("@TC", uye.TC);
                d.cmd.Parameters.AddWithValue("@kullaniciAdi", uye.kullaniciAdi);
                d.cmd.Parameters.AddWithValue("@sifre", uye.sifre);
                d.cmd.Parameters.AddWithValue("@ad", uye.ad);
                d.cmd.Parameters.AddWithValue("@soyad", uye.soyad);
                d.cmd.Parameters.AddWithValue("@cinsiyet", uye.cinsiyet);
                d.cmd.Parameters.AddWithValue("@dogumTarihi", uye.dogumTarihi);
                d.cmd.Parameters.AddWithValue("@evTel", uye.evTel);
                d.cmd.Parameters.AddWithValue("@cepTel", uye.cepTel);
                d.cmd.Parameters.AddWithValue("@email", uye.email);
                d.cmd.CommandType = CommandType.StoredProcedure;


                //int kayitMi = Convert.ToInt32(d.ExecuteScalar("insert into tbl_uye(TC,kullaniciAdi,sifre,ad,soyad,cinsiyet,dogumTarihi,cepTel,evTel,email) values(@TC,@kullaniciAdi,@sifre,@ad,@soyad,@cinsiyet,@dogumTarihi,@cepTel,@evTel,@email)"));
                int kayitMi = Convert.ToInt32(d.ExecuteScalar("Kaydol"));


                if (kayitMi > 0)
                {
                    this.ID = kayitMi;
                    return "Kayıt başarılı";
                }
            }
            else
            {
                return "Kullanıcı Adı veya Email daha önceden kayıtlı";
            }

        }
        catch (Exception ex)
        {
            //return "Hata oluştu";
            return ex.Message;


        }

        return "Kullanıcı Adı veya Email daha önceden kayıtlı";
    }

    public bool KayitliMi()
    {

        d.cmd.Parameters.AddWithValue("@kullaniciOrEmail", this.email);
        d.cmd.CommandType = CommandType.StoredProcedure;
        int varMi = d.ExecuteNonQuery("KayitliMi");
        if (varMi > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
        //int varMi = d.ExecuteNonQuery("select * from tbl_uye where email=@kullaniciOrEmail");
        //if (varMi > 0)
        //{
        //    return true;

        //}
        //else
        //{
        //    d.cmd.CommandType = CommandType.StoredProcedure;
        //    d.cmd.Parameters.AddWithValue("@kullaniciOrEmail", this.kullaniciAdi);
        //    //d.cmd.Parameters.AddWithValue("@sifre", sifre);
        //    varMi = d.ExecuteNonQuery("select * from tbl_uye where kullaniciAdi=@kullaniciOrEmail");
        //    if (varMi > 0)
        //    {
        //        return true;


        //    }
        //    else
        //    {
        //        return false;
        //    }


    }


    public List<string> AdminMailAdresleriGetir()
    {
        //DataTable adminler = d.TableGetir("select email from tbl_uye where yetki='admin'");
        d.cmd.CommandType = CommandType.StoredProcedure;
        DataTable adminler = d.TableGetir("AdminMailAdresleriGetir");
        List<string> adminListesi = new List<string>();
        foreach (DataRow item in adminler.Rows)
        {
            string email = item["email"].ToString();
            adminListesi.Add(email);
        }
        return adminListesi;

    }

    public DataRow HesapBilgileriGetir()
    {

        d.cmd.Parameters.AddWithValue("@uyeId", this.ID);
        d.cmd.CommandType = CommandType.StoredProcedure;
        //return d.SatirGetir("select * from tbl_uye where ID=@uyeId");
        return d.SatirGetir("HesapBilgileriGetir");


    }

    public int BilgileriDuzenle(Uye uye)
    {

        try
        {
            d.cmd.Parameters.AddWithValue("@uyeId", uye.ID);
            d.cmd.Parameters.AddWithValue("@ad", uye.ad);
            d.cmd.Parameters.AddWithValue("@soyad", uye.soyad);
            d.cmd.Parameters.AddWithValue("@email", uye.email);
            d.cmd.Parameters.AddWithValue("@sifre", uye.sifre);
            d.cmd.CommandType = CommandType.StoredProcedure;

            //return d.ExecuteNonQuery("update tbl_uye  set ad=@ad, soyad=@soyad,email=@email, sifre=@sifre where ID=@uyeId");
            return d.ExecuteNonQuery("BilgileriDuzenle");

        }
        catch (Exception)
        {

            throw;
        }
    }

    public bool DogruMu()
    {
        d.cmd.Parameters.AddWithValue("@uyeId", this.ID);
        d.cmd.Parameters.AddWithValue("@sifre", this.sifre);
        d.cmd.CommandType = CommandType.StoredProcedure;
        //return d.SatirGetir("select * from tbl_uye where ID=@uyeId");
        int dogruMu = d.ExecuteNonQuery("DogruMu");
        if (dogruMu > 0)
        {
            return true;
        }
        return false;
    }

    public void SifreDegistir(string sifre, int uyeId)
    {
        d.cmd.Parameters.AddWithValue("@sifre", sifre);
        d.cmd.Parameters.AddWithValue("@uyeId", uyeId);
        d.cmd.CommandType = CommandType.StoredProcedure;
        //DataRow dr = d.SatirGetir("select * from tbl_uye where email=@email");
        try
        {
            d.ExecuteNonQuery("SifreDegistir");
        }
        catch (Exception)
        {

            throw;
        }

    }
}