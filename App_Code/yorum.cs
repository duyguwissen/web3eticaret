﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


public class Yorum
{
    public int yorumID { get; set; }
    public int uyeID { get; set; }
    public int urunID { get; set; }
    public DateTime yorumTarih { get; set; }
    public string yorum { get; set; }
    public int puan { get; set; }

    Data d;

	public Yorum()
	{
        d = new Data();
	}

    public DataTable YorumlariGetir(int urunId)
    {

        d.cmd.Parameters.AddWithValue("@urunId", urunId);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.TableGetir("YorumlariGetir");
        //return d.TableGetir("select ad,soyad,yorumTarih,yorum,puan from tbl_yorum y inner join tbl_uye u on u.ID=y.uyeId where urunId=@urunId");

    }


    public bool PuanVer()
    {

        d.cmd.Parameters.AddWithValue("@uyeId", this.uyeID);
        d.cmd.Parameters.AddWithValue("@urunId", this.urunID);
        d.cmd.Parameters.AddWithValue("@yorum", this.yorum);
        d.cmd.Parameters.AddWithValue("@puan", this.puan);

        //int kaydettiMi = d.ExecuteNonQuery("insert into tbl_yorum(urunId,uyeId,yorum,puan) values(@urunId,@uyeId,@yorum,@puan)");
        d.cmd.CommandType = CommandType.StoredProcedure;
        int kaydettiMi = d.ExecuteNonQuery("PuanVer");
        if (kaydettiMi > 0)
        {
            return true;
        }
        else
        {
            return false;
        }

    }


}