﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for Kategori
/// </summary>
public class Kategori
{
    public int kategoriID { get; set; }
    public string kategoriAdi { get; set; }
    public bool kategoriCinsiyet { get; set; }
    Data data;
	public Kategori()
	{
        data = new Data();
		
	}
    public DataTable KategoriGetir(bool cinsiyet) {

        data.cmd.Parameters.AddWithValue("cinsiyet", cinsiyet);
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("KategoriGetir");
        //return data.TableGetir("SELECT * FROM tbl_kategori WHERE kategoriCinsiyet=@cinsiyet");
    }




 
}