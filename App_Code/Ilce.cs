﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Ilce
/// </summary>
public class Ilce
{
    public int id { get; set; }
    public string ilce { get; set; }
    public int ilID { get; set; }
    Data d;
    public Ilce()
    {
        d = new Data();
    }
    public DataTable IlceleriGetir(int ilId)
    {

        d.cmd.Parameters.AddWithValue("@ilId", ilId);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.TableGetir("IlceleriGetir");
        //return d.TableGetir("select * from tbl_ilce where ilID=@ilId");

    }

    public string ilceAdGetir(int ilceId)
    {

        d.cmd.Parameters.AddWithValue("@ilceId", ilceId);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.ExecuteScalar("ilceAdGetir").ToString();
        //return d.ExecuteScalar("select ilce from tbl_ilce where id=@ilceId").ToString();

    }
}