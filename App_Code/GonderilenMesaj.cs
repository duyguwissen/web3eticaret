﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class GonderilenMesaj
{
    public int ID { get; set; }
    public string adSoyad { get; set; }
    public string email { get; set; }
    public string mesajiniz { get; set; }
    public DateTime eklenmeTarihi { get; set; }

    Data data;
    public GonderilenMesaj()
    {
        data = new Data();
    }

    public bool MesajKayit()
    {
        data.cmd.Parameters.AddWithValue("padSoyad", this.adSoyad);
        data.cmd.Parameters.AddWithValue("pemail", this.email);
        data.cmd.Parameters.AddWithValue("pmesajiniz", this.mesajiniz);
        data.cmd.CommandType = CommandType.StoredProcedure;
        try
        {
            //data.ExecuteNonQuery("INSERT INTO tbl_gonderilenMesaj (adSoyad,email, mesajiniz) VALUES (@padSoyad, @pemail , @pmesajiniz )");
            data.ExecuteNonQuery("MesajKayit");
        }
        catch (Exception)
        {
            return false;

        }
        return true;
    }
}