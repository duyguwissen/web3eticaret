﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

public class Siparis
{
    public int siparisID { get; set; }
    public int urunId { get; set; }
    public int urunAdet { get; set; }
    public decimal toplamTutar { get; set; }
    public int uyeId { get; set; }
    public DateTime siparisTarih { get; set; }
    public int siparisDurumId { get; set; }
    public int odemeTipId { get; set; }
    public int kargoId { get; set; }
    public DateTime ongorulenTeslimTarih { get; set; }
    public DateTime teslimTarih { get; set; }
    public string takipNo { get; set; }
    public decimal urunFiyat { get; set; }
    public Urun urun { get; set; }
    Data d;

    public Siparis()
    {
        d = new Data();

    }

    public DataTable TumSiparisleriGetir()
    {
        d.cmd.Parameters.AddWithValue("@uyeId", this.uyeId);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.TableGetir("TumSiparisleriGetir");
        //return d.TableGetir("select * from tbl_siparis s inner join tbl_siparisDurum sd on s.siparisDurumId=sd.siparisDurumId where uyeId=@uyeId");



    }
    public DataTable BirkacSiparisGetir()
    {
        d.cmd.Parameters.AddWithValue("@uyeId", this.uyeId);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.TableGetir("BirkacSiparisGetir");
        //return d.TableGetir("select top 3 * from tbl_siparis s inner join tbl_siparisDurum sd on s.siparisDurumId=sd.siparisDurumId where uyeId=@uyeId");



    }
}