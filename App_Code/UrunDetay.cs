﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


public class UrunDetay
{
    public int urunDetayId { get; set; }
    public int urunId { get; set; }
    public int ozellikId { get; set; }
    public int ozellikDetayId { get; set; }
    public string ozellikDetayi { get; set; }
    public int stokAdet { get; set; }
    Data data;
	public UrunDetay()
	{
	    data=new Data();
        
    }

    public DataTable kategoriIdileBedenGetir(string katid){

        data.cmd.Parameters.AddWithValue("katid", katid);

        return data.TableGetir("select DISTINCT ozellikDetayi,o.ozellikDetayId from tbl_urunDetay d inner join tbl_urun u on d.urunId=u.id inner join tbl_ozellikDetay o on d.ozellikDetayId=o.ozellikDetayId where d.ozellikId=1 and urunAktifPasif=1 and silinmismi=0  and urunKategoriId=@katid");
    

    }



    public DataTable uruneAitOzellikleriGetir(int urunId)
    {
        data.cmd.Parameters.AddWithValue("@urunId", urunId);

        return data.TableGetir("select distinct(ozellikAdi) from tbl_urunDetay ud inner join tbl_ozellik o on ud.ozellikId=o.ozellikId where ud.urunId=@urunId");

    }

    public DataTable OzellikDetaylariGetir(int urunId, string ozellikAd)
    {

        data.cmd.Parameters.AddWithValue("@urunId", urunId);
        data.cmd.Parameters.AddWithValue("@ozellikAd", ozellikAd);
        string sorgu = "select * from tbl_urunDetay ud inner join tbl_ozellik o on ud.ozellikId=o.ozellikId inner join tbl_ozellikDetay od on od.ozellikId=o.ozellikId  where ud.urunId=" + urunId + " and o.ozellikAdi='" + ozellikAd + "'";

        return data.TableGetir("select * from tbl_urunDetay ud inner join tbl_ozellik o on ud.ozellikId=o.ozellikId inner join tbl_ozellikDetay od on od.ozellikId=o.ozellikId  where ud.urunId=" + urunId + " and o.ozellikAdi='" + ozellikAd + "'");

    }

    public DataTable urunIdileBedenGetir() {

        data.cmd.Parameters.AddWithValue("@urunId", this.urunId);
        return data.TableGetir("select * from tbl_urunDetay ud inner join tbl_ozellikDetay od on ud.ozellikDetayId=od.ozellikDetayId where ud.ozellikId=1 and ud.urunId=@urunId ");
    
    
    }

    public DataTable urunIdileRenkGetir()
    {

        data.cmd.Parameters.AddWithValue("@urunId", this.urunId);


        //return data.TableGetir("Select DISTINCT renk from tbl_urun where urunKategoriId=@katid");
        return data.TableGetir("Select  renkId from tbl_urun where id=@urunId");

    }

    
}