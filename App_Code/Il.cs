﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Il
/// </summary>
public class Il
{

    public int id { get; set; }
    public string il { get; set; }
    Data d;

	public Il()
    {
        d = new Data();
		
	}

    public DataTable IlleriGetir() {

       //return  d.TableGetir("select * from tbl_il");
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.TableGetir("IlleriGetir");

    }
    public string ilAdGetir(int ilId) {

        d.cmd.Parameters.AddWithValue("@ilId", ilId);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.ExecuteScalar("ilAdGetir").ToString();
        //return d.ExecuteScalar("select il from tbl_il where id=@ilId").ToString();
        
    
    }
}