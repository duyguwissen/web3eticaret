﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


public class Tedarikci
{
    public int ID { get; set; }
    public int urunID { get; set; }
    public string firmaAdi { get; set; }
    public int ilID { get; set; }
    public int ilceID { get; set; }
    public string adresDetay { get; set; }
    public int vergiDairesiID { get; set; }
    public string vergiNo { get; set; }
    public string isTel { get; set; }
    public string cepTel { get; set; }
    public string email { get; set; }

	public Tedarikci()
	{
		
	}
}