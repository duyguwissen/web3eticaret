﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Kampanya
/// </summary>
public class Kampanya
{
    public int ID { get; set; }
    public int urunID { get; set; }
    public int indirinOrani { get; set; }
    public DateTime baslangicTarih { get; set; }
    public DateTime bitisTarihi { get; set; }
    public string kampanyaBaslik { get; set; }
    public string kampanyaAciklama { get; set; }
	public Kampanya()
	{
		
	}
}