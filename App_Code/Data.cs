﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Data
{
    //public static string baglanti = "Server=62.210.114.205,1433\\SQLEXPRESS2012;Database=Web3eticaret;User Id=web3user; Password=web987;";
    public static string baglanti = ConfigurationManager.ConnectionStrings["Connection"].ConnectionString;
    public SqlCommand cmd = new SqlCommand();

    public int ExecuteNonQuery(string sorgu)
    {
        SqlConnection con = new SqlConnection(baglanti);
        con.Open();
        cmd.Connection = con;
        cmd.CommandText = sorgu;
        int sonuc = cmd.ExecuteNonQuery();
        cmd.CommandText = "";
        cmd.Parameters.Clear();
        con.Close();
        return sonuc;
    }

    public DataRow SatirGetir(string sorgu)
    {
        SqlConnection con = new SqlConnection(baglanti);
        con.Open();
        cmd.Connection = con;
        cmd.CommandText = sorgu;
        SqlDataReader rdr = cmd.ExecuteReader();
        cmd.CommandText = "";
        cmd.Parameters.Clear();
        DataTable dt = new DataTable();
        dt.Load(rdr);
        con.Close();
        try
        {
            return dt.Rows[0];
        }
        catch
        {
            return null;
        }
    }

    public DataTable TableGetir(string sorgu)
    {
        DataTable dt = new DataTable();
        SqlConnection con = new SqlConnection(baglanti);
        con.Open();
        //SqlCommand cmd=new SqlCommand(commandtext,connection);
        cmd.CommandText = sorgu;
        cmd.Connection = con;
        SqlDataReader rdr = cmd.ExecuteReader();
        dt.Load(rdr);
        con.Close();
        cmd.CommandText = "";
        cmd.Parameters.Clear();
        return dt;
    }

    public DataSet DataSetGetir(string sorgu)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(baglanti);
        con.Open();
        //SqlCommand cmd=new SqlCommand(commandtext,connection);
        cmd.CommandText = sorgu;
        cmd.Connection = con;
        SqlDataReader rdr = cmd.ExecuteReader();
        SqlDataAdapter sda = new SqlDataAdapter();
        sda.SelectCommand = cmd;
        rdr.Close();
        sda.Fill(ds);

        con.Close();
        cmd.CommandText = "";
        cmd.Parameters.Clear();
        return ds;
    }

    public object ExecuteScalar(string sorgu)
    {

        SqlConnection con = new SqlConnection(baglanti);
        con.Open();
        cmd.Connection = con;
        cmd.CommandText = sorgu;
        object sonuc = cmd.ExecuteScalar();
        cmd.CommandText = "";
        cmd.Parameters.Clear();
        con.Close();
        return sonuc;

    }
}

