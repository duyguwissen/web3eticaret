﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


public class UrunResim
{
    public int resimId { get; set; }
    public int urunId { get; set; }
    public string resimUrl { get; set; }

    Data d;
	public UrunResim()
	{
        d = new Data();
	
	}


    public DataTable urunResimleriGetir() {

        d.cmd.Parameters.AddWithValue("@urunId", this.urunId);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.TableGetir("urunResimleriGetir");
        //return d.TableGetir("select * from tbl_urunResim where urunId=@urunId");
    
    }
}