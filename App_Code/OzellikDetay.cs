﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


public class OzellikDetay
{

    public int ozellikDetayId { get; set; }
    public int ozellikId { get; set; }
    public string ozellikDetayi { get; set; }
    Data data;
	public OzellikDetay()
	{
        data = new Data();
	}
    public DataTable TumbedenleriGetir()
    {
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("TumbedenleriGetir");
        //return data.TableGetir("SELECT * FROM tbl_ozellikDetay WHERE ozellikId=1");
    }

   
}