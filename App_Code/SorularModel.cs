﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SorularModel
/// </summary>
public class SorularModel
{
    public int ID { get; set; }
    public string Soru { get; set; }
    public string Cevap { get; set; }
    public string Tur { get; set; }
    Data data;

    public SorularModel()
    {
        data = new Data();
    }
    public DataTable TumSorulariGetir(string Tur)
    {
        data.cmd.Parameters.AddWithValue("tur", Tur);
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("TumSorulariGetir");
        //return data.TableGetir("SELECT * FROM tbl_sikSorulanlar WHERE Tur=@tur");
    }
}