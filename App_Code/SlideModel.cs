﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SlideModel
/// </summary>
public class SlideModel
{
    public int ID { get; set; }
    public string Yazi { get; set; }
    public string Resim { get; set; }
    public string Altyazi { get; set; }

    Data data;

	public SlideModel()
	{
        data = new Data();
	}
    public DataTable SlideGetir()
    {
        data.cmd.CommandType = CommandType.StoredProcedure;
        return data.TableGetir("SlideGetir");
        //return data.TableGetir("SELECT * FROM tbl_slider");
    }
}