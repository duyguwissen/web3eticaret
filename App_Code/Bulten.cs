﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Bulten
/// </summary>
public class Bulten
{
    public int ID { get; set; }
    public string Adsoyad { get; set; }
    public string Email { get; set; }
    public DateTime EklenmeTarihi { get; set; }

    Data data;

    public Bulten()
    {
        data = new Data();
    }
    public void BultenKayit()
    {
        data.cmd.Parameters.AddWithValue("padSoyad", this.Adsoyad);
        data.cmd.Parameters.AddWithValue("pemail", this.Email);
        data.cmd.CommandType = CommandType.StoredProcedure;
        data.ExecuteNonQuery("BultenKayit");
        //data.ExecuteNonQuery("INSERT INTO tbl_bulten (Adsoyad,Email)  VALUES (@padSoyad, @pemail)");

    }

    public bool BultenKayiti()
    {
        data.cmd.Parameters.AddWithValue("padSoyad", this.Adsoyad);
        data.cmd.Parameters.AddWithValue("pemail", this.Email);
        data.ExecuteNonQuery("INSERT INTO tbl_bulten (Adsoyad,Email)  VALUES (@padSoyad, @pemail)");


        try
        {
            data.ExecuteNonQuery("INSERT INTO tbl_bulten (Adsoyad,Email) VALUES (@padSoyad, @pemail)");
        }
        catch (Exception)
        {
            return false;

        }
        return true;

    }
}
