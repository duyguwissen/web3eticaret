﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Adres
/// </summary>
public class Adres
{
    public int uyeID { get; set; }
    public string adresTanimi { get; set; }
    public string adSoyad { get; set; }
    public int sehirID { get; set; }
    public int ilceID { get; set; }
    public string postaKodu { get; set; }
    public string cepTel { get; set; }
    public string evTel { get; set; }
    public bool defaultAdres { get; set; }
    public string email { get; set; }
    public string adresDetay { get; set; }
    //public string faturaAdresDetay { get; set; }
    //public string teslimatAdresDetay { get; set; }

    Data d;
	public Adres()
	{
        d = new Data();
	}

    public DataRow TeslimatAdresGetir() {

        d.cmd.Parameters.AddWithValue("@uyeId", this.uyeID);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.SatirGetir("TeslimatAdresGetir");
        //return d.SatirGetir("select * from tbl_adres a inner join tbl_il i on i.id=a.sehirID inner join tbl_ilce ilce on ilce.id=a.ilceID where a.uyeID=@uyeId and a.adresTanimi='Teslimat'");
    }

    public DataRow FaturaAdresGetir()
    {

        d.cmd.Parameters.AddWithValue("@uyeId", this.uyeID);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.SatirGetir("FaturaAdresGetir");
        //return d.SatirGetir("select * from tbl_adres a inner join tbl_il i on i.id=a.sehirID inner join tbl_ilce ilce on ilce.id=a.ilceID where a.uyeID=@uyeId and a.adresTanimi='Fatura'");
    }

    public bool AdresDegistir(Adres adres) {

        d.cmd.Parameters.AddWithValue("@adSoyad", adres.adSoyad);
        d.cmd.Parameters.AddWithValue("@sehirId", adres.sehirID);
        d.cmd.Parameters.AddWithValue("@ilceId", adres.ilceID);
        d.cmd.Parameters.AddWithValue("@postaKodu", adres.postaKodu);
        d.cmd.Parameters.AddWithValue("@cepTel", adres.cepTel);
        d.cmd.Parameters.AddWithValue("@evTel", adres.evTel);
        d.cmd.Parameters.AddWithValue("@adres", adres.adresDetay);
        d.cmd.Parameters.AddWithValue("@uyeId", adres.uyeID);
        d.cmd.Parameters.AddWithValue("@adresTanimi", adres.adresTanimi);
        

        d.cmd.CommandType = CommandType.StoredProcedure;
        int kaydedildiMi=d.ExecuteNonQuery("AdresDegistir");
        //d.ExecuteNonQuery("update tbl_adres set adSoyad=@adSoyad,sehirID=@sehirId,ilceID=@ilceId,postaKodu=@postaKodu,cepTel=@cepTel,evTel=@evTel,AdresDetay=@adres where uyeId=@uyeId and adresTanimi=@adresTanimi");
        if (kaydedildiMi>0)
        {
            return true;
        }
        return false;

    }

    public void FaturaAdresiEkle(Adres adres) {

        if (adres.defaultAdres==true)
        {
            d.ExecuteNonQuery("");
        }
    
    }


    public DataRow AdresGetir()
    {

        d.cmd.Parameters.AddWithValue("@uyeId", this.uyeID);
        d.cmd.CommandType = CommandType.StoredProcedure;
        return d.SatirGetir("AdresGetir");
        //return d.SatirGetir("select * from tbl_adres a inner join tbl_il i on i.id=a.sehirID inner join tbl_ilce ilce on ilce.id=a.ilceID where a.uyeID=@uyeId and defaultAdres='true'");
    }

    
}