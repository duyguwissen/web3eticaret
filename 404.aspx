﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="404.aspx.cs" Inherits="_404" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <section id="Content" role="main">

        <div class="full-width four-o-four">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h1 class="strong-header">
                            Error 404:<br>
                            Page not found
                        </h1>
                        <p>
                            The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.
                        </p>
                        <a href="/" class="btn btn-primary">Return to home page</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="clearfix visible-xs visible-sm"></div> <!-- fixes floating problems when mobile menu is visible -->





</asp:Content>

