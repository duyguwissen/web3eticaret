﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class sepet_full : System.Web.UI.Page
{
    List<Siparis> siparisler;
    protected void Page_Load(object sender, EventArgs e)
    {
        SepetiYukle();
    }

    public decimal ToplamTutar()
    {
        Sepet sepettekiler = (Sepet)Session["sepettekiler"];
        return sepettekiler.toplamTutar;
        //decimal tutar=0;
        //foreach (var item in siparisler)
        //{
        //    tutar += Convert.ToDecimal(item.toplamTutar);
        //}
        
        //return tutar;
    }
    protected void lbtnKaldir_Click(object sender, EventArgs e)
    {
        LinkButton lbtn=(LinkButton)(sender as LinkButton);
        int index=Convert.ToInt32(lbtn.CommandArgument.ToString());

        Sepet sepettekiler = (Sepet)Session["sepettekiler"];
        decimal siparistutar = sepettekiler.siparisListesi[index].toplamTutar;
        sepettekiler.toplamTutar -= siparistutar;
        sepettekiler.siparisListesi.RemoveAt(index);
        Session["sepettekiler"] = sepettekiler;
        Response.RedirectToRoute("dolusepet", null);
    }
    protected void rptUrunler_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        SepetiYukle();

    }
    public void SepetiYukle() {
        Sepet sepettekiler = (Sepet)Session["sepettekiler"];
        siparisler = new List<Siparis>();
        siparisler = sepettekiler.siparisListesi;
        rptUrunler.DataSource = siparisler;
        rptUrunler.DataBind();
    
    }
}