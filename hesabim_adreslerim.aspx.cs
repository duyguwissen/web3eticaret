﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class hesabim_adreslerim : System.Web.UI.Page
{
    Adres adres;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["kullanici"] != null)
        {
            adres = new Adres();
            adres.uyeID = ((Uye)(Session["kullanici"])).ID;

            /*fatura bilgileri*/
            FaturaAdresGetir();
            /*teslimat bilgileri*/
            TeslimatAdresGetir();

        }
        else
        {
            Response.Write("<script>alert('Önce giriş yapmalısınız.');window.location.href='" + GetRouteUrl("anasayfa", null) + "'</script>");
        }
    }
    protected void lbtnCikis_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.RedirectToRoute("anasayfa", null);
    }
    public void FaturaAdresGetir()
    {
        DataRow faturaAdres = adres.FaturaAdresGetir();
        if (faturaAdres != null)
        {
            lblIsim.Text = faturaAdres["adSoyad"].ToString();
            lblFaturaAdres.Text = faturaAdres["AdresDetay"].ToString();
            lblFaturaIl.Text = faturaAdres["il"].ToString();
            lblFaturaIlcePostaKod.Text = faturaAdres["ilce"].ToString() + "," + faturaAdres["postaKodu"].ToString();
        }
    }
    public void TeslimatAdresGetir()
    {
        DataRow teslimatAdres = adres.TeslimatAdresGetir();
        if (teslimatAdres != null)
        {
            lblIsim2.Text = teslimatAdres["adSoyad"].ToString();
            lblTeslimatAdres.Text = teslimatAdres["AdresDetay"].ToString();
            lblTeslimatIl.Text = teslimatAdres["il"].ToString();
            lblTeslimatIlce.Text = teslimatAdres["ilce"].ToString() + "," + teslimatAdres["postaKodu"].ToString();
        }
    }
}