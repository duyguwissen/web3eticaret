﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class uyekayit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    protected void btnKaydol_Click(object sender, EventArgs e)
    {
        Uye uye = new Uye();
        uye.ad = txtAd.Text.Trim();
        uye.soyad = txtSoyad.Text.Trim();
        uye.cepTel = txtCepTel.Text;
        if (rbtnCinsiyet.SelectedValue.ToString() == "Bayan")
        {
            uye.cinsiyet = false;
        }
        else
        {
            uye.cinsiyet = true;
        }
        uye.dogumTarihi = Convert.ToDateTime(txtDogumTarihi.Text).Date;
        uye.email = txtEmail.Text.Trim();
        uye.evTel = txtEvTel.Text.Trim();
        uye.kullaniciAdi = txtKullaniciAd.Text.Trim();
        uye.sifre = txtPassword.Text.Trim();
        uye.TC = txtTc.Text;


        string durum = uye.Kaydol(uye);


        Response.Write("<script>alert('" + durum + "')</script>");

        if (durum=="Kayıt başarılı")
        {
            
            //Response.Redirect(GetRouteUrl("anasayfa", ""));
            Response.Write("<script>window.location.href='" + GetRouteUrl("anasayfa", null) + "'</script>");
            Session["kullanici"] = uye;
            txtTc.Text = "";
            txtCepTel.Text = "";
            txtDogumTarihi.Text = "";
            txtKullaniciAd.Text = "";
            txtPassword.Text = "";
            txtRepeatPassword.Text = "";
            txtAd.Text = "";
            txtSoyad.Text = "";
            txtEvTel.Text = "";
            txtEmail.Text = "";
        }
       


    }
    protected void cvTc_ServerValidate(object source, ServerValidateEventArgs args)
    {
        string KimlikNumarasi = args.Value;

        if (KimlikNumarasi.Length != 11)
        {
            args.IsValid = false;
        }

        else
        {
            int KimlikSonHane = Convert.ToInt32(KimlikNumarasi.Substring(10));
            //TC Kimlik No uzunluğunu kontrol edelim
            if (KimlikNumarasi.Length != 11)
            {
                args.IsValid = false;
                return;
            }
            //TC Kİmlik no son hanenin çift olup olmadığını kontrol edelim
            if (KimlikSonHane % 2 == 1)
            {
                args.IsValid = false;
                return;
            }
            //İlk 10 hane toplamını alalım
            string OnHane = KimlikNumarasi.Substring(0, 10);
            int toplam = 0;
            foreach (char c in OnHane)
            {
                toplam += Convert.ToInt32(c.ToString());
            }
            //10 sane toplamını kontrol edelim
            if (toplam % 10 == KimlikSonHane)
            {
                args.IsValid = true;
            }
            else
            {
                args.IsValid = false;
            }
        }
    }
}